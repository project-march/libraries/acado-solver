#!/bin/bash
# Tool for generating all models at once or separately

# The goal of this script is that when a change is made to the models,
# the models can easily be rebuild before pushing it to git

# Bash error handling
set -o nounset
set -o errexit
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
set -o errtrace
set -o pipefail

# Echo colors
RED='\e[31m'
BLUE='\e[34m'
NC='\e[0m' # No Color

# Function for generating all models
generateAllModels() {
  # Go to models/ directory
  (cd models || exit;
    # Loop through all models
    for model in *;
    do
      # Check if $model is a directory
      [[ -d "$model" ]] || break

      # generate the model
      (cd .. || exit; generateModel "$model")
    done
  )
}

generateModel() {
  # Get model name from argument
  model="$1"

  # Go to model directory and run the executable
  echo -e "----------\nGenerating: ${BLUE}$model${NC}\n----------"
  (cd "models/$model/bin" || { echo -e "${RED}Failure, \"$model\" does not exist${NC}"; exit; }; ./"$model")
}

printHelpMessage() {
  # Print help message
  echo "Usage: ./generate_model.sh [OPTION]... or [FILE]..."
  echo "Tool for generating all models at once or separately."
  echo
  echo "options:"
  echo "-h, --help    Print this Help"
  echo "-a, --all     generate all models"
}

# Check if no arguments have been given
if [[ $# -eq 0 ]]; then
  echo -e "No arguments were given, use ${BLUE}./generate_model.sh -h${NC} for the help page."
  exit 1
fi

# Parse given arguments
while [[ $# -gt 0 ]]
do
  key="$1"

  case $key in
    -h|--help) # Print help message
      printHelpMessage
      break
      ;;
    -a|--all) # Generate all models
      generateAllModels
      break
      ;;
    *) # Generate single/multiple models
      generateModel "$key"
      shift
      ;;
  esac
done