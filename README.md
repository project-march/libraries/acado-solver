# acado-solver

acado-solver is a repository containing code that uses the [ACADO Toolkit](http://acado.github.io/) to export optimized, highly efficient C-code to solve a nonlinear model predictive control (NMPC) problem.  

The problem being solved is of a rotational joint that is modelled, for now, as a frictionless actuated point-mass pendulum.

## Installation

### Part 1 - Installing the ACADO Toolkit

Please follow the installation instructions for you desired operating system. 

[Linux](http://acado.github.io/install_linux.html)  
[OS X](http://acado.github.io/install_osx.html)  
[Windows](http://acado.github.io/install_windows.html)  

### Part 2 - Building the acado-solver Repository
In order to build ACADO based executables outside the ACADO source tree you'll need to source the acado environment by either appending the following line to your environment file or by running it in your terminal:
```bash
source <ACADO_ROOT>/build/acado_env.sh
```
where `<ACADO_ROOT>` equals the installation folder of the ACADO Toolkit.

Now type the following commands in your terminal to build the repository:
```bash
mkdir build
cd build
cmake ..
make
cd ..
```

## How to use?

After the build process, executables will appear in the respective model directories. Running these will generate the model_export directory with the acado generated files.

Some executables accept arguments. which arguments can be seen by supplying the executable with the `-h` or `--help` arguments e.g.
```bash
./<model_name> -h
./<model_name> --help
```

### Adding new models
A new model can be added by adding a new directory to the `models/` with the following structure
```bash
<model_name>/
	<model_export>
	<model_name>.cpp
	CMakeLists.txt
```

where
* `<model_name>` should be substituted for the name of your model (e.g. test_joint_linear)
* CMakeLists.txt should contain at least the following
```cmake
# Minimum required version of cmake
CMAKE_MINIMUM_REQUIRED( VERSION 2.8 )

# Project name and programming languages used
PROJECT( <model_name> )

# CMake module(s) path
SET( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR} )

# Include and link directories
INCLUDE_DIRECTORIES( . $ENV{ACADO_ENV_INCLUDE_DIRS} )
link_directories( $ENV{ACADO_ENV_LIBRARY_DIRS} )

# Build mpc_model executable
ADD_EXECUTABLE( ${PROJECT_NAME} ${PROJECT_NAME}.cpp )
TARGET_LINK_LIBRARIES( ${PROJECT_NAME} $ENV{ACADO_ENV_SHARED_LIBRARIES} )
SET_TARGET_PROPERTIES( ${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin )
```
* `<model_name>.cpp` contains the actual code describing the model. Look at other models and the ACADO documentation to see how to make your own version. Be sure to export the files to the `model_export` folder otherwise the model can't be used by the march_acado_mpc package in the main Project MARCH repository.

Don't forget to add your model name to the `MODEL_DIRECTORIES` variable in the main `CMakeLists.txt` e.g.
```cmake
SET( MODEL_DIRECTORIES
     models/test_joint_linear
     models/test_joint_rotational 
     models/<model_name> )
```

## ACADO Toolkit Resources 

The best way to understand the code is by going through the [online ACADO Toolkit tutorials](http://acado.sourceforge.net/doc/html/db/d4e/tutorial.html) and exploring the examples located in the `<ACADO_ROOT>` folder.

A list of all the classes, structs, unions and interfaces, with documentation, can be found here: [Class List](http://acado.sourceforge.net/doc/html/annotated.html).