#include <acado_toolkit.hpp>
#include <acado_gnuplot.hpp>
#include <iostream>
#include <cmath>

#define _USE_MATH_DEFINES

USING_NAMESPACE_ACADO

void simulateModel(DifferentialEquation f, OCP ocp, double Ts, double t_sim) {
    // SETTING UP THE (SIMULATED) PROCESS:
    // -----------------------------------
    OutputFcn identity;
    DynamicSystem dynamicSystem(f, identity);

    Process process(dynamicSystem, INT_RK45);


    // SETTING UP THE MPC CONTROLLER:
    // ------------------------------
    RealTimeAlgorithm alg(ocp, Ts);
    alg.set(INTEGRATOR_TYPE, INT_RK78);

    StaticReferenceTrajectory zeroReference;

    Controller controller(alg, zeroReference);


    // SETTING UP THE SIMULATION ENVIRONMENT,  RUN THE EXAMPLE...
    // ----------------------------------------------------------
    SimulationEnvironment sim(0.0,t_sim,process,controller);

    DVector x0(2);
    x0.setAll(0.0);
    x0(0) = 0.0; // theta
    x0(1) = 0.0; // dtheta

    if (sim.init(x0) != SUCCESSFUL_RETURN)
        exit(EXIT_FAILURE);
    if (sim.run() != SUCCESSFUL_RETURN)
        exit(EXIT_FAILURE);


    // PLOT THE RESULTS
    // ----------------
    VariablesGrid sampledProcessOutput;
    sim.getSampledProcessOutput(sampledProcessOutput);

    VariablesGrid feedbackControl;
    sim.getFeedbackControl(feedbackControl);

    GnuplotWindow window;

    // State theta, Angle
    window.addSubplot(sampledProcessOutput(0), "state, Angle");
    window.setLabelX(0, "time [s]");
    window.setLabelY(0, "rad");

    // State dtheta, Angular Velocity
    window.addSubplot(sampledProcessOutput(1), "state, Angular Velocity");
    window.setLabelX(1, "time [s]");
    window.setLabelY(1, "rad/s");

    // Control T, torque
    window.addSubplot(feedbackControl(0), "input, Effort");
    window.setLabelX(2, "time [s]");
    window.setLabelY(2, "A");

    window.plot();

    exit(EXIT_SUCCESS);
}

void generateMPCModel(OCP ocp) {

    // DEFINE AN MPC EXPORT MODULE AND GENERATE THE CODE:
    // --------------------------------------------------

    OCPexport mpc(ocp);

    // MPC settings
    mpc.set(HESSIAN_APPROXIMATION,          GAUSS_NEWTON);
    mpc.set(DISCRETIZATION_TYPE,            MULTIPLE_SHOOTING);
    mpc.set(SPARSE_QP_SOLUTION,             FULL_CONDENSING);
    mpc.set(INTEGRATOR_TYPE,                INT_RK4);
    mpc.set(QP_SOLVER,                      QP_QPOASES);
    mpc.set(HOTSTART_QP,                    NO);
    mpc.set(LEVENBERG_MARQUARDT,            1e-10);
    mpc.set(CG_HARDCODE_CONSTRAINT_VALUES,  YES);
    mpc.set(USE_SINGLE_PRECISION,           YES);

    // Do not generate tests, makes or matlab-related interfaces.
    mpc.set( GENERATE_TEST_FILE,            NO);
    mpc.set( GENERATE_MAKE_FILE,            NO);
    mpc.set( GENERATE_MATLAB_INTERFACE,     NO);
    mpc.set( GENERATE_SIMULINK_INTERFACE,   NO);

  if (mpc.exportCode( "../model_export" ) != SUCCESSFUL_RETURN) {
        exit(EXIT_FAILURE);
    }

    mpc.printDimensionsQP( );
}

bool generateModelCheck(int argc, char** argv) {
    /*
     * Checks if the mpc model should be generated or simulated based on the given executable arguments.
     * On default this functions returns true but when the executable arguments -s or --simulate are used
     * the functions returns false.
     * There is also a help argument that will exit the program after it has displayed the help page.
     */

    // Check given arguments
    for (int i = 0; i < argc; i++)
    {
        if (!strcmp(argv[i], "-h") || !strcmp(argv[i],"--help")) {
            std::cout
                    << "Usage: ./mpc_model [OPTION]...\n"
                    << "Generates the mpc model files using the ACADO Toolkit code generation when no options are given\n\n"
                    << "Options:\n"
                    << "-s, --simulate" << "\t\t" << "perform a closed-loop mpc simulation\n"
                    << "-h, --help" << "\t\t" << "display this help and exit\n"
                    << std::endl;
            exit(EXIT_SUCCESS);
        }
        else if (!strcmp(argv[i], "-s") || !strcmp(argv[i],"--simulate")) {
            return false;
        }
    }

    return true;
}

int main(int argc, char** argv)
{

    // Switching variable for generating or simulating the mpc
    bool generate_model = generateModelCheck(argc, argv);


    /**
     * ==========================================
     * Define System Variables (States + Control)
     * ==========================================
     */

    // states: linear translation (x [m]) and linear velocity (dx [m/s])
    // intermediate states: linear acceleration (ddx [m/s^2]) and linear drive force (Fd [N])
    // control: Effort (I [A])

    // test_joint_linear
    DifferentialState   x, dx;
    IntermediateState   ddx, Fd;
    Control             I;


    /**
     * ==========
     * General Parameters
     * ==========
     */

    // General Parameters
    double g = 9.81;                        // gravitational constant       // m/s^2

    // OCP Parameters
    const int    N       =   10;            // horizon length               // [ ]
    const int    Ni      =    4;            // integrator amount            // [ ]
    const double Ts      = 0.04;            // sampling time                // [s]
    const double t_end   = N*Ts;            // ocp end time                 // [s]
    const double t_sim   =  1.0;            // simulation time              // [s]

    /**
     * ===============================================
     * Test_joint_linear Parameters and Dynamics
     * ===============================================
     */

    // Base Parameters (test_joint_linear)
    double m_pushrod =  0.30;                // point mass push rod          // kg
    double m_load = 0.0;                    // point mass load              // kg
    double pulley_ratio = 4.0;              // ratio of pulleys

    double pitch = 0.002;
    double rot_to_lin_conversion = (2.0 * M_PI) / pitch;
    double ball_screw_efficiency = 1;

    double kv = 355.0;
    double motor_constant = 8.27/kv;

    // Constraints (test_joint_linear)
    double x_max  = 0.02;   // Maximum joint position      // [m]
    double dx_max = 0.10;   // Maximum joint speed         // [m/s]
    double I_max  = 28.0;   // Maximum torque              // [A]

    // Dynamics (test_joint_linear)
    Fd = (I * motor_constant) * rot_to_lin_conversion * ball_screw_efficiency;
    double Fl = pulley_ratio * m_load * g;
    double Ff = 0.0; // Friction forces, usually a function of velocity

    ddx = (Fd + Fl - Ff*dx)/m_pushrod;


    /**
     * =====================
     * Differential Equation
     * =====================
     */

    // Define differential equation
    DifferentialEquation f;

    // test_joint_linear
    f << dot(x)  == dx;
    f << dot(dx) == ddx;

    /**
     * ========================================================
     * Linear Least Squares (LSQ) Vector and Weighting Matrices
     * ========================================================
     */


    // Joint state expressions (combining all states of a joint under a single object)
    Expression states;
    states << x;
    states << dx;

    // Joint control expressions (combining all controls under two objects)
    Expression control;
    control << I;

    // Running LSQ vector (states + controls)
    Function h;
    h << states;
    h << control;

    // End LSQ vector (only states)
    Function hN;
    hN << states;

    // Running cost matrix
    DMatrix Q(h.getDim(),h.getDim());
    Q.setAll(0.0);
    Q(0,0) = 100.0; // x
    Q(1,1) = 1.0;   // dx
    Q(2,2) = 0.1;   // I

    // End cost matrix
    DMatrix QN(hN.getDim(),hN.getDim());
    QN.setAll(0.0);
    QN(0,0) = Q(0,0); // x
    QN(1,1) = Q(1,1); // dx

    // Running reference
    DVector r(h.getDim());
    r.setAll(0.01);

    // End reference
    DVector rN(h.getDim());
    rN.setAll(0.01);


    /**
     * =============================
     * Optimal Control Problem (OCP)
     * =============================
     */

    OCP ocp(0.0, t_end, N);

    // System dynamics constraint
    ocp.subjectTo(f);

    // state and input constraints
    ocp.subjectTo( -x_max <= x  <=  x_max);
    ocp.subjectTo(-dx_max <= dx <= dx_max);
    ocp.subjectTo( -I_max <= I  <=  I_max);

    // Set the LSQ objective for either simulation or code generation purposes
    if (!generate_model) {
        // The simulation environment requires a reference vector in the LSQ objective
        ocp.minimizeLSQ(Q, h, r);
        ocp.minimizeLSQEndTerm(QN, hN, rN);

        simulateModel(f, ocp, Ts, t_sim);
    } else {

        // Create post model generation modifiable weighting matrices
        BMatrix Q  = eye<bool>(h.getDim());
        BMatrix QN = eye<bool>(hN.getDim());

        // Code generation supports only the standard LSQ objective
        ocp.minimizeLSQ(Q, h);
        ocp.minimizeLSQEndTerm(QN, hN);

        generateMPCModel(ocp);
    }

    return EXIT_SUCCESS;

}