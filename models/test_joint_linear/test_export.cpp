
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <algorithm>

using namespace std;

#include "acado_common.h"
#include "acado_auxiliary_functions.h"

#define NX          ACADO_NX	/* number of differential states */
#define NXA         ACADO_NXA	/* number of alg. states */
#define NU          ACADO_NU	/* number of control inputs */
#define N          	ACADO_N		/* number of control intervals */
#define NY			    ACADO_NY	/* number of references, nodes 0..N - 1 */
#define NYN			    ACADO_NYN
#define NUM_STEPS   100			/* number of simulation steps */

ACADOvariables acadoVariables = {};
ACADOworkspace acadoWorkspace = {};

int main() {
    // Define iteration and timer variables
    unsigned int i, j, iter;
    acado_timer t;
    real_t t1, t2;
    real_t fdbSum = 0.0;
    real_t prepSum = 0.0;
    int status;

    t1 = t2 = 0;

    // Initialize the solver
    acado_initializeSolver();

    // Prepare a consistent initial guess
    for (i = 0; i < N + 1; ++i) {
        acadoVariables.x[i * NX + 0] = 0.0;
        acadoVariables.x[i * NX + 1] = 0.0;
    }

    // Prepare references
    for (i = 0; i < N; ++i) {
        acadoVariables.y[i * NY + 0] = 0.3; // theta
        acadoVariables.y[i * NY + 1] = 0; // dtheta
        acadoVariables.y[i * NY + 2] = 0; // T
    }

    acadoVariables.yN[0] = 0.3; // theta
    acadoVariables.yN[1] = 0; // dtheta

    // Current state feedback
    for (i = 0; i < NX; ++i) {
        acadoVariables.x0[i] = acadoVariables.x[i];
    }

    // Set weighting matrices
    double W_diag[NY] = {100.0, 1.0, 0.5};

    std::fill(std::begin(acadoVariables.W),std::end(acadoVariables.W), 0.0);
    std::fill(std::begin(acadoVariables.WN),std::end(acadoVariables.WN), 0.0);

    for (i = 0; i < NY; ++i) {
        acadoVariables.W[i*(NY+1)] = W_diag[i];
    }

    for (i = 0; i < NYN; ++i) {
        acadoVariables.WN[i*(NYN+1)] = W_diag[i];
    }

    // Logger initialization
    vector<vector<double>> log;

    log.resize(NUM_STEPS);
    for (i = 0; i < log.size(); ++i) {
        log[i].resize(NX + NXA + NU + 5, 0.0);
    }

    // Warm-up the solver
    acado_preparationStep();

    // Real-time iterations loop
    for(iter = 0; iter < NUM_STEPS; iter++) {
        acado_tic(&t);
        status = acado_feedbackStep();
        t2 = acado_toc(&t);

        if (status) {
            cout << "Iteration:" << iter << ", QP problem! QP status: " << status << endl;
            break;
        }

        // Logging

        // Log file column headers
        // [theta (rad), dtheta (rad/s), input_(N/m), preparation_time_(s), feedback_time_(s), objective, KKT, NWSR]

        for(i = 0; i < NX; i++)
            log[iter][i] = acadoVariables.x[i];
        for(j = 0;  i < NX + NXA + NU; i++, j++)
            log[iter][i] = acadoVariables.u[j];

        log[iter][i++] = t1;
        log[iter][i++] = t2;
        log[iter][i++] = acado_getObjective();
        log[iter][i++] = acado_getKKT();
        log[iter][i++] = acado_getNWSR();

        // output iteration data
        cout	<< "Iteration #" << setw( 4 ) << iter
                << ", KKT value: " << scientific << acado_getKKT()
                << ", objective value: " << scientific << acado_getObjective()
                << endl;


        // Prepare for the next iteration

        // Feed the (N)MPC with an ideal feedback signal
        for (i = 0; i < NX; ++i) {
            acadoVariables.x0[i] = acadoVariables.x[NX + i];
        }

        // Shift states and control
        acado_shiftStates(2, 0, 0);
        acado_shiftControls(0);

        // Perform a preparation step for the next iteration
        acado_tic(&t);
        acado_preparationStep();
        t1 = acado_toc(&t);

        // Log the total preparation and feedback time
        prepSum += t1;
        fdbSum += t2;
    }

    // Output the average times in microseconds
    cout << "Average feedback time:    " << scientific << fdbSum / NUM_STEPS * 1e6 << " microseconds" << endl;
    cout << "Average preparation time: " << scientific << prepSum / NUM_STEPS * 1e6 << " microseconds" << endl;


    // Save log to a file

    // Create a timestamp for unique file naming
    time_t now = time(nullptr);
    tm tm = *localtime(&now);

    string timestamp;
    stringstream textStream;

    textStream << put_time(&tm, "%c %Z");
    timestamp = textStream.str();
    replace(timestamp.begin(),timestamp.end(),' ', '_');

    ofstream dataLog( "../model_export/log/model_export_" + timestamp + ".log" );
    if (dataLog.is_open()) {

        for (i = 0; i < log.size(); i++) {
            for (j = 0; j < log[i].size(); j++) {
                dataLog << log[i][j] << " ";
            }
            dataLog << endl;
        }

        dataLog.close();
    }
    else {
        cout << "Log file could not be opened" << endl;
        return 1;
    }

    // Check for solver failure
    if (status) {
        cout << "Solver failed!" << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


