# Minimum required version of cmake
CMAKE_MINIMUM_REQUIRED( VERSION 2.8 )

# Project name and programming languages used
PROJECT( test_joint_linear )

# CMake module(s) path
SET( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR} )

# Include and link directories
INCLUDE_DIRECTORIES( . $ENV{ACADO_ENV_INCLUDE_DIRS} )
link_directories( $ENV{ACADO_ENV_LIBRARY_DIRS} )

# Get all sources in the model_export directory
AUX_SOURCE_DIRECTORY( model_export MODEL_EXPORT_SRCS_LIN )

# Get qpoases directory and all sources
set( QPOASES_DIR $ENV{ACADO_ENV_EXTERNAL_PACKAGES_DIR}/qpoases )
AUX_SOURCE_DIRECTORY( ${QPOASES_DIR}/SRC QPOASES_SRCS )

INCLUDE_DIRECTORIES(
        model_export
        # "lib/qpoases" is needed for acado_qpoases_interface to correctly build
        # as this generated file includes "INCLUDE/QProblem.hpp".
        ${QPOASES_DIR}
        ${QPOASES_DIR}/INCLUDE
        ${QPOASES_DIR}/SRC )

ADD_LIBRARY( MODEL_EXPORT_LIB_LIN
        "${QPOASES_SRCS}"
        "${MODEL_EXPORT_SRCS_LIN}" )


# Build mpc_model executable
ADD_EXECUTABLE( ${PROJECT_NAME} ${PROJECT_NAME}.cpp )
TARGET_LINK_LIBRARIES( ${PROJECT_NAME} $ENV{ACADO_ENV_SHARED_LIBRARIES} )
SET_TARGET_PROPERTIES( ${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin )

# Build test executable
ADD_EXECUTABLE( test_export_lin test_export.cpp)
TARGET_LINK_LIBRARIES( test_export_lin MODEL_EXPORT_LIB_LIN)
SET_TARGET_PROPERTIES( test_export_lin PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin )

# Create log directory in model_export
FILE( MAKE_DIRECTORY model_export/log )

