/*
 *    This file was auto-generated using the ACADO Toolkit.
 *    
 *    While ACADO Toolkit is free software released under the terms of
 *    the GNU Lesser General Public License (LGPL), the generated code
 *    as such remains the property of the user who used ACADO Toolkit
 *    to generate this code. In particular, user dependent data of the code
 *    do not inherit the GNU LGPL license. On the other hand, parts of the
 *    generated code that are a direct copy of source code from the
 *    ACADO Toolkit or the software tools it is based on, remain, as derived
 *    work, automatically covered by the LGPL license.
 *    
 *    ACADO Toolkit is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *    
 */


#include "acado_common.h"


void acado_rhs_forw(const real_t* in, real_t* out)
{
const real_t* xd = in;
const real_t* u = in + 28;
/* Vector of auxiliary variables; number of elements: 170. */
real_t* a = acadoWorkspace.rhs_aux;

/* Compute intermediate quantities: */
a[0] = (((real_t)(1.0636978224025597e+00)-xd[0])-(real_t)(1.9739599999999999e-01));
a[1] = (cos(a[0]));
a[2] = (cos(a[0]));
a[3] = (sqrt(((real_t)(2.6625955225000005e-02)+((real_t)(2.2048915597334944e-02)*a[2]))));
a[4] = (pow((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[1]))/a[3]),2));
a[5] = (sqrt(((real_t)(1.0000000000000000e+00)-a[4])));
a[6] = (((u[0]*(real_t)(2.3295774647887322e-02))*(real_t)(3.1415926535897929e+03))*(real_t)(9.0000000000000002e-01));
a[7] = (cos((xd[0]+(real_t)(1.9739599999999999e-01))));
a[8] = ((real_t)(7.6484999999999997e-02)*a[7]);
a[9] = (xd[0]+(real_t)(3.9689540665727480e-01));
a[10] = (sin(a[9]));
a[11] = ((real_t)(2.0845854014432341e-01)*((((a[5]*a[6])*a[8])-((real_t)(5.0589528876980232e+01)*a[10]))-((real_t)(5.0000000000000000e+00)*xd[1])));
a[12] = (((real_t)(1.0636978224025597e+00)-xd[2])-(real_t)(1.9739599999999999e-01));
a[13] = (cos(a[12]));
a[14] = (cos(a[12]));
a[15] = (sqrt(((real_t)(2.6625955225000005e-02)+((real_t)(2.2048915597334944e-02)*a[14]))));
a[16] = (pow((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[13]))/a[15]),2));
a[17] = (sqrt(((real_t)(1.0000000000000000e+00)-a[16])));
a[18] = (((u[1]*(real_t)(2.3295774647887322e-02))*(real_t)(3.1415926535897929e+03))*(real_t)(9.0000000000000002e-01));
a[19] = (cos((xd[2]+(real_t)(1.9739599999999999e-01))));
a[20] = ((real_t)(7.6484999999999997e-02)*a[19]);
a[21] = (xd[2]+(real_t)(3.9689540665727480e-01));
a[22] = (sin(a[21]));
a[23] = ((real_t)(2.0845854014432341e-01)*((((a[17]*a[18])*a[20])-((real_t)(5.0589528876980232e+01)*a[22]))-((real_t)(5.0000000000000000e+00)*xd[3])));
a[24] = ((real_t)(2.0000000000000000e+00)*(((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[1]))/a[3]));
a[25] = ((real_t)(0.0000000000000000e+00)-xd[4]);
a[26] = ((real_t)(-1.0000000000000000e+00)*(sin(a[0])));
a[27] = (a[25]*a[26]);
a[28] = ((real_t)(1.0000000000000000e+00)/a[3]);
a[29] = ((real_t)(-1.0000000000000000e+00)*(sin(a[0])));
a[30] = (a[25]*a[29]);
a[31] = (1.0/sqrt(((real_t)(2.6625955225000005e-02)+((real_t)(2.2048915597334944e-02)*a[2]))));
a[32] = (a[31]*(real_t)(5.0000000000000000e-01));
a[33] = (((real_t)(2.2048915597334944e-02)*a[30])*a[32]);
a[34] = (a[28]*a[28]);
a[35] = (a[24]*((((real_t)(1.4413882197381803e-01)*a[27])*a[28])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[1]))*a[33])*a[34])));
a[36] = (1.0/sqrt(((real_t)(1.0000000000000000e+00)-a[4])));
a[37] = (a[36]*(real_t)(5.0000000000000000e-01));
a[38] = (((real_t)(0.0000000000000000e+00)-a[35])*a[37]);
a[39] = ((real_t)(-1.0000000000000000e+00)*(sin((xd[0]+(real_t)(1.9739599999999999e-01)))));
a[40] = (xd[4]*a[39]);
a[41] = ((real_t)(7.6484999999999997e-02)*a[40]);
a[42] = (cos(a[9]));
a[43] = (xd[4]*a[42]);
a[44] = ((real_t)(2.0845854014432341e-01)*(((((a[38]*a[6])*a[8])+((a[5]*a[6])*a[41]))-((real_t)(5.0589528876980232e+01)*a[43]))-((real_t)(5.0000000000000000e+00)*xd[8])));
a[45] = ((real_t)(0.0000000000000000e+00)-xd[5]);
a[46] = (a[45]*a[26]);
a[47] = (a[45]*a[29]);
a[48] = (((real_t)(2.2048915597334944e-02)*a[47])*a[32]);
a[49] = (a[24]*((((real_t)(1.4413882197381803e-01)*a[46])*a[28])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[1]))*a[48])*a[34])));
a[50] = (((real_t)(0.0000000000000000e+00)-a[49])*a[37]);
a[51] = (xd[5]*a[39]);
a[52] = ((real_t)(7.6484999999999997e-02)*a[51]);
a[53] = (xd[5]*a[42]);
a[54] = ((real_t)(2.0845854014432341e-01)*(((((a[50]*a[6])*a[8])+((a[5]*a[6])*a[52]))-((real_t)(5.0589528876980232e+01)*a[53]))-((real_t)(5.0000000000000000e+00)*xd[9])));
a[55] = ((real_t)(0.0000000000000000e+00)-xd[6]);
a[56] = (a[55]*a[26]);
a[57] = (a[55]*a[29]);
a[58] = (((real_t)(2.2048915597334944e-02)*a[57])*a[32]);
a[59] = (a[24]*((((real_t)(1.4413882197381803e-01)*a[56])*a[28])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[1]))*a[58])*a[34])));
a[60] = (((real_t)(0.0000000000000000e+00)-a[59])*a[37]);
a[61] = (xd[6]*a[39]);
a[62] = ((real_t)(7.6484999999999997e-02)*a[61]);
a[63] = (xd[6]*a[42]);
a[64] = ((real_t)(2.0845854014432341e-01)*(((((a[60]*a[6])*a[8])+((a[5]*a[6])*a[62]))-((real_t)(5.0589528876980232e+01)*a[63]))-((real_t)(5.0000000000000000e+00)*xd[10])));
a[65] = ((real_t)(0.0000000000000000e+00)-xd[7]);
a[66] = (a[65]*a[26]);
a[67] = (a[65]*a[29]);
a[68] = (((real_t)(2.2048915597334944e-02)*a[67])*a[32]);
a[69] = (a[24]*((((real_t)(1.4413882197381803e-01)*a[66])*a[28])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[1]))*a[68])*a[34])));
a[70] = (((real_t)(0.0000000000000000e+00)-a[69])*a[37]);
a[71] = (xd[7]*a[39]);
a[72] = ((real_t)(7.6484999999999997e-02)*a[71]);
a[73] = (xd[7]*a[42]);
a[74] = ((real_t)(2.0845854014432341e-01)*(((((a[70]*a[6])*a[8])+((a[5]*a[6])*a[72]))-((real_t)(5.0589528876980232e+01)*a[73]))-((real_t)(5.0000000000000000e+00)*xd[11])));
a[75] = ((real_t)(2.0000000000000000e+00)*(((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[13]))/a[15]));
a[76] = ((real_t)(0.0000000000000000e+00)-xd[12]);
a[77] = ((real_t)(-1.0000000000000000e+00)*(sin(a[12])));
a[78] = (a[76]*a[77]);
a[79] = ((real_t)(1.0000000000000000e+00)/a[15]);
a[80] = ((real_t)(-1.0000000000000000e+00)*(sin(a[12])));
a[81] = (a[76]*a[80]);
a[82] = (1.0/sqrt(((real_t)(2.6625955225000005e-02)+((real_t)(2.2048915597334944e-02)*a[14]))));
a[83] = (a[82]*(real_t)(5.0000000000000000e-01));
a[84] = (((real_t)(2.2048915597334944e-02)*a[81])*a[83]);
a[85] = (a[79]*a[79]);
a[86] = (a[75]*((((real_t)(1.4413882197381803e-01)*a[78])*a[79])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[13]))*a[84])*a[85])));
a[87] = (1.0/sqrt(((real_t)(1.0000000000000000e+00)-a[16])));
a[88] = (a[87]*(real_t)(5.0000000000000000e-01));
a[89] = (((real_t)(0.0000000000000000e+00)-a[86])*a[88]);
a[90] = ((real_t)(-1.0000000000000000e+00)*(sin((xd[2]+(real_t)(1.9739599999999999e-01)))));
a[91] = (xd[12]*a[90]);
a[92] = ((real_t)(7.6484999999999997e-02)*a[91]);
a[93] = (cos(a[21]));
a[94] = (xd[12]*a[93]);
a[95] = ((real_t)(2.0845854014432341e-01)*(((((a[89]*a[18])*a[20])+((a[17]*a[18])*a[92]))-((real_t)(5.0589528876980232e+01)*a[94]))-((real_t)(5.0000000000000000e+00)*xd[16])));
a[96] = ((real_t)(0.0000000000000000e+00)-xd[13]);
a[97] = (a[96]*a[77]);
a[98] = (a[96]*a[80]);
a[99] = (((real_t)(2.2048915597334944e-02)*a[98])*a[83]);
a[100] = (a[75]*((((real_t)(1.4413882197381803e-01)*a[97])*a[79])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[13]))*a[99])*a[85])));
a[101] = (((real_t)(0.0000000000000000e+00)-a[100])*a[88]);
a[102] = (xd[13]*a[90]);
a[103] = ((real_t)(7.6484999999999997e-02)*a[102]);
a[104] = (xd[13]*a[93]);
a[105] = ((real_t)(2.0845854014432341e-01)*(((((a[101]*a[18])*a[20])+((a[17]*a[18])*a[103]))-((real_t)(5.0589528876980232e+01)*a[104]))-((real_t)(5.0000000000000000e+00)*xd[17])));
a[106] = ((real_t)(0.0000000000000000e+00)-xd[14]);
a[107] = (a[106]*a[77]);
a[108] = (a[106]*a[80]);
a[109] = (((real_t)(2.2048915597334944e-02)*a[108])*a[83]);
a[110] = (a[75]*((((real_t)(1.4413882197381803e-01)*a[107])*a[79])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[13]))*a[109])*a[85])));
a[111] = (((real_t)(0.0000000000000000e+00)-a[110])*a[88]);
a[112] = (xd[14]*a[90]);
a[113] = ((real_t)(7.6484999999999997e-02)*a[112]);
a[114] = (xd[14]*a[93]);
a[115] = ((real_t)(2.0845854014432341e-01)*(((((a[111]*a[18])*a[20])+((a[17]*a[18])*a[113]))-((real_t)(5.0589528876980232e+01)*a[114]))-((real_t)(5.0000000000000000e+00)*xd[18])));
a[116] = ((real_t)(0.0000000000000000e+00)-xd[15]);
a[117] = (a[116]*a[77]);
a[118] = (a[116]*a[80]);
a[119] = (((real_t)(2.2048915597334944e-02)*a[118])*a[83]);
a[120] = (a[75]*((((real_t)(1.4413882197381803e-01)*a[117])*a[79])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[13]))*a[119])*a[85])));
a[121] = (((real_t)(0.0000000000000000e+00)-a[120])*a[88]);
a[122] = (xd[15]*a[90]);
a[123] = ((real_t)(7.6484999999999997e-02)*a[122]);
a[124] = (xd[15]*a[93]);
a[125] = ((real_t)(2.0845854014432341e-01)*(((((a[121]*a[18])*a[20])+((a[17]*a[18])*a[123]))-((real_t)(5.0589528876980232e+01)*a[124]))-((real_t)(5.0000000000000000e+00)*xd[19])));
a[126] = ((real_t)(0.0000000000000000e+00)-xd[20]);
a[127] = (a[126]*a[26]);
a[128] = (a[126]*a[29]);
a[129] = (((real_t)(2.2048915597334944e-02)*a[128])*a[32]);
a[130] = (a[24]*((((real_t)(1.4413882197381803e-01)*a[127])*a[28])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[1]))*a[129])*a[34])));
a[131] = (((real_t)(0.0000000000000000e+00)-a[130])*a[37]);
a[132] = (xd[20]*a[39]);
a[133] = ((real_t)(7.6484999999999997e-02)*a[132]);
a[134] = (xd[20]*a[42]);
a[135] = ((real_t)(2.0845854014432341e-01)*(((((a[131]*a[6])*a[8])+((a[5]*a[6])*a[133]))-((real_t)(5.0589528876980232e+01)*a[134]))-((real_t)(5.0000000000000000e+00)*xd[22])));
a[136] = ((real_t)(6.5867251044137547e+01));
a[137] = ((real_t)(2.0845854014432341e-01)*((a[5]*a[136])*a[8]));
a[138] = ((real_t)(0.0000000000000000e+00)-xd[21]);
a[139] = (a[138]*a[26]);
a[140] = (a[138]*a[29]);
a[141] = (((real_t)(2.2048915597334944e-02)*a[140])*a[32]);
a[142] = (a[24]*((((real_t)(1.4413882197381803e-01)*a[139])*a[28])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[1]))*a[141])*a[34])));
a[143] = (((real_t)(0.0000000000000000e+00)-a[142])*a[37]);
a[144] = (xd[21]*a[39]);
a[145] = ((real_t)(7.6484999999999997e-02)*a[144]);
a[146] = (xd[21]*a[42]);
a[147] = ((real_t)(2.0845854014432341e-01)*(((((a[143]*a[6])*a[8])+((a[5]*a[6])*a[145]))-((real_t)(5.0589528876980232e+01)*a[146]))-((real_t)(5.0000000000000000e+00)*xd[23])));
a[148] = ((real_t)(0.0000000000000000e+00)-xd[24]);
a[149] = (a[148]*a[77]);
a[150] = (a[148]*a[80]);
a[151] = (((real_t)(2.2048915597334944e-02)*a[150])*a[83]);
a[152] = (a[75]*((((real_t)(1.4413882197381803e-01)*a[149])*a[79])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[13]))*a[151])*a[85])));
a[153] = (((real_t)(0.0000000000000000e+00)-a[152])*a[88]);
a[154] = (xd[24]*a[90]);
a[155] = ((real_t)(7.6484999999999997e-02)*a[154]);
a[156] = (xd[24]*a[93]);
a[157] = ((real_t)(2.0845854014432341e-01)*(((((a[153]*a[18])*a[20])+((a[17]*a[18])*a[155]))-((real_t)(5.0589528876980232e+01)*a[156]))-((real_t)(5.0000000000000000e+00)*xd[26])));
a[158] = ((real_t)(0.0000000000000000e+00)-xd[25]);
a[159] = (a[158]*a[77]);
a[160] = (a[158]*a[80]);
a[161] = (((real_t)(2.2048915597334944e-02)*a[160])*a[83]);
a[162] = (a[75]*((((real_t)(1.4413882197381803e-01)*a[159])*a[79])-((((real_t)(7.6484999999999997e-02)+((real_t)(1.4413882197381803e-01)*a[13]))*a[161])*a[85])));
a[163] = (((real_t)(0.0000000000000000e+00)-a[162])*a[88]);
a[164] = (xd[25]*a[90]);
a[165] = ((real_t)(7.6484999999999997e-02)*a[164]);
a[166] = (xd[25]*a[93]);
a[167] = ((real_t)(2.0845854014432341e-01)*(((((a[163]*a[18])*a[20])+((a[17]*a[18])*a[165]))-((real_t)(5.0589528876980232e+01)*a[166]))-((real_t)(5.0000000000000000e+00)*xd[27])));
a[168] = ((real_t)(6.5867251044137547e+01));
a[169] = ((real_t)(2.0845854014432341e-01)*((a[17]*a[168])*a[20]));

/* Compute outputs: */
out[0] = xd[1];
out[1] = a[11];
out[2] = xd[3];
out[3] = a[23];
out[4] = xd[8];
out[5] = xd[9];
out[6] = xd[10];
out[7] = xd[11];
out[8] = a[44];
out[9] = a[54];
out[10] = a[64];
out[11] = a[74];
out[12] = xd[16];
out[13] = xd[17];
out[14] = xd[18];
out[15] = xd[19];
out[16] = a[95];
out[17] = a[105];
out[18] = a[115];
out[19] = a[125];
out[20] = xd[22];
out[21] = xd[23];
out[22] = (a[135]+a[137]);
out[23] = a[147];
out[24] = xd[26];
out[25] = xd[27];
out[26] = a[157];
out[27] = (a[167]+a[169]);
}

/* Fixed step size:0.004 */
int acado_integrate( real_t* const rk_eta, int resetIntegrator )
{
int error;

int run1;
acadoWorkspace.rk_ttt = 0.0000000000000000e+00;
rk_eta[4] = 1.0000000000000000e+00;
rk_eta[5] = 0.0000000000000000e+00;
rk_eta[6] = 0.0000000000000000e+00;
rk_eta[7] = 0.0000000000000000e+00;
rk_eta[8] = 0.0000000000000000e+00;
rk_eta[9] = 1.0000000000000000e+00;
rk_eta[10] = 0.0000000000000000e+00;
rk_eta[11] = 0.0000000000000000e+00;
rk_eta[12] = 0.0000000000000000e+00;
rk_eta[13] = 0.0000000000000000e+00;
rk_eta[14] = 1.0000000000000000e+00;
rk_eta[15] = 0.0000000000000000e+00;
rk_eta[16] = 0.0000000000000000e+00;
rk_eta[17] = 0.0000000000000000e+00;
rk_eta[18] = 0.0000000000000000e+00;
rk_eta[19] = 1.0000000000000000e+00;
rk_eta[20] = 0.0000000000000000e+00;
rk_eta[21] = 0.0000000000000000e+00;
rk_eta[22] = 0.0000000000000000e+00;
rk_eta[23] = 0.0000000000000000e+00;
rk_eta[24] = 0.0000000000000000e+00;
rk_eta[25] = 0.0000000000000000e+00;
rk_eta[26] = 0.0000000000000000e+00;
rk_eta[27] = 0.0000000000000000e+00;
acadoWorkspace.rk_xxx[28] = rk_eta[28];
acadoWorkspace.rk_xxx[29] = rk_eta[29];

for (run1 = 0; run1 < 2; ++run1)
{
acadoWorkspace.rk_xxx[0] = + rk_eta[0];
acadoWorkspace.rk_xxx[1] = + rk_eta[1];
acadoWorkspace.rk_xxx[2] = + rk_eta[2];
acadoWorkspace.rk_xxx[3] = + rk_eta[3];
acadoWorkspace.rk_xxx[4] = + rk_eta[4];
acadoWorkspace.rk_xxx[5] = + rk_eta[5];
acadoWorkspace.rk_xxx[6] = + rk_eta[6];
acadoWorkspace.rk_xxx[7] = + rk_eta[7];
acadoWorkspace.rk_xxx[8] = + rk_eta[8];
acadoWorkspace.rk_xxx[9] = + rk_eta[9];
acadoWorkspace.rk_xxx[10] = + rk_eta[10];
acadoWorkspace.rk_xxx[11] = + rk_eta[11];
acadoWorkspace.rk_xxx[12] = + rk_eta[12];
acadoWorkspace.rk_xxx[13] = + rk_eta[13];
acadoWorkspace.rk_xxx[14] = + rk_eta[14];
acadoWorkspace.rk_xxx[15] = + rk_eta[15];
acadoWorkspace.rk_xxx[16] = + rk_eta[16];
acadoWorkspace.rk_xxx[17] = + rk_eta[17];
acadoWorkspace.rk_xxx[18] = + rk_eta[18];
acadoWorkspace.rk_xxx[19] = + rk_eta[19];
acadoWorkspace.rk_xxx[20] = + rk_eta[20];
acadoWorkspace.rk_xxx[21] = + rk_eta[21];
acadoWorkspace.rk_xxx[22] = + rk_eta[22];
acadoWorkspace.rk_xxx[23] = + rk_eta[23];
acadoWorkspace.rk_xxx[24] = + rk_eta[24];
acadoWorkspace.rk_xxx[25] = + rk_eta[25];
acadoWorkspace.rk_xxx[26] = + rk_eta[26];
acadoWorkspace.rk_xxx[27] = + rk_eta[27];
acado_rhs_forw( acadoWorkspace.rk_xxx, acadoWorkspace.rk_kkk );
acadoWorkspace.rk_xxx[0] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[0] + rk_eta[0];
acadoWorkspace.rk_xxx[1] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[1] + rk_eta[1];
acadoWorkspace.rk_xxx[2] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[2] + rk_eta[2];
acadoWorkspace.rk_xxx[3] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[3] + rk_eta[3];
acadoWorkspace.rk_xxx[4] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[4] + rk_eta[4];
acadoWorkspace.rk_xxx[5] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[5] + rk_eta[5];
acadoWorkspace.rk_xxx[6] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[6] + rk_eta[6];
acadoWorkspace.rk_xxx[7] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[7] + rk_eta[7];
acadoWorkspace.rk_xxx[8] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[8] + rk_eta[8];
acadoWorkspace.rk_xxx[9] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[9] + rk_eta[9];
acadoWorkspace.rk_xxx[10] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[10] + rk_eta[10];
acadoWorkspace.rk_xxx[11] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[11] + rk_eta[11];
acadoWorkspace.rk_xxx[12] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[12] + rk_eta[12];
acadoWorkspace.rk_xxx[13] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[13] + rk_eta[13];
acadoWorkspace.rk_xxx[14] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[14] + rk_eta[14];
acadoWorkspace.rk_xxx[15] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[15] + rk_eta[15];
acadoWorkspace.rk_xxx[16] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[16] + rk_eta[16];
acadoWorkspace.rk_xxx[17] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[17] + rk_eta[17];
acadoWorkspace.rk_xxx[18] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[18] + rk_eta[18];
acadoWorkspace.rk_xxx[19] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[19] + rk_eta[19];
acadoWorkspace.rk_xxx[20] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[20] + rk_eta[20];
acadoWorkspace.rk_xxx[21] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[21] + rk_eta[21];
acadoWorkspace.rk_xxx[22] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[22] + rk_eta[22];
acadoWorkspace.rk_xxx[23] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[23] + rk_eta[23];
acadoWorkspace.rk_xxx[24] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[24] + rk_eta[24];
acadoWorkspace.rk_xxx[25] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[25] + rk_eta[25];
acadoWorkspace.rk_xxx[26] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[26] + rk_eta[26];
acadoWorkspace.rk_xxx[27] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[27] + rk_eta[27];
acado_rhs_forw( acadoWorkspace.rk_xxx, &(acadoWorkspace.rk_kkk[ 28 ]) );
acadoWorkspace.rk_xxx[0] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[28] + rk_eta[0];
acadoWorkspace.rk_xxx[1] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[29] + rk_eta[1];
acadoWorkspace.rk_xxx[2] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[30] + rk_eta[2];
acadoWorkspace.rk_xxx[3] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[31] + rk_eta[3];
acadoWorkspace.rk_xxx[4] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[32] + rk_eta[4];
acadoWorkspace.rk_xxx[5] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[33] + rk_eta[5];
acadoWorkspace.rk_xxx[6] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[34] + rk_eta[6];
acadoWorkspace.rk_xxx[7] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[35] + rk_eta[7];
acadoWorkspace.rk_xxx[8] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[36] + rk_eta[8];
acadoWorkspace.rk_xxx[9] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[37] + rk_eta[9];
acadoWorkspace.rk_xxx[10] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[38] + rk_eta[10];
acadoWorkspace.rk_xxx[11] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[39] + rk_eta[11];
acadoWorkspace.rk_xxx[12] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[40] + rk_eta[12];
acadoWorkspace.rk_xxx[13] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[41] + rk_eta[13];
acadoWorkspace.rk_xxx[14] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[42] + rk_eta[14];
acadoWorkspace.rk_xxx[15] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[43] + rk_eta[15];
acadoWorkspace.rk_xxx[16] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[44] + rk_eta[16];
acadoWorkspace.rk_xxx[17] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[45] + rk_eta[17];
acadoWorkspace.rk_xxx[18] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[46] + rk_eta[18];
acadoWorkspace.rk_xxx[19] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[47] + rk_eta[19];
acadoWorkspace.rk_xxx[20] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[48] + rk_eta[20];
acadoWorkspace.rk_xxx[21] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[49] + rk_eta[21];
acadoWorkspace.rk_xxx[22] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[50] + rk_eta[22];
acadoWorkspace.rk_xxx[23] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[51] + rk_eta[23];
acadoWorkspace.rk_xxx[24] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[52] + rk_eta[24];
acadoWorkspace.rk_xxx[25] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[53] + rk_eta[25];
acadoWorkspace.rk_xxx[26] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[54] + rk_eta[26];
acadoWorkspace.rk_xxx[27] = + (real_t)2.0000000000000000e-03*acadoWorkspace.rk_kkk[55] + rk_eta[27];
acado_rhs_forw( acadoWorkspace.rk_xxx, &(acadoWorkspace.rk_kkk[ 56 ]) );
acadoWorkspace.rk_xxx[0] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[56] + rk_eta[0];
acadoWorkspace.rk_xxx[1] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[57] + rk_eta[1];
acadoWorkspace.rk_xxx[2] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[58] + rk_eta[2];
acadoWorkspace.rk_xxx[3] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[59] + rk_eta[3];
acadoWorkspace.rk_xxx[4] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[60] + rk_eta[4];
acadoWorkspace.rk_xxx[5] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[61] + rk_eta[5];
acadoWorkspace.rk_xxx[6] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[62] + rk_eta[6];
acadoWorkspace.rk_xxx[7] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[63] + rk_eta[7];
acadoWorkspace.rk_xxx[8] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[64] + rk_eta[8];
acadoWorkspace.rk_xxx[9] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[65] + rk_eta[9];
acadoWorkspace.rk_xxx[10] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[66] + rk_eta[10];
acadoWorkspace.rk_xxx[11] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[67] + rk_eta[11];
acadoWorkspace.rk_xxx[12] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[68] + rk_eta[12];
acadoWorkspace.rk_xxx[13] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[69] + rk_eta[13];
acadoWorkspace.rk_xxx[14] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[70] + rk_eta[14];
acadoWorkspace.rk_xxx[15] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[71] + rk_eta[15];
acadoWorkspace.rk_xxx[16] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[72] + rk_eta[16];
acadoWorkspace.rk_xxx[17] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[73] + rk_eta[17];
acadoWorkspace.rk_xxx[18] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[74] + rk_eta[18];
acadoWorkspace.rk_xxx[19] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[75] + rk_eta[19];
acadoWorkspace.rk_xxx[20] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[76] + rk_eta[20];
acadoWorkspace.rk_xxx[21] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[77] + rk_eta[21];
acadoWorkspace.rk_xxx[22] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[78] + rk_eta[22];
acadoWorkspace.rk_xxx[23] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[79] + rk_eta[23];
acadoWorkspace.rk_xxx[24] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[80] + rk_eta[24];
acadoWorkspace.rk_xxx[25] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[81] + rk_eta[25];
acadoWorkspace.rk_xxx[26] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[82] + rk_eta[26];
acadoWorkspace.rk_xxx[27] = + (real_t)4.0000000000000001e-03*acadoWorkspace.rk_kkk[83] + rk_eta[27];
acado_rhs_forw( acadoWorkspace.rk_xxx, &(acadoWorkspace.rk_kkk[ 84 ]) );
rk_eta[0] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[0] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[28] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[56] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[84];
rk_eta[1] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[1] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[29] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[57] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[85];
rk_eta[2] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[2] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[30] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[58] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[86];
rk_eta[3] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[3] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[31] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[59] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[87];
rk_eta[4] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[4] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[32] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[60] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[88];
rk_eta[5] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[5] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[33] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[61] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[89];
rk_eta[6] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[6] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[34] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[62] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[90];
rk_eta[7] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[7] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[35] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[63] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[91];
rk_eta[8] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[8] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[36] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[64] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[92];
rk_eta[9] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[9] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[37] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[65] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[93];
rk_eta[10] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[10] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[38] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[66] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[94];
rk_eta[11] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[11] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[39] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[67] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[95];
rk_eta[12] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[12] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[40] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[68] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[96];
rk_eta[13] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[13] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[41] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[69] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[97];
rk_eta[14] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[14] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[42] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[70] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[98];
rk_eta[15] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[15] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[43] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[71] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[99];
rk_eta[16] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[16] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[44] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[72] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[100];
rk_eta[17] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[17] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[45] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[73] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[101];
rk_eta[18] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[18] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[46] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[74] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[102];
rk_eta[19] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[19] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[47] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[75] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[103];
rk_eta[20] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[20] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[48] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[76] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[104];
rk_eta[21] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[21] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[49] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[77] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[105];
rk_eta[22] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[22] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[50] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[78] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[106];
rk_eta[23] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[23] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[51] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[79] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[107];
rk_eta[24] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[24] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[52] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[80] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[108];
rk_eta[25] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[25] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[53] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[81] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[109];
rk_eta[26] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[26] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[54] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[82] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[110];
rk_eta[27] += + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[27] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[55] + (real_t)1.3333333333333333e-03*acadoWorkspace.rk_kkk[83] + (real_t)6.6666666666666664e-04*acadoWorkspace.rk_kkk[111];
acadoWorkspace.rk_ttt += 5.0000000000000000e-01;
}
error = 0;
return error;
}

