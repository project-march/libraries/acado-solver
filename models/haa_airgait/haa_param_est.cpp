// ACADO Toolkit
#include <acado_toolkit.hpp>
#include <acado_gnuplot.hpp>

// Other
#include <vector>
#include <iostream>
#include <cmath>


#define _USE_MATH_DEFINES

// Use ACADO
USING_NAMESPACE_ACADO

// Function to calculate the distance to a point using the horizontal and vertical distances
double dist(double hor, double ver) {

  double dist_ = sqrt(hor*hor + ver*ver);

  return dist_;
}

int main(int argc, char *argv[])
{

  /**
   * ==========================================
   * Define System Variables (States + Control)
   * ==========================================
   */


  // states: angle (theta [rad]) and velocity (dtheta [rad/s])
  // intermediate state: acceleration (ddtheta [rad/s^2])
  // control: effort (u [A])


  // left_hip_aa
  DifferentialState theta_haa,  dtheta_haa;
  IntermediateState ddtheta_haa, phi_haa, F_haa,
  d1_haa, beta_haa, a_haa, C_haa,   // HAA Lever Arm Left
  inv_Ixx_haa;                      // Parameter estimation
  Control           u_haa;


  /**
   * ==========
   * General Parameters
   * ==========
   */

  // General Parameters
  double g = 9.81;                        // gravitational constant       // m/s^2

  // Linear Actuator Parameters
  double lin_pitch = 0.002;
  double rot_to_lin_conversion = (2.0 * M_PI) / lin_pitch;
  double ball_screw_efficiency = 0.9;

  double lin_kv = 355.0;
  double lin_motor_constant = 8.27/lin_kv;

  // HAA Lever Arm Parameters
  // https://confluence.projectmarch.nl:8443/display/62tech/Measurements+Backplate
  double d2 = 0.126; // j
  double d3 = 0.070; // p
  double b = dist(d2, d3);
  double c = 0.076485; // sqrt(0.015^2 + r^2) = 0.076485
  double psi_haa = 0.197396; // atan(0.015/r) = 0.197396


  /**
 * ============================================
 * Left/Right Hip (HAA) Parameters and Dynamics
 * ============================================
 */

  // constant Parameters
  double com_y_haa = -0.117469; // Center of mass x coordinate [m]
  double com_z_haa = -0.280263; // Center of mass z coordinate [m]

//  double L_haa =
//    dist(com_y_haa, com_z_haa);             // Straight line distance to center of mass [m]
  double theta_0_haa =
    atan(com_y_haa/com_z_haa);              // Angle of x-axis to line L [rad]

  // parameters to be estimated
  Parameter mass_haa;     // 16.97
  Parameter Ixx_com_haa;  // 3.23
  Parameter L_haa;
//  Parameter Tf_haa;

  // parameter calculations (HAA)
  inv_Ixx_haa = 1/(Ixx_com_haa + mass_haa*L_haa*L_haa); // Inverse inertia around axis or rotation [(kgm^2)^-1]

  // constraints (HAA)
  double theta_lb_haa = -0.1848;  // Lower bound on the left HAA joint angle [rad]
  double theta_ub_haa = 0.1680;   // Upper bound on the left HAA joint angle [rad]

  double dtheta_max_haa = 3.0;      // Maximum joint speed [rad/s]
  double ddtheta_max_haa = 2.0;     // Maximum joint acceleration [rad/s^2]
  double u_max_haa = 30.0;          // Maximum joint torque [A]

  // Force exerted by the linear actuator given input effort
  F_haa = (u_haa * lin_motor_constant) * rot_to_lin_conversion * ball_screw_efficiency;

  // HAA Lever Arm Dynamics (Left)
  d1_haa = c*cos(theta_haa + psi_haa);                          // Lever arm length [m]
  beta_haa = M_PI/2 - atan(d3/d2) - theta_haa - psi_haa;        // Angle of actuator triangle [rad]
  a_haa = sqrt(b*b + c*c + 2*b*c*cos(beta_haa));                // Linear actuator length [m]
  C_haa = sqrt(1 - pow((c+b*cos(beta_haa))/(a_haa), 2));      // Effective force coefficient

  // Total Dynamics (HAA)
  double Tf_haa = 0.0;  // Dynamic Friction coefficient left HAA
  phi_haa = theta_haa + theta_0_haa;
  ddtheta_haa = inv_Ixx_haa * (C_haa * F_haa * d1_haa - mass_haa * g * L_haa * sin(phi_haa) - Tf_haa * dtheta_haa);


  /**
   * =====================
   * Differential Equation
   * =====================
   */

  // Define differential equation
  DifferentialEquation f;

  // left_hip_aa
  f << dot(theta_haa)  == dtheta_haa;
  f << dot(dtheta_haa) == ddtheta_haa;

  // NOTE: "f" needs to have the same order/sequence as "h"


  /**
   * ========================================================
   * Linear Least Squares (LSQ) Vector and Weighting Matrices
   * ========================================================
   */

  // Running LSQ vector (states + controls)
  Function h;
  h << theta_haa  << dtheta_haa;
  h << u_haa;

  // Running weight vectors
  // Using same tuning for left and right leg
  std::vector<double> Q_haa  = {1.0, 0.001}; // Cost on haa states
  std::vector<double> R      = {1e9}; // Cost on controls

  // Combine running weight vectors into a single vector W_diag
  // representing the values on the diagonal of the weights matrix
  // W_diag = {Q_left, Q_right, R_left, R_right}
  std::vector<double> W_diag;
  W_diag.insert(W_diag.end(), Q_haa.begin(), Q_haa.end());
  W_diag.insert(W_diag.end(), R.begin(), R.end());

  // Set running weights
  DMatrix W(h.getDim(), h.getDim());
  W.setAll(0.0);
  for (int i = 0; i < h.getDim(); ++i) {
    W(i,i) = W_diag[i];
  }


/**
 * =====================
 * READ MEASUREMENT DATA
 * =====================
 */

  // get file path from executable argument
  char *filePath = argv[1];

  VariablesGrid measurements;
  measurements.read(filePath);

  if( measurements.isEmpty() == BT_TRUE ) {
    printf("The file \"%s\" can't be opened or doesn't exists.", filePath);
    return 0;
  }


  /**
   * =============================
   * Optimal Control Problem (OCP)
   * =============================
   */

  OCP ocp(measurements.getTimePoints());

  // System dynamics constraint
  ocp.minimizeLSQ(W, h, measurements);
  ocp.subjectTo(f);

//  // HAA constraints
  ocp.subjectTo(theta_lb_haa    <= theta_haa  <= theta_ub_haa);
  ocp.subjectTo(-dtheta_max_haa <= dtheta_haa <= dtheta_max_haa);
  ocp.subjectTo(-u_max_haa      <= u_haa      <= u_max_haa);

  // Parameter constraints
  ocp.subjectTo(16.97 <= mass_haa <= 25.0);
  ocp.subjectTo(3.23  <= Ixx_com_haa <= 20.0);
  ocp.subjectTo(0.0   <= L_haa <= 0.5);
//  ocp.subjectTo(0.0   <= Tf_haa <= 5.0);

//  ocp.subjectTo('AT_START', theta_haa == 0.0);
//  ocp.subjectTo('AT_START', dtheta_haa == 0.0);

  /**
   * ============================
   * PERFORM PARAMETER ESTIMATION
   * ============================
   */

  // create parameter estimation algorithm from the ocp object
  ParameterEstimationAlgorithm algorithm(ocp);

  // initialise the applied control signal

  // Use the following settings
//  algorithm.set(INTEGRATOR_TYPE, INT_LYAPUNOV45);
//  algorithm.set(ABSOLUTE_TOLERANCE, 1e-6);
//  algorithm.set(INTEGRATOR_TOLERANCE, 1e-6);

  // solve the parameter estimation problem
  algorithm.solve();


  /**
   * =================================================
   * GET RESULTS OF THE PARAMETER ESTIMATION ALGORITHM
   * =================================================
   */

  // retrieve estimated parameters
  DVector params;
  algorithm.getParameters(params);

  DMatrix var;
  algorithm.getParameterVarianceCovariance(var);

  // print results to the terminal and to a .txt file
  params.print("../data/params.txt");

  printf("\n\nResults for the parameters: \n");
  printf("-----------------------------------------------\n");
  printf("  m  =  %.3e +/- %.3e \n", params(0,0), sqrt( var(0,0) ) );
  printf("  I  =  %.3e +/- %.3e \n", params(1), sqrt( var(1,1) ) );
  printf("  L  =  %.3e +/- %.3e \n", params(2), sqrt( var(2,2) ) );
  printf("-----------------------------------------------\n\n\n");

  // get states and inputs
  VariablesGrid states;
  algorithm.getDifferentialStates(states);

  VariablesGrid input;
  algorithm.getControls(input);

  // plot the results
  GnuplotWindow window0;
  window0.addSubplot(states(0), "State, Angle", "time [s]", "angle [rad]");
  window0.addSubplot(measurements(0), "State, Angle", "time [s]", "angle [rad]", PM_POINTS);
  window0.plot();

  GnuplotWindow window1;
  window1.addSubplot(states(1), "State, Angular velocity", "time [s]", "velocity [rad/s]");
  window1.addSubplot(measurements(1), "State, Angular velocity", "time [s]", "velocity [rad/s]", PM_POINTS);
  window1.plot();

  GnuplotWindow window2;
  window2.addSubplot( input(0), "Input, Motor Torque", "time [s]", "Torque [Nm]");
  window2.addSubplot(measurements(2),"Input, Motor Torque", "time [s]", "Torque [Nm]", PM_POINTS);
  window2.plot();

  return EXIT_SUCCESS;

}

//[	1.1970000000000724e+01	]
//[	1.6362400753761290e+00	]

//Estimated Parameter Values
//[       2.0000000000000000e+01  ]
//[       3.2300000000000013e+00  ]
//[       1.2778243658613900e-01  ]
//[       5.0000000000007283e+00  ]

//[       1.6969999999999999e+01  ]
//[       5.0000000000000009e+00  ]
//[       1.7481553652835755e-01  ]
//[       5.0000000000008242e+00  ]
