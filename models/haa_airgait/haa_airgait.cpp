// ACADO Toolkit
#include <acado_toolkit.hpp>
#include <acado_gnuplot.hpp>

// Other
#include <vector>
#include <iostream>
#include <cmath>


#define _USE_MATH_DEFINES

// Use ACADO
USING_NAMESPACE_ACADO

// Function to calculate the distance to a point using the horizontal and vertical distances
double dist(double hor, double ver) {

  double dist_ = sqrt(hor*hor + ver*ver);

  return dist_;
}


void simulateModel(DifferentialEquation f, OCP ocp, double dt, double t_sim) {

  // setting up the simulated process
  OutputFcn identity;
  DynamicSystem dynamicSystem(f, identity);

  Process process(dynamicSystem, INT_RK45);


  // Setting up the MPC controller
  RealTimeAlgorithm alg(ocp, dt);
  alg.set(INTEGRATOR_TYPE, INT_RK78);

  StaticReferenceTrajectory zeroReference;

  Controller controller(alg, zeroReference);

  // setting up the simulation environment
  SimulationEnvironment sim(0.0, t_sim, process, controller);

  DVector x0(f.getNX());
  x0.setAll(0.0);
  x0(0) = 0.0;
  x0(2) = 0.0;

  if (sim.init(x0) != SUCCESSFUL_RETURN)
    exit(EXIT_FAILURE);
  if (sim.run() != SUCCESSFUL_RETURN)
    exit(EXIT_FAILURE);


  // plot the results
  VariablesGrid differentialStates;
  sim.getProcessDifferentialStates(differentialStates);

  VariablesGrid feedbackControl;
  sim.getFeedbackControl(feedbackControl);

  VariablesGrid intermediateStates;
  sim.getProcessIntermediateStates(intermediateStates);

  // Plot the joint angles, velocities and torques
  GnuplotWindow window_pos, window_vel, window_torque;
  for (int i = 0; i < f.getNX()/2; ++i) {
    // State theta, angle
    window_pos.addSubplot(differentialStates(2*i), "State, Angle");
    window_pos.setLabelX(i,"time [s]");
    window_pos.setLabelY(i,"rad");

    // State dtheta, Angular Velocity
    window_vel.addSubplot(differentialStates(2*i + 1), "State, Angular Velocity");
    window_vel.setLabelX(i, "time [s]");
    window_vel.setLabelY(i, "rad/s");

    // Control T, torque
    window_torque.addSubplot(feedbackControl(i), "input, Effort");
    window_torque.setLabelX(i, "time [s]");
    window_torque.setLabelY(i, "A");
  }
  window_pos.plot();
  window_vel.plot();
  window_torque.plot();

  exit(EXIT_SUCCESS);

}

void generateMPCModel(OCP ocp) {

  // define an MPC export module and generate the code
  OCPexport mpc(ocp);

  // MPC settings
  mpc.set(HESSIAN_APPROXIMATION,          GAUSS_NEWTON);
  mpc.set(DISCRETIZATION_TYPE,            MULTIPLE_SHOOTING);
  mpc.set(SPARSE_QP_SOLUTION,             FULL_CONDENSING);
  mpc.set(INTEGRATOR_TYPE,                INT_RK4);
  mpc.set(QP_SOLVER,                      QP_QPOASES);
  mpc.set(HOTSTART_QP,                    NO);
  mpc.set(LEVENBERG_MARQUARDT,            1e-10);
  mpc.set(CG_HARDCODE_CONSTRAINT_VALUES,  YES);
  mpc.set(USE_SINGLE_PRECISION,           YES);

  // Do not generate tests, makes or matlab-related interfaces.
  mpc.set( GENERATE_TEST_FILE,            NO);
  mpc.set( GENERATE_MAKE_FILE,            NO);
  mpc.set( GENERATE_MATLAB_INTERFACE,     NO);
  mpc.set( GENERATE_SIMULINK_INTERFACE,   NO);

  if (mpc.exportCode( "../model_export" ) != SUCCESSFUL_RETURN) {
    exit(EXIT_FAILURE);
  }

  mpc.printDimensionsQP( );
}

bool generateModelCheck(int argc, char** argv) {
  /*
   * Checks if the mpc model should be generated or simulated based on the given executable arguments.
   * On default this functions returns true but when the executable arguments -s or --simulate are used
   * the functions returns false.
   * There is also a help argument that will exit the program after it has displayed the help page.
   */

  // Check given arguments
  for (int i = 0; i < argc; i++)
  {
    if (!strcmp(argv[i], "-h") || !strcmp(argv[i],"--help")) {
      std::cout
        << "Usage: ./mpc_model [OPTION]...\n"
        << "Generates the mpc model files using the ACADO Toolkit code generation when no options are given\n\n"
        << "Options:\n"
        << "-s, --simulate" << "\t\t" << "perform a closed-loop mpc simulation\n"
        << "-h, --help" << "\t\t" << "display this help and exit\n"
        << std::endl;
      exit(EXIT_SUCCESS);
    }
    else if (!strcmp(argv[i], "-s") || !strcmp(argv[i],"--simulate")) {
      return false;
    }
  }

  return true;
}

int main(int argc, char** argv)
{

  // Switching variable for generating or simulating the mpc
  bool generate_model = generateModelCheck(argc, argv);


  /**
   * ==========================================
   * Define System Variables (States + Control)
   * ==========================================
   */


  // states: angle (theta [rad]) and velocity (dtheta [rad/s])
  // intermediate state: acceleration (ddtheta [rad/s^2])
  // control: effort (u [A])


  // left_hip_aa
  DifferentialState theta_haa_l,  dtheta_haa_l;
  IntermediateState ddtheta_haa_l, phi_haa_l, F_haa_l,
                    d1_haa_l, beta_haa_l, a_haa_l, C_haa_l;   // HAA Lever Arm Left
  Control           u_haa_l;

  // right_hip_aa
  DifferentialState theta_haa_r,  dtheta_haa_r;
  IntermediateState ddtheta_haa_r, phi_haa_r, F_haa_r,
                    d1_haa_r, beta_haa_r, a_haa_r, C_haa_r;   // HAA Lever Arm Right
  Control           u_haa_r;


  /**
   * ==========
   * General Parameters
   * ==========
   */

  // General Parameters
  double g = 9.81;                        // gravitational constant       // m/s^2

  // Linear Actuator Parameters
  double lin_pitch = 0.002;
  // TODO: Is the rot_to_lin_conversion correct?
  double rot_to_lin_conversion = (2.0 * M_PI) / lin_pitch;
  double ball_screw_efficiency = 0.9;

  double lin_kv = 355.0;
  double lin_motor_constant = 8.27/lin_kv;

  // HAA Lever Arm Parameters
  // https://confluence.projectmarch.nl:8443/display/62tech/Measurements+Backplate
  double d2 = 0.126; // j
  double d3 = 0.070; // p
  double b = dist(d2, d3);
  std::cout << "b: " << b << std::endl;
  double c = 0.076485; // sqrt(0.015^2 + r^2) = 0.076485
  double psi_haa = 0.197396; // atan(0.015/r) = 0.197396

  // OCP Parameters
  int    N       = 20;            // horizon length               // [ ]
  double dt      = 0.008;            // sampling time                // [s]
  double t_end   = N*dt;            // ocp end time                 // [s]
  double t_sim   = 2.0;            // simulation time              // [s]


  /**
 * ============================================
 * Left/Right Hip (HAA) Parameters and Dynamics
 * ============================================
 */

  // base Parameters (LEFT_HAA)
  double mass_haa = 16.97;      // Joint load mass [kg]
  double Ixx_com_haa = 3.23;    // Center of mass inertia around x-axis [kgm^2]
  double com_y_haa = -0.117469; // Center of mass x coordinate [m]
  double com_z_haa = -0.280263; // Center of mass z coordinate [m]

  // parameter calculations (HAA)
  double L_haa =
    dist(com_y_haa, com_z_haa);             // Straight line distance to center of mass [m]
  std::cout << "L_haa: " << L_haa << std::endl;
  double theta_0_haa =
    atan(com_y_haa/com_z_haa);              // Angle of x-axis to line L [rad]
  std::cout << "theta_0_haa: " << theta_0_haa << std::endl;
  double inv_Ixx_haa =
    1/(Ixx_com_haa + mass_haa*L_haa*L_haa); // Inverse inertia around axis or rotation [(kgm^2)^-1]
    
  // constraints (HAA)
  double theta_lb_haa_l = -0.1848;  // Lower bound on the left HAA joint angle [rad]
  double theta_ub_haa_l = 0.1680;   // Upper bound on the left HAA joint angle [rad]

  double theta_lb_haa_r = -0.1549;  // Lower bound on the right HAA joint angle [rad]
  double theta_ub_haa_r = 0.1864;   // Upper bound on the right HAA joint angle [rad]

  double dtheta_max_haa = 2.5;      // Maximum joint speed [rad/s]
  double ddtheta_max_haa = 2.0;     // Maximum joint acceleration [rad/s^2]
  double u_max_haa = 30.0;          // Maximum joint torque [A]

  // Force exerted by the linear actuator given input effort
  F_haa_l = (u_haa_l * lin_motor_constant) * rot_to_lin_conversion * ball_screw_efficiency;
  F_haa_r = (u_haa_r * lin_motor_constant) * rot_to_lin_conversion * ball_screw_efficiency;

  // HAA Lever Arm Dynamics (Left)
  d1_haa_l = c*cos(theta_haa_l + psi_haa);                          // Lever arm length [m]
  beta_haa_l = M_PI/2 - atan(d3/d2) - theta_haa_l - psi_haa;        // Angle of actuator triangle [rad]
  a_haa_l = sqrt(b*b + c*c + 2*b*c*cos(beta_haa_l));                // Linear actuator length [m]
  C_haa_l = sqrt(1 - pow((c+b*cos(beta_haa_l))/(a_haa_l), 2));      // Effective force coefficient

  // HAA Lever Arm Dynamics (Right)
  d1_haa_r = c*cos(theta_haa_r + psi_haa);                          // Lever arm length [m]
  beta_haa_r = M_PI/2 - atan(d3/d2) - theta_haa_r - psi_haa;        // Angle of actuator triangle [rad]
  a_haa_r = sqrt(b*b + c*c + 2*b*c*cos(beta_haa_r));                // Linear actuator length [m]
  C_haa_r = sqrt(1 - pow((c+b*cos(beta_haa_r))/(a_haa_r), 2));      // Effective force coefficient

  // Total Dynamics (HAA)
  double Tf_haa_l = 5.0;  // Dynamic Friction coefficient left HAA
  double Tf_haa_r = 5.0;  // Dynamic Friction coefficient left HAA
  phi_haa_l = theta_haa_l + theta_0_haa;
  phi_haa_r = theta_haa_r + theta_0_haa;
  ddtheta_haa_l = inv_Ixx_haa * (C_haa_l * F_haa_l * d1_haa_l - mass_haa * g * L_haa * sin(phi_haa_l) - Tf_haa_l * dtheta_haa_l);
  ddtheta_haa_r = inv_Ixx_haa * (C_haa_r * F_haa_r * d1_haa_r - mass_haa * g * L_haa * sin(phi_haa_r) - Tf_haa_r * dtheta_haa_r);


  /**
   * =====================
   * Differential Equation
   * =====================
   */

  // Define differential equation
  DifferentialEquation f;

  // left_hip_aa
  f << dot(theta_haa_l)  == dtheta_haa_l;
  f << dot(dtheta_haa_l) == ddtheta_haa_l;

  // right_hip_aa
  f << dot(theta_haa_r)  == dtheta_haa_r;
  f << dot(dtheta_haa_r) == ddtheta_haa_r;

  // NOTE: "f" needs to have the same order/sequence as "h"


  /**
   * ========================================================
   * Linear Least Squares (LSQ) Vector and Weighting Matrices
   * ========================================================
   */

  // Joint state expressions (combining all states of a joint under a single object)
  Expression left_hip_aa;
  left_hip_aa << theta_haa_l  << dtheta_haa_l;

  Expression right_hip_aa;
  right_hip_aa << theta_haa_r  << dtheta_haa_r;

  // Joint control expressions (combining all controls under two objects)
  Expression left_controls;
  left_controls << u_haa_l;

  Expression right_controls;
  right_controls << u_haa_r;

  // Running LSQ vector (states + controls)
  Function h;
  h << left_hip_aa;
  h << right_hip_aa;
  h << left_controls << right_controls;

  // End LSQ vector (only states)
  Function hN;
  hN << left_hip_aa;
  hN << right_hip_aa;

  // Running weight vectors
  // Using same tuning for left and right leg
  std::vector<double> Q_haa  = {200.0, 1.0}; // Cost on haa states
  std::vector<double> R      = {0.01}; // Cost on controls

  // Combine running weight vectors into a single vector W_diag
  // representing the values on the diagonal of the weights matrix
  // W_diag = {Q_left, Q_right, R_left, R_right}
  std::vector<double> W_diag;
  W_diag.insert(W_diag.end(), Q_haa.begin(), Q_haa.end());
  W_diag.insert(W_diag.end(), W_diag.begin(), W_diag.end());
  W_diag.insert(W_diag.end(), R.begin(), R.end());
  W_diag.insert(W_diag.end(), R.begin(), R.end());

  // Set running weights
  DMatrix W(h.getDim(), h.getDim());
  W.setAll(0.0);
  for (int i = 0; i < h.getDim(); ++i) {
    W(i,i) = W_diag[i];
  }

  // Set end weights matrix equal to running weights matrix
  DMatrix WN(hN.getDim(), hN.getDim());
  WN.setAll(0.0);
  for (int i = 0; i < hN.getDim(); ++i) {
    WN(i,i) = W_diag[i];
  }

  // Set running reference with equilibrium torque reference
  double reference = 0.1;
  double u_eq_haa = 0.0;
  DVector r(h.getDim());
  r.setAll(reference);
  r(0) = reference;           // position left_hip_aa
  r(2) = reference;           // position right_hip_aa
  r(f.getNX()+0) = u_eq_haa;  // equilibrium torque left_hip_aa
  r(f.getNX()+1) = u_eq_haa;  // equilibrium torque right_hip_aa

  // Set end reference
  DVector rN(hN.getDim());
  rN.setAll(0.0);
  rN(0) = r(0);
  rN(2) = r(2);


  /**
   * =============================
   * Optimal Control Problem (OCP)
   * =============================
   */

  OCP ocp(0.0, t_end, N);

  // System dynamics constraint
  ocp.subjectTo(f);

  // Left and right HAA constraints
  // ocp.subjectTo(theta_lb_haa_l   <= theta_haa_l  <= theta_ub_haa_l);
  ocp.subjectTo(-dtheta_max_haa  <= dtheta_haa_l <= dtheta_max_haa);
  ocp.subjectTo(-u_max_haa       <= u_haa_l      <= u_max_haa);

  // ocp.subjectTo(theta_lb_haa_r   <= theta_haa_r  <= theta_ub_haa_r);
  ocp.subjectTo(-dtheta_max_haa  <= dtheta_haa_r <= dtheta_max_haa);
  ocp.subjectTo(-u_max_haa       <= u_haa_r      <= u_max_haa);

  // Note: the position constraints are omitted because they cause oscillations
  // when the pilot forces the joints outside their defined limits.


  // Set the LSQ objective for either simulation or code generation purposes
  if (!generate_model) {
    // The simulation environment requires a reference vector in the LSQ objective
    ocp.minimizeLSQ(W, h, r);
    ocp.minimizeLSQEndTerm(WN, hN, rN);

    simulateModel(f, ocp, dt, t_sim);
  } else {

    // Create post model generation modifiable weighting matrices
    BMatrix W  = eye<bool>(h.getDim());
    BMatrix WN = eye<bool>(hN.getDim());

    // Code generation supports only the standard LSQ objective
    ocp.minimizeLSQ(W, h);
    ocp.minimizeLSQEndTerm(WN, hN);

    generateMPCModel(ocp);
  }

  return EXIT_SUCCESS;

}


