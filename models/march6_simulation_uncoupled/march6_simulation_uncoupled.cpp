// ACADO Toolkit
#include <acado_toolkit.hpp>
#include <acado_gnuplot.hpp>

// Other
#include <vector>
#include <iostream>

// Use ACADO
USING_NAMESPACE_ACADO

// Function to calculate the distance to a point using the horizontal and vertical distances
double dist(double hor, double ver) {

  double dist_ = sqrt(hor*hor + ver*ver);

  return dist_;
}

void simulateModel(DifferentialEquation f, OCP ocp, double dt, double t_sim) {

  // Setting up the simulated process
  OutputFcn identity;
  DynamicSystem dynamicSystem(f, identity);

  Process process(dynamicSystem, INT_RK45);


  // Setting up the MPC controller
  RealTimeAlgorithm alg(ocp, dt);
  alg.set(INTEGRATOR_TYPE, INT_RK78);

  StaticReferenceTrajectory zeroReference;

  Controller controller(alg, zeroReference);


  // Setting up the simulation environment
  SimulationEnvironment sim(0.0,t_sim,process,controller);

  DVector x0(f.getNX());
  x0.setAll(0.0);

  if (sim.init(x0) != SUCCESSFUL_RETURN) {
    exit(EXIT_FAILURE);
  }
  if (sim.run() != SUCCESSFUL_RETURN) {
    exit(EXIT_FAILURE);
  }


  // Plot the results
  VariablesGrid differentialStates;
  sim.getProcessDifferentialStates(differentialStates);

  VariablesGrid feedbackControl;
  sim.getFeedbackControl(feedbackControl);

  VariablesGrid intermediateStates;
  sim.getProcessIntermediateStates(intermediateStates);

  // Plot the joint angles, velocities and torques
  GnuplotWindow window_pos, window_vel, window_torque;
  for (int i = 0; i < f.getNX()/2; ++i) {
    // State theta, angle
    window_pos.addSubplot(differentialStates(2*i), "state, Angle");
    window_pos.setLabelX(i,"time [s]");
    window_pos.setLabelY(i,"rad");

    // State dtheta, Angular Velocity
    window_vel.addSubplot(differentialStates(2*i + 1), "state, Angular Velocity");
    window_vel.setLabelX(i, "time [s]");
    window_vel.setLabelY(i, "rad/s");

    // Control T, torque
    window_torque.addSubplot(feedbackControl(i), "input, Torque");
    window_torque.setLabelX(i, "time [s]");
    window_torque.setLabelY(i, "N/m");
  }
  window_pos.plot();
  window_vel.plot();
  window_torque.plot();

  exit(EXIT_SUCCESS);
}

void generateMPCModel(OCP ocp, int N, int Ni) {

  /**
 * Code Generation (Export)
 */

  OCPexport mpc(ocp);

  // MPC settings
  mpc.set(HESSIAN_APPROXIMATION,          GAUSS_NEWTON);
  mpc.set(DISCRETIZATION_TYPE,            MULTIPLE_SHOOTING);
  mpc.set(SPARSE_QP_SOLUTION,             FULL_CONDENSING);
  mpc.set(INTEGRATOR_TYPE,                INT_RK4);
  mpc.set(QP_SOLVER,                      QP_QPOASES);
  mpc.set(HOTSTART_QP,                    NO);
  mpc.set(LEVENBERG_MARQUARDT,            1e-10);
  mpc.set(CG_HARDCODE_CONSTRAINT_VALUES,  YES);
  mpc.set(USE_SINGLE_PRECISION,           YES);

  // Do not generate tests, makes or matlab-related interfaces.
  mpc.set( GENERATE_TEST_FILE,            NO);
  mpc.set( GENERATE_MAKE_FILE,            NO);
  mpc.set( GENERATE_MATLAB_INTERFACE,     NO);
  mpc.set( GENERATE_SIMULINK_INTERFACE,   NO);

  if (mpc.exportCode( "../model_export" ) != SUCCESSFUL_RETURN) {
    exit(EXIT_FAILURE);
  }
  mpc.printDimensionsQP( );
}

bool generateModelCheck(int argc, char** argv) {
  /*
   * Checks if the mpc model should be generated or simulated based on the given executable arguments.
   * On default this functions returns true but when the executable arguments -s or --simulate are used
   * the functions returns false.
   * There is also a help argument that will exit the program after it has displayed the help page.
   */

  // Check given arguments
  for (int i = 0; i < argc; i++)
  {
    // strcmp() returns 0 when the two strings are equal.
    // The NOT operator "!" is used to flip the output of strcmp() for correct usage in the if statement.
    if (!strcmp(argv[i], "-h") || !strcmp(argv[i],"--help")) {
      std::cout
        << "Usage: ./mpc_model [OPTION]...\n"
        << "Generates the mpc model files using the ACADO Toolkit code generation when no options are given\n\n"
        << "Options:\n"
        << "-s, --simulate" << "\t\t" << "perform a closed-loop mpc simulation\n"
        << "-h, --help" << "\t\t" << "display this help and exit\n"
        << std::endl;
      exit(EXIT_SUCCESS);
    }
    else if (!strcmp(argv[i], "-s") || !strcmp(argv[i],"--simulate")) {
      return false;
    }
  }

  return true;
}

/**
 * The executable build with this script supports command-line arguments
 * These are supplied to the main function by the variables argc and argv
 * @param argc - number of command-line arguments
 * @param argv - list of command-line arguments
 */

int main(int argc, char** argv) {

  // Switching variable for generating or simulating the mpc
  bool generate_model = generateModelCheck(argc, argv);

  /**
   * ==========================================
   * Define System Variables (States + Control)
   * ==========================================
   */

  // states: angle (theta [rad]) and velocity (dtheta [rad/s])
  // intermediate state: acceleration (ddtheta [rad/s^2])
  // control: torque (u [N/m])

  // left_ankle
  DifferentialState theta_adpf_l, dtheta_adpf_l;
  IntermediateState ddtheta_adpf_l, phi_adpf_l;
  Control           u_adpf_l;

  // left_hip_aa
  DifferentialState theta_haa_l,  dtheta_haa_l;
  IntermediateState ddtheta_haa_l, phi_haa_l;
  Control           u_haa_l;

  // left_hip_fe
  DifferentialState theta_hfe_l,  dtheta_hfe_l;
  IntermediateState ddtheta_hfe_l, phi_hfe_l;
  Control           u_hfe_l;

  // left_knee
  DifferentialState theta_kfe_l,  dtheta_kfe_l;
  IntermediateState ddtheta_kfe_l, phi_kfe_l;
  Control           u_kfe_l;

  // right_ankle
  DifferentialState theta_adpf_r, dtheta_adpf_r;
  IntermediateState ddtheta_adpf_r, phi_adpf_r;
  Control           u_adpf_r;

  // right_hip_aa
  DifferentialState theta_haa_r,  dtheta_haa_r;
  IntermediateState ddtheta_haa_r, phi_haa_r;
  Control           u_haa_r;

  // right_hip_fe
  DifferentialState theta_hfe_r,  dtheta_hfe_r;
  IntermediateState ddtheta_hfe_r, phi_hfe_r;
  Control           u_hfe_r;

  // right_knee
  DifferentialState theta_kfe_r,  dtheta_kfe_r;
  IntermediateState ddtheta_kfe_r, phi_kfe_r;
  Control           u_kfe_r;


  /**
   * ==================
   * General Parameters
   * ==================
   */

  // General Parameters
  double g = 9.81;      // gravitational constant [m/s^2]

  // OCP parameters
  int N = 10;           // Horizon length
  int Ni = 4;           // Intergrator amount
  double dt = 0.04;     // Discretization time [s]
  double t_end = N*dt;  // ocp end time [s]
  double t_sim = 2.0;   // Simulation duration [s]


  /**
   * ===============================================
   * Left/Right Ankle (ADPF) Parameters and Dynamics
   * ===============================================
   */

  // base Parameters (LEFT_ADPF)
  double mass_adpf = 0.789;       // Joint load mass [kg]
  double Iyy_com_adpf = 0.0066;  // Center of mass inertia around y-axis [kgm^2]
  double com_x_adpf = -0.028926; // Center of mass x coordinate [m]
  double com_z_adpf = 0.005212;  // Center of mass z coordinate [m]

  // parameter calculations (ADPF)
  double L_adpf =
    dist(com_x_adpf, com_z_adpf);               // Straight line distance to center of mass [m]
  double theta_0_adpf =
    atan(abs(com_z_adpf/com_x_adpf));           // Angle of x-axis to line L [rad]
  double inv_Iyy_adpf =
    1/(Iyy_com_adpf + mass_adpf*L_adpf*L_adpf); // Inverse inertia around axis or rotation [(kgm^2)^-1]
  double u_eq_adpf =
    mass_adpf * g * L_adpf * cos(theta_0_adpf);  // Equilibrium torque at zero initial state

  // constraints (ADPF)
  double theta_lb_adpf = -0.4591;  // Lower bound on the joint angle [rad]
  double theta_ub_adpf = 0.2772;   // Upper bound on the joint angle [rad]
  double dtheta_max_adpf = 1.0;    // Maximum joint speed [rad/s]
  double u_max_adpf = 200.0;       // Maximum joint torque [Nm]

  // Dynamics (ADPF)
  phi_adpf_l = theta_adpf_l + theta_0_adpf;
  phi_adpf_r = theta_adpf_r + theta_0_adpf;
  ddtheta_adpf_l = inv_Iyy_adpf * (u_adpf_l - mass_adpf * g * L_adpf * cos(phi_adpf_l));
  ddtheta_adpf_r = inv_Iyy_adpf * (u_adpf_r - mass_adpf * g * L_adpf * cos(phi_adpf_r));


  /**
 * ============================================
 * Left/Right Hip (HAA) Parameters and Dynamics
 * ============================================
 */

  // base Parameters (LEFT_HAA)
  double mass_haa = 16.97;      // Joint load mass [kg]
  double Ixx_com_haa = 3.23;    // Center of mass inertia around x-axis [kgm^2]
  double com_y_haa = -0.117469; // Center of mass x coordinate [m]
  double com_z_haa = -0.280263; // Center of mass z coordinate [m]

  // parameter calculations (HAA)
  double L_haa =
    dist(com_y_haa, com_z_haa);             // Straight line distance to center of mass [m]
  double theta_0_haa =
    -atan(abs(com_z_haa/com_y_haa));         // Angle of x-axis to line L [rad]
  double inv_Ixx_haa =
    1/(Ixx_com_haa + mass_haa*L_haa*L_haa); // Inverse inertia around axis or rotation [(kgm^2)^-1]
  double u_eq_haa =
    mass_haa * g * L_haa * cos(theta_0_haa); // Equilibrium torque at zero initial state

  // constraints (HAA)
  double theta_lb_haa = -0.3237;  // Lower bound on the joint angle [rad]
  double theta_ub_haa = 0.2853;   // Upper bound on the joint angle [rad]
  double dtheta_max_haa = 1.0;    // Maximum joint speed [rad/s]
  double u_max_haa = 200.0;       // Maximum joint torque [Nm]

  // Dynamics (HAA)
  phi_haa_l = theta_haa_l + theta_0_haa;
  phi_haa_r = theta_haa_r + theta_0_haa;
  ddtheta_haa_l = inv_Ixx_haa * (u_haa_l - mass_haa * g * L_haa * cos(phi_haa_l));
  ddtheta_haa_r = inv_Ixx_haa * (u_haa_r - mass_haa * g * L_haa * cos(phi_haa_r));


  /**
   * ============================================
   * Left/Right Hip (HFE) Parameters and Dynamics
   * ============================================
   */

  // base Parameters (LEFT_HFE)
  double mass_hfe = 13.95;      // Joint load mass [kg]
  double Iyy_com_hfe = 2.78;    // Center of mass inertia around y-axis [kgm^2]
  double com_x_hfe = -0.002372; // Center of mass x coordinate [m]
  double com_z_hfe = -0.347316; // Center of mass z coordinate [m]

  // parameter calculations (HFE)
  double L_hfe =
    dist(com_x_hfe, com_z_hfe);             // Straight line distance to center of mass [m]
  double theta_0_hfe =
    atan(abs(com_x_hfe/com_z_hfe));         // Angle of y-axis to line L [rad]
  double inv_Iyy_hfe =
    1/(Iyy_com_hfe + mass_hfe*L_hfe*L_hfe); // Inverse inertia around axis or rotation [(kgm^2)^-1]
  double u_eq_hfe =
    mass_hfe * g * L_hfe * sin(theta_0_hfe); // Equilibrium torque at zero initial state

  // constraints (HFE)
  double theta_lb_hfe = -0.3673;  // Lower bound on the joint angle [rad]
  double theta_ub_hfe = 1.7274;   // Upper bound on the joint angle [rad]
  double dtheta_max_hfe = 2.0;    // Maximum joint speed [rad/s]
  double u_max_hfe = 200.0;       // Maximum joint torque [Nm]

  // Dynamics (HFE)
  phi_hfe_l = theta_hfe_l + theta_0_hfe;
  phi_hfe_r = theta_hfe_r + theta_0_hfe;
  ddtheta_hfe_l = inv_Iyy_hfe * (u_hfe_l - mass_hfe * g * L_hfe * sin(phi_hfe_l));
  ddtheta_hfe_r = inv_Iyy_hfe * (u_hfe_r - mass_hfe * g * L_hfe * sin(phi_hfe_r));


/**
   * =============================================
   * Left/Right Knee (KFE) Parameters and Dynamics
   * =============================================
   */

  // base Parameters (LEFT_KFE)
  double mass_kfe = 5.79;       // Joint load mass [kg]
  double Iyy_com_kfe = 0.62;    // Center of mass inertia around y-axis [kgm^2]
  double com_x_kfe = -0.006717; // Center of mass x coordinate [m]
  double com_z_kfe = -0.156315; // Center of mass z coordinate [m]

  // parameter calculations (KFE)
  double L_kfe =
    dist(com_x_kfe, com_z_kfe);             // Straight line distance to center of mass [m]
  double theta_0_kfe =
    -atan(abs(com_x_kfe/com_z_kfe));        // Angle of y-axis to line L [rad]
  double inv_Iyy_kfe =
    1/(Iyy_com_kfe + mass_kfe*L_kfe*L_kfe); // Inverse inertia around axis or rotation [(kgm^2)^-1]
  double u_eq_kfe =
    mass_kfe * g * L_kfe * sin(theta_0_kfe); // Equilibrium torque at zero initial state

  // constraints (KFE)
  double theta_lb_kfe = -0.08748; // Lower bound on the joint angle [rad]
  double theta_ub_kfe = 1.9622;   // Upper bound on the joint angle [rad]
  double dtheta_max_kfe = 2.5;    // Maximum joint speed [rad/s]
  double u_max_kfe = 200.0;       // Maximum joint torque [Nm]

  // Dynamics (KFE)
  phi_kfe_l = theta_kfe_l + theta_0_kfe;
  phi_kfe_r = theta_kfe_r + theta_0_kfe;
  ddtheta_kfe_l = inv_Iyy_kfe * (u_kfe_l - mass_kfe * g * L_kfe * sin(phi_kfe_l));
  ddtheta_kfe_r = inv_Iyy_kfe * (u_kfe_r - mass_kfe * g * L_kfe * sin(phi_kfe_r));

  
  /*
   * =====================
   * Differential Equation
   * =====================
   */

  // Define differential equation
  DifferentialEquation f;

  // left_ankle
  f << dot(theta_adpf_l)  == dtheta_adpf_l;
  f << dot(dtheta_adpf_l) == ddtheta_adpf_l;

  // left_hip_aa
  f << dot(theta_haa_l)  == dtheta_haa_l;
  f << dot(dtheta_haa_l) == ddtheta_haa_l;

  // left_hip_fe
  f << dot(theta_hfe_l)  == dtheta_hfe_l;
  f << dot(dtheta_hfe_l) == ddtheta_hfe_l;

  // left_knee;
  f << dot(theta_kfe_l)  == dtheta_kfe_l;
  f << dot(dtheta_kfe_l) == ddtheta_kfe_l;

  // right_ankle
  f << dot(theta_adpf_r)  == dtheta_adpf_r;
  f << dot(dtheta_adpf_r) == ddtheta_adpf_r;

  // right_hip_aa
  f << dot(theta_haa_r)  == dtheta_haa_r;
  f << dot(dtheta_haa_r) == ddtheta_haa_r;

  // right_hip_fe
  f << dot(theta_hfe_r)  == dtheta_hfe_r;
  f << dot(dtheta_hfe_r) == ddtheta_hfe_r;

  // right_knee
  f << dot(theta_kfe_r)  == dtheta_kfe_r;
  f << dot(dtheta_kfe_r) == ddtheta_kfe_r;

  // NOTE: "f" needs to have the same order/sequence as "h"


  /**
   * Linear Least Squares (LSQ) Vector and Weighting Matrices
   */

  // Joint state expressions (combining all states of a joint under a single object)
  Expression left_ankle, left_hip_aa, left_hip_fe, left_knee;
  left_ankle  << theta_adpf_l << dtheta_adpf_l;
  left_hip_aa << theta_haa_l  << dtheta_haa_l;
  left_hip_fe << theta_hfe_l  << dtheta_hfe_l;
  left_knee   << theta_kfe_l  << dtheta_kfe_l;

  Expression right_ankle, right_hip_aa, right_hip_fe, right_knee;
  right_ankle  << theta_adpf_r << dtheta_adpf_r;
  right_hip_aa << theta_haa_r  << dtheta_haa_r;
  right_hip_fe << theta_hfe_r  << dtheta_hfe_r;
  right_knee   << theta_kfe_r  << dtheta_kfe_r;

  // Joint control expressions (combining all controls under two objects)
  Expression left_controls;
  left_controls << u_adpf_l << u_haa_l << u_hfe_l << u_kfe_l;

  Expression right_controls;
  right_controls << u_adpf_r << u_haa_r << u_hfe_r << u_kfe_r;

  // Running LSQ vector (states + controls)
  Function h;
  h << left_ankle    << left_hip_aa  << left_hip_fe  << left_knee;
  h << right_ankle   << right_hip_aa << right_hip_fe << right_knee;
  h << left_controls << right_controls;

  // End LSQ vector (only states)
  Function hN;
  hN << left_ankle  << left_hip_aa  << left_hip_fe  << left_knee;
  hN << right_ankle << right_hip_aa << right_hip_fe << right_knee;

  // Running weight vectors
  // Using same tuning for left and right leg
  std::vector<double> Q_adpf = {100.0, 1.0}; // Cost on adpf states
  std::vector<double> Q_haa  = {100.0, 1.0}; // Cost on haa states
  std::vector<double> Q_hfe  = {100.0, 1.0}; // Cost on hfe states
  std::vector<double> Q_kfe  = {100.0, 1.0}; // Cost on kfe states
  std::vector<double> R      = {0.01, 0.01,
                                0.01, 0.01}; // Cost on controls

  // Combine running weight vectors into a single vector W_diag
  // representing the values on the diagonal of the weights matrix
  // W_diag = {Q_left, Q_right, R_left, R_right}
  std::vector<double> W_diag;
  W_diag.insert(W_diag.end(), Q_adpf.begin(), Q_adpf.end());
  W_diag.insert(W_diag.end(), Q_haa.begin(), Q_haa.end());
  W_diag.insert(W_diag.end(), Q_hfe.begin(), Q_hfe.end());
  W_diag.insert(W_diag.end(), Q_kfe.begin(), Q_kfe.end());
  W_diag.insert(W_diag.end(), W_diag.begin(), W_diag.end());
  W_diag.insert(W_diag.end(), R.begin(), R.end());
  W_diag.insert(W_diag.end(), R.begin(), R.end());

  // Set running weights
  DMatrix W(h.getDim(), h.getDim());
  W.setAll(0.0);
  for (int i = 0; i < h.getDim(); ++i) {
    W(i,i) = W_diag[i];
  }

  // Set end weights matrix equal to running weights matrix
  DMatrix WN(hN.getDim(), hN.getDim());
  WN.setAll(0.0);
  for (int i = 0; i < hN.getDim(); ++i) {
    WN(i,i) = W_diag[i];
  }

  // Set running reference with equilibrium torque reference
  DVector r(h.getDim());
  r.setAll(0.0);
  r(f.getNX()+0) = u_eq_adpf; // equilibrium torque left_ankle
  r(f.getNX()+1) = u_eq_haa;  // equilibrium torque left_hip_aa
  r(f.getNX()+2) = u_eq_hfe;  // equilibrium torque left_hip_fe
  r(f.getNX()+3) = u_eq_kfe;  // equilibrium torque left_knee
  r(f.getNX()+4) = u_eq_adpf; // equilibrium torque right_ankle
  r(f.getNX()+5) = u_eq_haa;  // equilibrium torque right_hip_aa
  r(f.getNX()+6) = u_eq_hfe;  // equilibrium torque right_hip_fe
  r(f.getNX()+7) = u_eq_kfe;  // equilibrium torque right_knee

  // Set end reference
  DVector rN(h.getDim());
  rN.setAll(0.0);


  /**
   * Optimal Control Problem (OCP)
   */

  // Optimal Control Problem (OCP) object
  OCP ocp(0.0, t_end, N);

  // System dynamics constraint
  ocp.subjectTo(f);

  // Left and right ADPF constraints
  ocp.subjectTo(theta_lb_adpf    <= theta_adpf_l  <= theta_ub_adpf);
  ocp.subjectTo(-dtheta_max_adpf  <= dtheta_adpf_l <= dtheta_max_adpf);
  ocp.subjectTo(-u_max_adpf       <= u_adpf_l      <= u_max_adpf);

  ocp.subjectTo(theta_lb_adpf    <= theta_adpf_r  <= theta_ub_adpf);
  ocp.subjectTo(-dtheta_max_adpf  <= dtheta_adpf_r <= dtheta_max_adpf);
  ocp.subjectTo(-u_max_adpf       <= u_adpf_r      <= u_max_adpf);

  // Left and right HAA constraints
  ocp.subjectTo(theta_lb_haa    <= theta_haa_l  <= theta_ub_haa);
  ocp.subjectTo(-dtheta_max_haa  <= dtheta_haa_l <= dtheta_max_haa);
  ocp.subjectTo(-u_max_haa       <= u_haa_l      <= u_max_haa);

  ocp.subjectTo(theta_lb_haa    <= theta_haa_r  <= theta_ub_haa);
  ocp.subjectTo(-dtheta_max_haa  <= dtheta_haa_r <= dtheta_max_haa);
  ocp.subjectTo(-u_max_haa       <= u_haa_r      <= u_max_haa);

  // Left and right HFE constraints
  ocp.subjectTo(theta_lb_hfe    <= theta_hfe_l  <= theta_ub_hfe);
  ocp.subjectTo(-dtheta_max_hfe  <= dtheta_hfe_l <= dtheta_max_hfe);
  ocp.subjectTo(-u_max_hfe       <= u_hfe_l      <= u_max_hfe);

  ocp.subjectTo(theta_lb_hfe    <= theta_hfe_r  <= theta_ub_hfe);
  ocp.subjectTo(-dtheta_max_hfe  <= dtheta_hfe_r <= dtheta_max_hfe);
  ocp.subjectTo(-u_max_hfe       <= u_hfe_r      <= u_max_hfe);

  // Left and right KFE constraints
  ocp.subjectTo(theta_lb_kfe    <= theta_kfe_l  <= theta_ub_kfe);
  ocp.subjectTo(-dtheta_max_kfe  <= dtheta_kfe_l <= dtheta_max_kfe);
  ocp.subjectTo(-u_max_kfe       <= u_kfe_l      <= u_max_kfe);

  ocp.subjectTo(theta_lb_kfe    <= theta_kfe_r  <= theta_ub_kfe);
  ocp.subjectTo(-dtheta_max_kfe  <= dtheta_kfe_r <= dtheta_max_kfe);
  ocp.subjectTo(-u_max_kfe       <= u_kfe_r      <= u_max_kfe);

  // Set the LSQ objective for either simulation or code generation purposes
  // after which call either the simulation or code generation function
  if (!generate_model) {

    // The simulation environment requires a reference vector in the LSQ objective
    ocp.minimizeLSQ(W, h, r);
    ocp.minimizeLSQEndTerm(WN, hN, rN);

    // Perform the simulation
    simulateModel(f, ocp, dt, t_sim);

  } else {

    // Create post model generation modifiable weighting matrices
    // Keep in mind that the amount of values in W and WN
    // increase quadratically with the length of h and hN respectively.
    BMatrix W_bool  = eye<bool>(h.getDim());
    BMatrix WN_bool = eye<bool>(hN.getDim());

    // Code generation supports only the standard LSQ objective
    ocp.minimizeLSQ(W_bool, h);
    ocp.minimizeLSQEndTerm(WN_bool, hN);

    // Perform the code generation
    generateMPCModel(ocp, N, Ni);
  }

  return 0;
}