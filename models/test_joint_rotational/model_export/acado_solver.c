/*
 *    This file was auto-generated using the ACADO Toolkit.
 *    
 *    While ACADO Toolkit is free software released under the terms of
 *    the GNU Lesser General Public License (LGPL), the generated code
 *    as such remains the property of the user who used ACADO Toolkit
 *    to generate this code. In particular, user dependent data of the code
 *    do not inherit the GNU LGPL license. On the other hand, parts of the
 *    generated code that are a direct copy of source code from the
 *    ACADO Toolkit or the software tools it is based on, remain, as derived
 *    work, automatically covered by the LGPL license.
 *    
 *    ACADO Toolkit is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *    
 */


#include "acado_common.h"




/******************************************************************************/
/*                                                                            */
/* ACADO code generation                                                      */
/*                                                                            */
/******************************************************************************/


int acado_modelSimulation(  )
{
int ret;

int lRun1;
ret = 0;
for (lRun1 = 0; lRun1 < 20; ++lRun1)
{
acadoWorkspace.state[0] = acadoVariables.x[lRun1 * 2];
acadoWorkspace.state[1] = acadoVariables.x[lRun1 * 2 + 1];

acadoWorkspace.state[8] = acadoVariables.u[lRun1];

ret = acado_integrate(acadoWorkspace.state, 1);

acadoWorkspace.d[lRun1 * 2] = acadoWorkspace.state[0] - acadoVariables.x[lRun1 * 2 + 2];
acadoWorkspace.d[lRun1 * 2 + 1] = acadoWorkspace.state[1] - acadoVariables.x[lRun1 * 2 + 3];

acadoWorkspace.evGx[lRun1 * 4] = acadoWorkspace.state[2];
acadoWorkspace.evGx[lRun1 * 4 + 1] = acadoWorkspace.state[3];
acadoWorkspace.evGx[lRun1 * 4 + 2] = acadoWorkspace.state[4];
acadoWorkspace.evGx[lRun1 * 4 + 3] = acadoWorkspace.state[5];

acadoWorkspace.evGu[lRun1 * 2] = acadoWorkspace.state[6];
acadoWorkspace.evGu[lRun1 * 2 + 1] = acadoWorkspace.state[7];
}
return ret;
}

void acado_evaluateLSQ(const real_t* in, real_t* out)
{
const real_t* xd = in;
const real_t* u = in + 2;

/* Compute outputs: */
out[0] = xd[0];
out[1] = xd[1];
out[2] = u[0];
}

void acado_evaluateLSQEndTerm(const real_t* in, real_t* out)
{
const real_t* xd = in;

/* Compute outputs: */
out[0] = xd[0];
out[1] = xd[1];
}

void acado_setObjQ1Q2( real_t* const tmpObjS, real_t* const tmpQ1, real_t* const tmpQ2 )
{
tmpQ2[0] = +tmpObjS[0];
tmpQ2[1] = +tmpObjS[1];
tmpQ2[2] = +tmpObjS[2];
tmpQ2[3] = +tmpObjS[3];
tmpQ2[4] = +tmpObjS[4];
tmpQ2[5] = +tmpObjS[5];
tmpQ1[0] = + tmpQ2[0];
tmpQ1[1] = + tmpQ2[1];
tmpQ1[2] = + tmpQ2[3];
tmpQ1[3] = + tmpQ2[4];
}

void acado_setObjR1R2( real_t* const tmpObjS, real_t* const tmpR1, real_t* const tmpR2 )
{
tmpR2[0] = +tmpObjS[6];
tmpR2[1] = +tmpObjS[7];
tmpR2[2] = +tmpObjS[8];
tmpR1[0] = + tmpR2[2];
}

void acado_setObjQN1QN2( real_t* const tmpObjSEndTerm, real_t* const tmpQN1, real_t* const tmpQN2 )
{
tmpQN2[0] = +tmpObjSEndTerm[0];
tmpQN2[1] = +tmpObjSEndTerm[1];
tmpQN2[2] = +tmpObjSEndTerm[2];
tmpQN2[3] = +tmpObjSEndTerm[3];
tmpQN1[0] = + tmpQN2[0];
tmpQN1[1] = + tmpQN2[1];
tmpQN1[2] = + tmpQN2[2];
tmpQN1[3] = + tmpQN2[3];
}

void acado_evaluateObjective(  )
{
int runObj;
for (runObj = 0; runObj < 20; ++runObj)
{
acadoWorkspace.objValueIn[0] = acadoVariables.x[runObj * 2];
acadoWorkspace.objValueIn[1] = acadoVariables.x[runObj * 2 + 1];
acadoWorkspace.objValueIn[2] = acadoVariables.u[runObj];

acado_evaluateLSQ( acadoWorkspace.objValueIn, acadoWorkspace.objValueOut );
acadoWorkspace.Dy[runObj * 3] = acadoWorkspace.objValueOut[0];
acadoWorkspace.Dy[runObj * 3 + 1] = acadoWorkspace.objValueOut[1];
acadoWorkspace.Dy[runObj * 3 + 2] = acadoWorkspace.objValueOut[2];

acado_setObjQ1Q2( acadoVariables.W, &(acadoWorkspace.Q1[ runObj * 4 ]), &(acadoWorkspace.Q2[ runObj * 6 ]) );

acado_setObjR1R2( acadoVariables.W, &(acadoWorkspace.R1[ runObj ]), &(acadoWorkspace.R2[ runObj * 3 ]) );

}
acadoWorkspace.objValueIn[0] = acadoVariables.x[40];
acadoWorkspace.objValueIn[1] = acadoVariables.x[41];
acado_evaluateLSQEndTerm( acadoWorkspace.objValueIn, acadoWorkspace.objValueOut );

acadoWorkspace.DyN[0] = acadoWorkspace.objValueOut[0];
acadoWorkspace.DyN[1] = acadoWorkspace.objValueOut[1];

acado_setObjQN1QN2( acadoVariables.WN, acadoWorkspace.QN1, acadoWorkspace.QN2 );

}

void acado_multGxd( real_t* const dOld, real_t* const Gx1, real_t* const dNew )
{
dNew[0] += + Gx1[0]*dOld[0] + Gx1[1]*dOld[1];
dNew[1] += + Gx1[2]*dOld[0] + Gx1[3]*dOld[1];
}

void acado_moveGxT( real_t* const Gx1, real_t* const Gx2 )
{
Gx2[0] = Gx1[0];
Gx2[1] = Gx1[1];
Gx2[2] = Gx1[2];
Gx2[3] = Gx1[3];
}

void acado_multGxGx( real_t* const Gx1, real_t* const Gx2, real_t* const Gx3 )
{
Gx3[0] = + Gx1[0]*Gx2[0] + Gx1[1]*Gx2[2];
Gx3[1] = + Gx1[0]*Gx2[1] + Gx1[1]*Gx2[3];
Gx3[2] = + Gx1[2]*Gx2[0] + Gx1[3]*Gx2[2];
Gx3[3] = + Gx1[2]*Gx2[1] + Gx1[3]*Gx2[3];
}

void acado_multGxGu( real_t* const Gx1, real_t* const Gu1, real_t* const Gu2 )
{
Gu2[0] = + Gx1[0]*Gu1[0] + Gx1[1]*Gu1[1];
Gu2[1] = + Gx1[2]*Gu1[0] + Gx1[3]*Gu1[1];
}

void acado_moveGuE( real_t* const Gu1, real_t* const Gu2 )
{
Gu2[0] = Gu1[0];
Gu2[1] = Gu1[1];
}

void acado_setBlockH11( int iRow, int iCol, real_t* const Gu1, real_t* const Gu2 )
{
acadoWorkspace.H[(iRow * 20) + (iCol)] += + Gu1[0]*Gu2[0] + Gu1[1]*Gu2[1];
}

void acado_setBlockH11_R1( int iRow, int iCol, real_t* const R11 )
{
acadoWorkspace.H[(iRow * 20) + (iCol)] = R11[0];
}

void acado_zeroBlockH11( int iRow, int iCol )
{
acadoWorkspace.H[(iRow * 20) + (iCol)] = 0.0000000000000000e+00;
}

void acado_copyHTH( int iRow, int iCol )
{
acadoWorkspace.H[(iRow * 20) + (iCol)] = acadoWorkspace.H[(iCol * 20) + (iRow)];
}

void acado_multQ1d( real_t* const Gx1, real_t* const dOld, real_t* const dNew )
{
dNew[0] = + Gx1[0]*dOld[0] + Gx1[1]*dOld[1];
dNew[1] = + Gx1[2]*dOld[0] + Gx1[3]*dOld[1];
}

void acado_multQN1d( real_t* const QN1, real_t* const dOld, real_t* const dNew )
{
dNew[0] = + acadoWorkspace.QN1[0]*dOld[0] + acadoWorkspace.QN1[1]*dOld[1];
dNew[1] = + acadoWorkspace.QN1[2]*dOld[0] + acadoWorkspace.QN1[3]*dOld[1];
}

void acado_multRDy( real_t* const R2, real_t* const Dy1, real_t* const RDy1 )
{
RDy1[0] = + R2[0]*Dy1[0] + R2[1]*Dy1[1] + R2[2]*Dy1[2];
}

void acado_multQDy( real_t* const Q2, real_t* const Dy1, real_t* const QDy1 )
{
QDy1[0] = + Q2[0]*Dy1[0] + Q2[1]*Dy1[1] + Q2[2]*Dy1[2];
QDy1[1] = + Q2[3]*Dy1[0] + Q2[4]*Dy1[1] + Q2[5]*Dy1[2];
}

void acado_multEQDy( real_t* const E1, real_t* const QDy1, real_t* const U1 )
{
U1[0] += + E1[0]*QDy1[0] + E1[1]*QDy1[1];
}

void acado_multQETGx( real_t* const E1, real_t* const Gx1, real_t* const H101 )
{
H101[0] += + E1[0]*Gx1[0] + E1[1]*Gx1[2];
H101[1] += + E1[0]*Gx1[1] + E1[1]*Gx1[3];
}

void acado_zeroBlockH10( real_t* const H101 )
{
{ int lCopy; for (lCopy = 0; lCopy < 2; lCopy++) H101[ lCopy ] = 0; }
}

void acado_multEDu( real_t* const E1, real_t* const U1, real_t* const dNew )
{
dNew[0] += + E1[0]*U1[0];
dNew[1] += + E1[1]*U1[0];
}

void acado_macETSlu( real_t* const E0, real_t* const g1 )
{
g1[0] += 0.0;
;
}

void acado_condensePrep(  )
{
int lRun1;
int lRun2;
int lRun3;
int lRun4;
int lRun5;
/** Row vector of size: 40 */
static const int xBoundIndices[ 40 ] = 
{ 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41 };
acado_moveGuE( acadoWorkspace.evGu, acadoWorkspace.E );
acado_moveGxT( &(acadoWorkspace.evGx[ 4 ]), acadoWorkspace.T );
acado_multGxd( acadoWorkspace.d, &(acadoWorkspace.evGx[ 4 ]), &(acadoWorkspace.d[ 2 ]) );
acado_multGxGx( acadoWorkspace.T, acadoWorkspace.evGx, &(acadoWorkspace.evGx[ 4 ]) );

acado_multGxGu( acadoWorkspace.T, acadoWorkspace.E, &(acadoWorkspace.E[ 2 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 2 ]), &(acadoWorkspace.E[ 4 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 8 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 2 ]), &(acadoWorkspace.evGx[ 8 ]), &(acadoWorkspace.d[ 4 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 4 ]), &(acadoWorkspace.evGx[ 8 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 2 ]), &(acadoWorkspace.E[ 6 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 4 ]), &(acadoWorkspace.E[ 8 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 4 ]), &(acadoWorkspace.E[ 10 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 12 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 4 ]), &(acadoWorkspace.evGx[ 12 ]), &(acadoWorkspace.d[ 6 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 8 ]), &(acadoWorkspace.evGx[ 12 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 6 ]), &(acadoWorkspace.E[ 12 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 8 ]), &(acadoWorkspace.E[ 14 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 10 ]), &(acadoWorkspace.E[ 16 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 6 ]), &(acadoWorkspace.E[ 18 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 16 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 6 ]), &(acadoWorkspace.evGx[ 16 ]), &(acadoWorkspace.d[ 8 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 12 ]), &(acadoWorkspace.evGx[ 16 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 12 ]), &(acadoWorkspace.E[ 20 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 14 ]), &(acadoWorkspace.E[ 22 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 16 ]), &(acadoWorkspace.E[ 24 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 18 ]), &(acadoWorkspace.E[ 26 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 8 ]), &(acadoWorkspace.E[ 28 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 20 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 8 ]), &(acadoWorkspace.evGx[ 20 ]), &(acadoWorkspace.d[ 10 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 16 ]), &(acadoWorkspace.evGx[ 20 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 20 ]), &(acadoWorkspace.E[ 30 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 22 ]), &(acadoWorkspace.E[ 32 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 24 ]), &(acadoWorkspace.E[ 34 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 26 ]), &(acadoWorkspace.E[ 36 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 28 ]), &(acadoWorkspace.E[ 38 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 10 ]), &(acadoWorkspace.E[ 40 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 24 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 10 ]), &(acadoWorkspace.evGx[ 24 ]), &(acadoWorkspace.d[ 12 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 20 ]), &(acadoWorkspace.evGx[ 24 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 30 ]), &(acadoWorkspace.E[ 42 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 32 ]), &(acadoWorkspace.E[ 44 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 34 ]), &(acadoWorkspace.E[ 46 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 36 ]), &(acadoWorkspace.E[ 48 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 38 ]), &(acadoWorkspace.E[ 50 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 40 ]), &(acadoWorkspace.E[ 52 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 12 ]), &(acadoWorkspace.E[ 54 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 28 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 12 ]), &(acadoWorkspace.evGx[ 28 ]), &(acadoWorkspace.d[ 14 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 24 ]), &(acadoWorkspace.evGx[ 28 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 42 ]), &(acadoWorkspace.E[ 56 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 44 ]), &(acadoWorkspace.E[ 58 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 46 ]), &(acadoWorkspace.E[ 60 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 48 ]), &(acadoWorkspace.E[ 62 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 50 ]), &(acadoWorkspace.E[ 64 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 52 ]), &(acadoWorkspace.E[ 66 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 54 ]), &(acadoWorkspace.E[ 68 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 14 ]), &(acadoWorkspace.E[ 70 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 32 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 14 ]), &(acadoWorkspace.evGx[ 32 ]), &(acadoWorkspace.d[ 16 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 28 ]), &(acadoWorkspace.evGx[ 32 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.E[ 72 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.E[ 74 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 60 ]), &(acadoWorkspace.E[ 76 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 62 ]), &(acadoWorkspace.E[ 78 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 64 ]), &(acadoWorkspace.E[ 80 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 66 ]), &(acadoWorkspace.E[ 82 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 68 ]), &(acadoWorkspace.E[ 84 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 70 ]), &(acadoWorkspace.E[ 86 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 16 ]), &(acadoWorkspace.E[ 88 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 36 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 16 ]), &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.d[ 18 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 32 ]), &(acadoWorkspace.evGx[ 36 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.E[ 90 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.E[ 92 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.E[ 94 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 78 ]), &(acadoWorkspace.E[ 96 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 80 ]), &(acadoWorkspace.E[ 98 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 82 ]), &(acadoWorkspace.E[ 100 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 84 ]), &(acadoWorkspace.E[ 102 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 86 ]), &(acadoWorkspace.E[ 104 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 88 ]), &(acadoWorkspace.E[ 106 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 18 ]), &(acadoWorkspace.E[ 108 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 40 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 18 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.d[ 20 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.evGx[ 40 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.E[ 110 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.E[ 112 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.E[ 114 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.E[ 116 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 98 ]), &(acadoWorkspace.E[ 118 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 100 ]), &(acadoWorkspace.E[ 120 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 102 ]), &(acadoWorkspace.E[ 122 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 104 ]), &(acadoWorkspace.E[ 124 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 106 ]), &(acadoWorkspace.E[ 126 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 108 ]), &(acadoWorkspace.E[ 128 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 20 ]), &(acadoWorkspace.E[ 130 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 44 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 20 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.d[ 22 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.evGx[ 44 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.E[ 132 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.E[ 134 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.E[ 136 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.E[ 138 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.E[ 140 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 120 ]), &(acadoWorkspace.E[ 142 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 122 ]), &(acadoWorkspace.E[ 144 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 124 ]), &(acadoWorkspace.E[ 146 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 126 ]), &(acadoWorkspace.E[ 148 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 128 ]), &(acadoWorkspace.E[ 150 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 130 ]), &(acadoWorkspace.E[ 152 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 22 ]), &(acadoWorkspace.E[ 154 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 48 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 22 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.d[ 24 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.evGx[ 48 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.E[ 156 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.E[ 158 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.E[ 160 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.E[ 162 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.E[ 164 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.E[ 166 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 144 ]), &(acadoWorkspace.E[ 168 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 146 ]), &(acadoWorkspace.E[ 170 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 148 ]), &(acadoWorkspace.E[ 172 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 150 ]), &(acadoWorkspace.E[ 174 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 152 ]), &(acadoWorkspace.E[ 176 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 154 ]), &(acadoWorkspace.E[ 178 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 24 ]), &(acadoWorkspace.E[ 180 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 52 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 24 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.d[ 26 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.evGx[ 52 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.E[ 182 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.E[ 184 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.E[ 186 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.E[ 188 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.E[ 190 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.E[ 192 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.E[ 194 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 170 ]), &(acadoWorkspace.E[ 196 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 172 ]), &(acadoWorkspace.E[ 198 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 174 ]), &(acadoWorkspace.E[ 200 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 176 ]), &(acadoWorkspace.E[ 202 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 178 ]), &(acadoWorkspace.E[ 204 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 180 ]), &(acadoWorkspace.E[ 206 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 26 ]), &(acadoWorkspace.E[ 208 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 56 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 26 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.d[ 28 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.evGx[ 56 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.E[ 210 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.E[ 212 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.E[ 214 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.E[ 216 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.E[ 218 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.E[ 220 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.E[ 222 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.E[ 224 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 198 ]), &(acadoWorkspace.E[ 226 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 200 ]), &(acadoWorkspace.E[ 228 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 202 ]), &(acadoWorkspace.E[ 230 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 204 ]), &(acadoWorkspace.E[ 232 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 206 ]), &(acadoWorkspace.E[ 234 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 208 ]), &(acadoWorkspace.E[ 236 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 28 ]), &(acadoWorkspace.E[ 238 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 60 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 28 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.d[ 30 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.evGx[ 60 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.E[ 240 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.E[ 242 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.E[ 244 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.E[ 246 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.E[ 248 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.E[ 250 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.E[ 252 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.E[ 254 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.E[ 256 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 228 ]), &(acadoWorkspace.E[ 258 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 230 ]), &(acadoWorkspace.E[ 260 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 232 ]), &(acadoWorkspace.E[ 262 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 234 ]), &(acadoWorkspace.E[ 264 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 236 ]), &(acadoWorkspace.E[ 266 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 238 ]), &(acadoWorkspace.E[ 268 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 30 ]), &(acadoWorkspace.E[ 270 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 64 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 30 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.d[ 32 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.evGx[ 64 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.E[ 272 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.E[ 274 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.E[ 276 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.E[ 278 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.E[ 280 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.E[ 282 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.E[ 284 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.E[ 286 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.E[ 288 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.E[ 290 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 260 ]), &(acadoWorkspace.E[ 292 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 262 ]), &(acadoWorkspace.E[ 294 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 264 ]), &(acadoWorkspace.E[ 296 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 266 ]), &(acadoWorkspace.E[ 298 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 268 ]), &(acadoWorkspace.E[ 300 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 270 ]), &(acadoWorkspace.E[ 302 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 32 ]), &(acadoWorkspace.E[ 304 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 68 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 32 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.d[ 34 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.evGx[ 68 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.E[ 306 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.E[ 308 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.E[ 310 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.E[ 312 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.E[ 314 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.E[ 316 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.E[ 318 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.E[ 320 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.E[ 322 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.E[ 324 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.E[ 326 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 294 ]), &(acadoWorkspace.E[ 328 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 296 ]), &(acadoWorkspace.E[ 330 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 298 ]), &(acadoWorkspace.E[ 332 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 300 ]), &(acadoWorkspace.E[ 334 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 302 ]), &(acadoWorkspace.E[ 336 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 304 ]), &(acadoWorkspace.E[ 338 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 34 ]), &(acadoWorkspace.E[ 340 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 72 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 34 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.d[ 36 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.evGx[ 72 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.E[ 342 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.E[ 344 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.E[ 346 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.E[ 348 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.E[ 350 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.E[ 352 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.E[ 354 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.E[ 356 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.E[ 358 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.E[ 360 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.E[ 362 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.E[ 364 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 330 ]), &(acadoWorkspace.E[ 366 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 332 ]), &(acadoWorkspace.E[ 368 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 334 ]), &(acadoWorkspace.E[ 370 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 336 ]), &(acadoWorkspace.E[ 372 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 338 ]), &(acadoWorkspace.E[ 374 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 340 ]), &(acadoWorkspace.E[ 376 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 36 ]), &(acadoWorkspace.E[ 378 ]) );

acado_moveGxT( &(acadoWorkspace.evGx[ 76 ]), acadoWorkspace.T );
acado_multGxd( &(acadoWorkspace.d[ 36 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.d[ 38 ]) );
acado_multGxGx( acadoWorkspace.T, &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.evGx[ 76 ]) );

acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.E[ 380 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.E[ 382 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.E[ 384 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.E[ 386 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.E[ 388 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.E[ 390 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.E[ 392 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.E[ 394 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.E[ 396 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.E[ 398 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.E[ 400 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.E[ 402 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.E[ 404 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 368 ]), &(acadoWorkspace.E[ 406 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 370 ]), &(acadoWorkspace.E[ 408 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 372 ]), &(acadoWorkspace.E[ 410 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 374 ]), &(acadoWorkspace.E[ 412 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 376 ]), &(acadoWorkspace.E[ 414 ]) );
acado_multGxGu( acadoWorkspace.T, &(acadoWorkspace.E[ 378 ]), &(acadoWorkspace.E[ 416 ]) );

acado_moveGuE( &(acadoWorkspace.evGu[ 38 ]), &(acadoWorkspace.E[ 418 ]) );

acado_multGxGu( &(acadoWorkspace.Q1[ 4 ]), acadoWorkspace.E, acadoWorkspace.QE );
acado_multGxGu( &(acadoWorkspace.Q1[ 8 ]), &(acadoWorkspace.E[ 2 ]), &(acadoWorkspace.QE[ 2 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 8 ]), &(acadoWorkspace.E[ 4 ]), &(acadoWorkspace.QE[ 4 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 12 ]), &(acadoWorkspace.E[ 6 ]), &(acadoWorkspace.QE[ 6 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 12 ]), &(acadoWorkspace.E[ 8 ]), &(acadoWorkspace.QE[ 8 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 12 ]), &(acadoWorkspace.E[ 10 ]), &(acadoWorkspace.QE[ 10 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 16 ]), &(acadoWorkspace.E[ 12 ]), &(acadoWorkspace.QE[ 12 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 16 ]), &(acadoWorkspace.E[ 14 ]), &(acadoWorkspace.QE[ 14 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 16 ]), &(acadoWorkspace.E[ 16 ]), &(acadoWorkspace.QE[ 16 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 16 ]), &(acadoWorkspace.E[ 18 ]), &(acadoWorkspace.QE[ 18 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 20 ]), &(acadoWorkspace.E[ 20 ]), &(acadoWorkspace.QE[ 20 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 20 ]), &(acadoWorkspace.E[ 22 ]), &(acadoWorkspace.QE[ 22 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 20 ]), &(acadoWorkspace.E[ 24 ]), &(acadoWorkspace.QE[ 24 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 20 ]), &(acadoWorkspace.E[ 26 ]), &(acadoWorkspace.QE[ 26 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 20 ]), &(acadoWorkspace.E[ 28 ]), &(acadoWorkspace.QE[ 28 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 24 ]), &(acadoWorkspace.E[ 30 ]), &(acadoWorkspace.QE[ 30 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 24 ]), &(acadoWorkspace.E[ 32 ]), &(acadoWorkspace.QE[ 32 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 24 ]), &(acadoWorkspace.E[ 34 ]), &(acadoWorkspace.QE[ 34 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 24 ]), &(acadoWorkspace.E[ 36 ]), &(acadoWorkspace.QE[ 36 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 24 ]), &(acadoWorkspace.E[ 38 ]), &(acadoWorkspace.QE[ 38 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 24 ]), &(acadoWorkspace.E[ 40 ]), &(acadoWorkspace.QE[ 40 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 28 ]), &(acadoWorkspace.E[ 42 ]), &(acadoWorkspace.QE[ 42 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 28 ]), &(acadoWorkspace.E[ 44 ]), &(acadoWorkspace.QE[ 44 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 28 ]), &(acadoWorkspace.E[ 46 ]), &(acadoWorkspace.QE[ 46 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 28 ]), &(acadoWorkspace.E[ 48 ]), &(acadoWorkspace.QE[ 48 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 28 ]), &(acadoWorkspace.E[ 50 ]), &(acadoWorkspace.QE[ 50 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 28 ]), &(acadoWorkspace.E[ 52 ]), &(acadoWorkspace.QE[ 52 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 28 ]), &(acadoWorkspace.E[ 54 ]), &(acadoWorkspace.QE[ 54 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 32 ]), &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.QE[ 56 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 32 ]), &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.QE[ 58 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 32 ]), &(acadoWorkspace.E[ 60 ]), &(acadoWorkspace.QE[ 60 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 32 ]), &(acadoWorkspace.E[ 62 ]), &(acadoWorkspace.QE[ 62 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 32 ]), &(acadoWorkspace.E[ 64 ]), &(acadoWorkspace.QE[ 64 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 32 ]), &(acadoWorkspace.E[ 66 ]), &(acadoWorkspace.QE[ 66 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 32 ]), &(acadoWorkspace.E[ 68 ]), &(acadoWorkspace.QE[ 68 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 32 ]), &(acadoWorkspace.E[ 70 ]), &(acadoWorkspace.QE[ 70 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 36 ]), &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QE[ 72 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 36 ]), &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.QE[ 74 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 36 ]), &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.QE[ 76 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 36 ]), &(acadoWorkspace.E[ 78 ]), &(acadoWorkspace.QE[ 78 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 36 ]), &(acadoWorkspace.E[ 80 ]), &(acadoWorkspace.QE[ 80 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 36 ]), &(acadoWorkspace.E[ 82 ]), &(acadoWorkspace.QE[ 82 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 36 ]), &(acadoWorkspace.E[ 84 ]), &(acadoWorkspace.QE[ 84 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 36 ]), &(acadoWorkspace.E[ 86 ]), &(acadoWorkspace.QE[ 86 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 36 ]), &(acadoWorkspace.E[ 88 ]), &(acadoWorkspace.QE[ 88 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 90 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QE[ 92 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.QE[ 94 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.QE[ 96 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.E[ 98 ]), &(acadoWorkspace.QE[ 98 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.E[ 100 ]), &(acadoWorkspace.QE[ 100 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.E[ 102 ]), &(acadoWorkspace.QE[ 102 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.E[ 104 ]), &(acadoWorkspace.QE[ 104 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.E[ 106 ]), &(acadoWorkspace.QE[ 106 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.E[ 108 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 110 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 112 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QE[ 114 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.QE[ 116 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.QE[ 118 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 120 ]), &(acadoWorkspace.QE[ 120 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 122 ]), &(acadoWorkspace.QE[ 122 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 124 ]), &(acadoWorkspace.QE[ 124 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 126 ]), &(acadoWorkspace.QE[ 126 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 128 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.E[ 130 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 132 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 134 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 136 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QE[ 138 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.QE[ 140 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.QE[ 142 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 144 ]), &(acadoWorkspace.QE[ 144 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 146 ]), &(acadoWorkspace.QE[ 146 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 148 ]), &(acadoWorkspace.QE[ 148 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 150 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 152 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.E[ 154 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 156 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 158 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 160 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 162 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QE[ 164 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.QE[ 166 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.QE[ 168 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 170 ]), &(acadoWorkspace.QE[ 170 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 172 ]), &(acadoWorkspace.QE[ 172 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 174 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 176 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 178 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.E[ 180 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 182 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 184 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 186 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 188 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 190 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QE[ 192 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.QE[ 194 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.QE[ 196 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 198 ]), &(acadoWorkspace.QE[ 198 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 200 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 202 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 204 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 206 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.E[ 208 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 210 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 212 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 214 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 216 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 218 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 220 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QE[ 222 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.QE[ 224 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.QE[ 226 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 228 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 230 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 232 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 234 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 236 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.E[ 238 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 240 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 242 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 244 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 246 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 248 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 250 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 252 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QE[ 254 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.QE[ 256 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 260 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 262 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 264 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 266 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 268 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.E[ 270 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 272 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 274 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 276 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 278 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 280 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 282 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 284 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 286 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QE[ 288 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 294 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 296 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 298 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 300 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 302 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.E[ 304 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 306 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 308 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 310 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 312 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 314 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 316 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 318 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 320 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 322 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 330 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 332 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 334 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 336 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 338 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.E[ 340 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 342 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 344 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 346 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 348 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 350 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 352 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 354 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 356 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 358 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 368 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 370 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 372 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 374 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 376 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_multGxGu( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.E[ 378 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 380 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 382 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 384 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 386 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 388 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 390 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 392 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 394 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 396 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 398 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 400 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QE[ 402 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.QE[ 404 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 406 ]), &(acadoWorkspace.QE[ 406 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 408 ]), &(acadoWorkspace.QE[ 408 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 410 ]), &(acadoWorkspace.QE[ 410 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 412 ]), &(acadoWorkspace.QE[ 412 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 414 ]), &(acadoWorkspace.QE[ 414 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 416 ]), &(acadoWorkspace.QE[ 416 ]) );
acado_multGxGu( acadoWorkspace.QN1, &(acadoWorkspace.E[ 418 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_zeroBlockH10( acadoWorkspace.H10 );
acado_multQETGx( acadoWorkspace.QE, acadoWorkspace.evGx, acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 2 ]), &(acadoWorkspace.evGx[ 4 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 6 ]), &(acadoWorkspace.evGx[ 8 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 12 ]), &(acadoWorkspace.evGx[ 12 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 20 ]), &(acadoWorkspace.evGx[ 16 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 30 ]), &(acadoWorkspace.evGx[ 20 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 42 ]), &(acadoWorkspace.evGx[ 24 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 56 ]), &(acadoWorkspace.evGx[ 28 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 72 ]), &(acadoWorkspace.evGx[ 32 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 90 ]), &(acadoWorkspace.evGx[ 36 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 110 ]), &(acadoWorkspace.evGx[ 40 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 132 ]), &(acadoWorkspace.evGx[ 44 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 156 ]), &(acadoWorkspace.evGx[ 48 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 182 ]), &(acadoWorkspace.evGx[ 52 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 210 ]), &(acadoWorkspace.evGx[ 56 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 240 ]), &(acadoWorkspace.evGx[ 60 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 272 ]), &(acadoWorkspace.evGx[ 64 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 306 ]), &(acadoWorkspace.evGx[ 68 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 342 ]), &(acadoWorkspace.evGx[ 72 ]), acadoWorkspace.H10 );
acado_multQETGx( &(acadoWorkspace.QE[ 380 ]), &(acadoWorkspace.evGx[ 76 ]), acadoWorkspace.H10 );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 4 ]), &(acadoWorkspace.evGx[ 4 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 8 ]), &(acadoWorkspace.evGx[ 8 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 14 ]), &(acadoWorkspace.evGx[ 12 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 22 ]), &(acadoWorkspace.evGx[ 16 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 32 ]), &(acadoWorkspace.evGx[ 20 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 44 ]), &(acadoWorkspace.evGx[ 24 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 58 ]), &(acadoWorkspace.evGx[ 28 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 74 ]), &(acadoWorkspace.evGx[ 32 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 92 ]), &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 112 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 134 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 158 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 184 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 212 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 242 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 274 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 308 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 344 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 382 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 2 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 10 ]), &(acadoWorkspace.evGx[ 8 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 16 ]), &(acadoWorkspace.evGx[ 12 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 24 ]), &(acadoWorkspace.evGx[ 16 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 34 ]), &(acadoWorkspace.evGx[ 20 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 46 ]), &(acadoWorkspace.evGx[ 24 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 60 ]), &(acadoWorkspace.evGx[ 28 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 76 ]), &(acadoWorkspace.evGx[ 32 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 94 ]), &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 114 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 136 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 160 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 186 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 214 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 244 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 276 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 310 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 346 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 384 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 4 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 18 ]), &(acadoWorkspace.evGx[ 12 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 26 ]), &(acadoWorkspace.evGx[ 16 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 36 ]), &(acadoWorkspace.evGx[ 20 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 48 ]), &(acadoWorkspace.evGx[ 24 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 62 ]), &(acadoWorkspace.evGx[ 28 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 78 ]), &(acadoWorkspace.evGx[ 32 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 96 ]), &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 116 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 138 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 162 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 188 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 216 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 246 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 278 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 312 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 348 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 386 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 6 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 28 ]), &(acadoWorkspace.evGx[ 16 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 38 ]), &(acadoWorkspace.evGx[ 20 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 50 ]), &(acadoWorkspace.evGx[ 24 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 64 ]), &(acadoWorkspace.evGx[ 28 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 80 ]), &(acadoWorkspace.evGx[ 32 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 98 ]), &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 118 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 140 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 164 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 190 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 218 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 248 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 280 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 314 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 350 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 388 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 8 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 40 ]), &(acadoWorkspace.evGx[ 20 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 52 ]), &(acadoWorkspace.evGx[ 24 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 66 ]), &(acadoWorkspace.evGx[ 28 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 82 ]), &(acadoWorkspace.evGx[ 32 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 100 ]), &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 120 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 142 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 166 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 192 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 220 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 250 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 282 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 316 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 352 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 390 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 10 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 54 ]), &(acadoWorkspace.evGx[ 24 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 68 ]), &(acadoWorkspace.evGx[ 28 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 84 ]), &(acadoWorkspace.evGx[ 32 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 102 ]), &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 122 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 144 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 168 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 194 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 222 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 252 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 284 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 318 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 354 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 392 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 12 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 70 ]), &(acadoWorkspace.evGx[ 28 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 86 ]), &(acadoWorkspace.evGx[ 32 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 104 ]), &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 124 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 146 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 170 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 196 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 224 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 254 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 286 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 320 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 356 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 394 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 14 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 88 ]), &(acadoWorkspace.evGx[ 32 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 106 ]), &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 126 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 148 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 172 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 198 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 226 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 256 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 288 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 322 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 358 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 396 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 16 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 108 ]), &(acadoWorkspace.evGx[ 36 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 128 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 150 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 174 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 200 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 228 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 258 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 290 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 324 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 360 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 398 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 18 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 20 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 130 ]), &(acadoWorkspace.evGx[ 40 ]), &(acadoWorkspace.H10[ 20 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 152 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 20 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 176 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 20 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 202 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 20 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 230 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 20 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 260 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 20 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 292 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 20 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 326 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 20 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 362 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 20 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 400 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 20 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 22 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 154 ]), &(acadoWorkspace.evGx[ 44 ]), &(acadoWorkspace.H10[ 22 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 178 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 22 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 204 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 22 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 232 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 22 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 262 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 22 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 294 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 22 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 328 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 22 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 364 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 22 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 402 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 22 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 24 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 180 ]), &(acadoWorkspace.evGx[ 48 ]), &(acadoWorkspace.H10[ 24 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 206 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 24 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 234 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 24 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 264 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 24 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 296 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 24 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 330 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 24 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 366 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 24 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 404 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 24 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 26 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 208 ]), &(acadoWorkspace.evGx[ 52 ]), &(acadoWorkspace.H10[ 26 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 236 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 26 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 266 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 26 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 298 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 26 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 332 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 26 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 368 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 26 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 406 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 26 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 28 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 238 ]), &(acadoWorkspace.evGx[ 56 ]), &(acadoWorkspace.H10[ 28 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 268 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 28 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 300 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 28 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 334 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 28 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 370 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 28 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 408 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 28 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 30 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 270 ]), &(acadoWorkspace.evGx[ 60 ]), &(acadoWorkspace.H10[ 30 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 302 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 30 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 336 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 30 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 372 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 30 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 410 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 30 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 32 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 304 ]), &(acadoWorkspace.evGx[ 64 ]), &(acadoWorkspace.H10[ 32 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 338 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 32 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 374 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 32 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 412 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 32 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 34 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 340 ]), &(acadoWorkspace.evGx[ 68 ]), &(acadoWorkspace.H10[ 34 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 376 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 34 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 414 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 34 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 36 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 378 ]), &(acadoWorkspace.evGx[ 72 ]), &(acadoWorkspace.H10[ 36 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 416 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 36 ]) );
acado_zeroBlockH10( &(acadoWorkspace.H10[ 38 ]) );
acado_multQETGx( &(acadoWorkspace.QE[ 418 ]), &(acadoWorkspace.evGx[ 76 ]), &(acadoWorkspace.H10[ 38 ]) );

acado_setBlockH11_R1( 0, 0, acadoWorkspace.R1 );
acado_setBlockH11( 0, 0, acadoWorkspace.E, acadoWorkspace.QE );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 2 ]), &(acadoWorkspace.QE[ 2 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 6 ]), &(acadoWorkspace.QE[ 6 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 12 ]), &(acadoWorkspace.QE[ 12 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 20 ]), &(acadoWorkspace.QE[ 20 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 30 ]), &(acadoWorkspace.QE[ 30 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 42 ]), &(acadoWorkspace.QE[ 42 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.QE[ 56 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QE[ 72 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 90 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 110 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 132 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 156 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 182 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 210 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 240 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 272 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 306 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 342 ]) );
acado_setBlockH11( 0, 0, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 380 ]) );

acado_zeroBlockH11( 0, 1 );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 2 ]), &(acadoWorkspace.QE[ 4 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 6 ]), &(acadoWorkspace.QE[ 8 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 12 ]), &(acadoWorkspace.QE[ 14 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 20 ]), &(acadoWorkspace.QE[ 22 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 30 ]), &(acadoWorkspace.QE[ 32 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 42 ]), &(acadoWorkspace.QE[ 44 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.QE[ 58 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QE[ 74 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 92 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 112 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 134 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 158 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 184 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 212 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 242 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 274 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 308 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 344 ]) );
acado_setBlockH11( 0, 1, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 382 ]) );

acado_zeroBlockH11( 0, 2 );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 6 ]), &(acadoWorkspace.QE[ 10 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 12 ]), &(acadoWorkspace.QE[ 16 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 20 ]), &(acadoWorkspace.QE[ 24 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 30 ]), &(acadoWorkspace.QE[ 34 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 42 ]), &(acadoWorkspace.QE[ 46 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.QE[ 60 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QE[ 76 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 94 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 114 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 136 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 160 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 186 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 214 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 244 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 276 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 310 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 346 ]) );
acado_setBlockH11( 0, 2, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 384 ]) );

acado_zeroBlockH11( 0, 3 );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 12 ]), &(acadoWorkspace.QE[ 18 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 20 ]), &(acadoWorkspace.QE[ 26 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 30 ]), &(acadoWorkspace.QE[ 36 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 42 ]), &(acadoWorkspace.QE[ 48 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.QE[ 62 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QE[ 78 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 96 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 116 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 138 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 162 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 188 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 216 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 246 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 278 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 312 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 348 ]) );
acado_setBlockH11( 0, 3, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 386 ]) );

acado_zeroBlockH11( 0, 4 );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 20 ]), &(acadoWorkspace.QE[ 28 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 30 ]), &(acadoWorkspace.QE[ 38 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 42 ]), &(acadoWorkspace.QE[ 50 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.QE[ 64 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QE[ 80 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 98 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 118 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 140 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 164 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 190 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 218 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 248 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 280 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 314 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 350 ]) );
acado_setBlockH11( 0, 4, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 388 ]) );

acado_zeroBlockH11( 0, 5 );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 30 ]), &(acadoWorkspace.QE[ 40 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 42 ]), &(acadoWorkspace.QE[ 52 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.QE[ 66 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QE[ 82 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 100 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 120 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 142 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 166 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 192 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 220 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 250 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 282 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 316 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 352 ]) );
acado_setBlockH11( 0, 5, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 390 ]) );

acado_zeroBlockH11( 0, 6 );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 42 ]), &(acadoWorkspace.QE[ 54 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.QE[ 68 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QE[ 84 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 102 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 122 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 144 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 168 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 194 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 222 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 252 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 284 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 318 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 354 ]) );
acado_setBlockH11( 0, 6, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 392 ]) );

acado_zeroBlockH11( 0, 7 );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.QE[ 70 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QE[ 86 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 104 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 124 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 146 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 170 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 196 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 224 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 254 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 286 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 320 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 356 ]) );
acado_setBlockH11( 0, 7, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 394 ]) );

acado_zeroBlockH11( 0, 8 );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QE[ 88 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 106 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 126 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 148 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 172 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 198 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 226 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 256 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 288 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 322 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 358 ]) );
acado_setBlockH11( 0, 8, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 396 ]) );

acado_zeroBlockH11( 0, 9 );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_setBlockH11( 0, 9, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 398 ]) );

acado_zeroBlockH11( 0, 10 );
acado_setBlockH11( 0, 10, &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 0, 10, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 0, 10, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 0, 10, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 0, 10, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 0, 10, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 0, 10, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 0, 10, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 0, 10, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 0, 10, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 0, 11 );
acado_setBlockH11( 0, 11, &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 0, 11, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 0, 11, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 0, 11, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 0, 11, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 0, 11, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 0, 11, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 0, 11, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 0, 11, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 0, 12 );
acado_setBlockH11( 0, 12, &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 0, 12, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 0, 12, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 0, 12, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 0, 12, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 0, 12, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 0, 12, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 0, 12, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 0, 13 );
acado_setBlockH11( 0, 13, &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 0, 13, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 0, 13, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 0, 13, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 0, 13, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 0, 13, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 0, 13, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 0, 14 );
acado_setBlockH11( 0, 14, &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 0, 14, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 0, 14, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 0, 14, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 0, 14, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 0, 14, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 0, 15 );
acado_setBlockH11( 0, 15, &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 0, 15, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 0, 15, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 0, 15, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 0, 15, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 0, 16 );
acado_setBlockH11( 0, 16, &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 0, 16, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 0, 16, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 0, 16, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 0, 17 );
acado_setBlockH11( 0, 17, &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 0, 17, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 0, 17, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 0, 18 );
acado_setBlockH11( 0, 18, &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 0, 18, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 0, 19 );
acado_setBlockH11( 0, 19, &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 1, 1, &(acadoWorkspace.R1[ 1 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 4 ]), &(acadoWorkspace.QE[ 4 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 8 ]), &(acadoWorkspace.QE[ 8 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 14 ]), &(acadoWorkspace.QE[ 14 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 22 ]), &(acadoWorkspace.QE[ 22 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 32 ]), &(acadoWorkspace.QE[ 32 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 44 ]), &(acadoWorkspace.QE[ 44 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.QE[ 58 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.QE[ 74 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QE[ 92 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 112 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 134 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 158 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 184 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 212 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 242 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 274 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 308 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 344 ]) );
acado_setBlockH11( 1, 1, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 382 ]) );

acado_zeroBlockH11( 1, 2 );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 8 ]), &(acadoWorkspace.QE[ 10 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 14 ]), &(acadoWorkspace.QE[ 16 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 22 ]), &(acadoWorkspace.QE[ 24 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 32 ]), &(acadoWorkspace.QE[ 34 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 44 ]), &(acadoWorkspace.QE[ 46 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.QE[ 60 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.QE[ 76 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QE[ 94 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 114 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 136 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 160 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 186 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 214 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 244 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 276 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 310 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 346 ]) );
acado_setBlockH11( 1, 2, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 384 ]) );

acado_zeroBlockH11( 1, 3 );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 14 ]), &(acadoWorkspace.QE[ 18 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 22 ]), &(acadoWorkspace.QE[ 26 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 32 ]), &(acadoWorkspace.QE[ 36 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 44 ]), &(acadoWorkspace.QE[ 48 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.QE[ 62 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.QE[ 78 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QE[ 96 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 116 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 138 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 162 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 188 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 216 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 246 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 278 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 312 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 348 ]) );
acado_setBlockH11( 1, 3, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 386 ]) );

acado_zeroBlockH11( 1, 4 );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 22 ]), &(acadoWorkspace.QE[ 28 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 32 ]), &(acadoWorkspace.QE[ 38 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 44 ]), &(acadoWorkspace.QE[ 50 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.QE[ 64 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.QE[ 80 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QE[ 98 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 118 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 140 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 164 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 190 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 218 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 248 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 280 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 314 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 350 ]) );
acado_setBlockH11( 1, 4, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 388 ]) );

acado_zeroBlockH11( 1, 5 );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 32 ]), &(acadoWorkspace.QE[ 40 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 44 ]), &(acadoWorkspace.QE[ 52 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.QE[ 66 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.QE[ 82 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QE[ 100 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 120 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 142 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 166 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 192 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 220 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 250 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 282 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 316 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 352 ]) );
acado_setBlockH11( 1, 5, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 390 ]) );

acado_zeroBlockH11( 1, 6 );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 44 ]), &(acadoWorkspace.QE[ 54 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.QE[ 68 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.QE[ 84 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QE[ 102 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 122 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 144 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 168 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 194 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 222 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 252 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 284 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 318 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 354 ]) );
acado_setBlockH11( 1, 6, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 392 ]) );

acado_zeroBlockH11( 1, 7 );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.QE[ 70 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.QE[ 86 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QE[ 104 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 124 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 146 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 170 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 196 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 224 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 254 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 286 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 320 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 356 ]) );
acado_setBlockH11( 1, 7, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 394 ]) );

acado_zeroBlockH11( 1, 8 );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.QE[ 88 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QE[ 106 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 126 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 148 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 172 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 198 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 226 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 256 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 288 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 322 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 358 ]) );
acado_setBlockH11( 1, 8, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 396 ]) );

acado_zeroBlockH11( 1, 9 );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_setBlockH11( 1, 9, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 398 ]) );

acado_zeroBlockH11( 1, 10 );
acado_setBlockH11( 1, 10, &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 1, 10, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 1, 10, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 1, 10, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 1, 10, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 1, 10, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 1, 10, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 1, 10, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 1, 10, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 1, 10, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 1, 11 );
acado_setBlockH11( 1, 11, &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 1, 11, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 1, 11, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 1, 11, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 1, 11, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 1, 11, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 1, 11, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 1, 11, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 1, 11, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 1, 12 );
acado_setBlockH11( 1, 12, &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 1, 12, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 1, 12, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 1, 12, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 1, 12, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 1, 12, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 1, 12, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 1, 12, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 1, 13 );
acado_setBlockH11( 1, 13, &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 1, 13, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 1, 13, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 1, 13, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 1, 13, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 1, 13, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 1, 13, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 1, 14 );
acado_setBlockH11( 1, 14, &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 1, 14, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 1, 14, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 1, 14, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 1, 14, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 1, 14, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 1, 15 );
acado_setBlockH11( 1, 15, &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 1, 15, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 1, 15, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 1, 15, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 1, 15, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 1, 16 );
acado_setBlockH11( 1, 16, &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 1, 16, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 1, 16, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 1, 16, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 1, 17 );
acado_setBlockH11( 1, 17, &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 1, 17, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 1, 17, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 1, 18 );
acado_setBlockH11( 1, 18, &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 1, 18, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 1, 19 );
acado_setBlockH11( 1, 19, &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 2, 2, &(acadoWorkspace.R1[ 2 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 10 ]), &(acadoWorkspace.QE[ 10 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 16 ]), &(acadoWorkspace.QE[ 16 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 24 ]), &(acadoWorkspace.QE[ 24 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 34 ]), &(acadoWorkspace.QE[ 34 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 46 ]), &(acadoWorkspace.QE[ 46 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 60 ]), &(acadoWorkspace.QE[ 60 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.QE[ 76 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.QE[ 94 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QE[ 114 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 136 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 160 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 186 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 214 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 244 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 276 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 310 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 346 ]) );
acado_setBlockH11( 2, 2, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 384 ]) );

acado_zeroBlockH11( 2, 3 );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 16 ]), &(acadoWorkspace.QE[ 18 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 24 ]), &(acadoWorkspace.QE[ 26 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 34 ]), &(acadoWorkspace.QE[ 36 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 46 ]), &(acadoWorkspace.QE[ 48 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 60 ]), &(acadoWorkspace.QE[ 62 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.QE[ 78 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.QE[ 96 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QE[ 116 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 138 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 162 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 188 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 216 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 246 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 278 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 312 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 348 ]) );
acado_setBlockH11( 2, 3, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 386 ]) );

acado_zeroBlockH11( 2, 4 );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 24 ]), &(acadoWorkspace.QE[ 28 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 34 ]), &(acadoWorkspace.QE[ 38 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 46 ]), &(acadoWorkspace.QE[ 50 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 60 ]), &(acadoWorkspace.QE[ 64 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.QE[ 80 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.QE[ 98 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QE[ 118 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 140 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 164 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 190 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 218 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 248 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 280 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 314 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 350 ]) );
acado_setBlockH11( 2, 4, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 388 ]) );

acado_zeroBlockH11( 2, 5 );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 34 ]), &(acadoWorkspace.QE[ 40 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 46 ]), &(acadoWorkspace.QE[ 52 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 60 ]), &(acadoWorkspace.QE[ 66 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.QE[ 82 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.QE[ 100 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QE[ 120 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 142 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 166 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 192 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 220 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 250 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 282 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 316 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 352 ]) );
acado_setBlockH11( 2, 5, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 390 ]) );

acado_zeroBlockH11( 2, 6 );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 46 ]), &(acadoWorkspace.QE[ 54 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 60 ]), &(acadoWorkspace.QE[ 68 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.QE[ 84 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.QE[ 102 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QE[ 122 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 144 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 168 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 194 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 222 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 252 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 284 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 318 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 354 ]) );
acado_setBlockH11( 2, 6, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 392 ]) );

acado_zeroBlockH11( 2, 7 );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 60 ]), &(acadoWorkspace.QE[ 70 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.QE[ 86 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.QE[ 104 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QE[ 124 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 146 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 170 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 196 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 224 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 254 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 286 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 320 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 356 ]) );
acado_setBlockH11( 2, 7, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 394 ]) );

acado_zeroBlockH11( 2, 8 );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.QE[ 88 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.QE[ 106 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QE[ 126 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 148 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 172 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 198 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 226 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 256 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 288 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 322 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 358 ]) );
acado_setBlockH11( 2, 8, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 396 ]) );

acado_zeroBlockH11( 2, 9 );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_setBlockH11( 2, 9, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 398 ]) );

acado_zeroBlockH11( 2, 10 );
acado_setBlockH11( 2, 10, &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 2, 10, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 2, 10, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 2, 10, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 2, 10, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 2, 10, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 2, 10, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 2, 10, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 2, 10, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 2, 10, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 2, 11 );
acado_setBlockH11( 2, 11, &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 2, 11, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 2, 11, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 2, 11, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 2, 11, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 2, 11, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 2, 11, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 2, 11, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 2, 11, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 2, 12 );
acado_setBlockH11( 2, 12, &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 2, 12, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 2, 12, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 2, 12, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 2, 12, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 2, 12, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 2, 12, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 2, 12, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 2, 13 );
acado_setBlockH11( 2, 13, &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 2, 13, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 2, 13, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 2, 13, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 2, 13, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 2, 13, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 2, 13, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 2, 14 );
acado_setBlockH11( 2, 14, &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 2, 14, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 2, 14, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 2, 14, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 2, 14, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 2, 14, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 2, 15 );
acado_setBlockH11( 2, 15, &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 2, 15, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 2, 15, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 2, 15, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 2, 15, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 2, 16 );
acado_setBlockH11( 2, 16, &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 2, 16, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 2, 16, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 2, 16, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 2, 17 );
acado_setBlockH11( 2, 17, &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 2, 17, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 2, 17, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 2, 18 );
acado_setBlockH11( 2, 18, &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 2, 18, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 2, 19 );
acado_setBlockH11( 2, 19, &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 3, 3, &(acadoWorkspace.R1[ 3 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 18 ]), &(acadoWorkspace.QE[ 18 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 26 ]), &(acadoWorkspace.QE[ 26 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 36 ]), &(acadoWorkspace.QE[ 36 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 48 ]), &(acadoWorkspace.QE[ 48 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 62 ]), &(acadoWorkspace.QE[ 62 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 78 ]), &(acadoWorkspace.QE[ 78 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.QE[ 96 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.QE[ 116 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QE[ 138 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 162 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 188 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 216 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 246 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 278 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 312 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 348 ]) );
acado_setBlockH11( 3, 3, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 386 ]) );

acado_zeroBlockH11( 3, 4 );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 26 ]), &(acadoWorkspace.QE[ 28 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 36 ]), &(acadoWorkspace.QE[ 38 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 48 ]), &(acadoWorkspace.QE[ 50 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 62 ]), &(acadoWorkspace.QE[ 64 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 78 ]), &(acadoWorkspace.QE[ 80 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.QE[ 98 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.QE[ 118 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QE[ 140 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 164 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 190 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 218 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 248 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 280 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 314 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 350 ]) );
acado_setBlockH11( 3, 4, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 388 ]) );

acado_zeroBlockH11( 3, 5 );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 36 ]), &(acadoWorkspace.QE[ 40 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 48 ]), &(acadoWorkspace.QE[ 52 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 62 ]), &(acadoWorkspace.QE[ 66 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 78 ]), &(acadoWorkspace.QE[ 82 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.QE[ 100 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.QE[ 120 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QE[ 142 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 166 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 192 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 220 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 250 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 282 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 316 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 352 ]) );
acado_setBlockH11( 3, 5, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 390 ]) );

acado_zeroBlockH11( 3, 6 );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 48 ]), &(acadoWorkspace.QE[ 54 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 62 ]), &(acadoWorkspace.QE[ 68 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 78 ]), &(acadoWorkspace.QE[ 84 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.QE[ 102 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.QE[ 122 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QE[ 144 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 168 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 194 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 222 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 252 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 284 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 318 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 354 ]) );
acado_setBlockH11( 3, 6, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 392 ]) );

acado_zeroBlockH11( 3, 7 );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 62 ]), &(acadoWorkspace.QE[ 70 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 78 ]), &(acadoWorkspace.QE[ 86 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.QE[ 104 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.QE[ 124 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QE[ 146 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 170 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 196 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 224 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 254 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 286 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 320 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 356 ]) );
acado_setBlockH11( 3, 7, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 394 ]) );

acado_zeroBlockH11( 3, 8 );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 78 ]), &(acadoWorkspace.QE[ 88 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.QE[ 106 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.QE[ 126 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QE[ 148 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 172 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 198 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 226 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 256 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 288 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 322 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 358 ]) );
acado_setBlockH11( 3, 8, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 396 ]) );

acado_zeroBlockH11( 3, 9 );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_setBlockH11( 3, 9, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 398 ]) );

acado_zeroBlockH11( 3, 10 );
acado_setBlockH11( 3, 10, &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 3, 10, &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 3, 10, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 3, 10, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 3, 10, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 3, 10, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 3, 10, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 3, 10, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 3, 10, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 3, 10, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 3, 11 );
acado_setBlockH11( 3, 11, &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 3, 11, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 3, 11, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 3, 11, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 3, 11, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 3, 11, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 3, 11, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 3, 11, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 3, 11, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 3, 12 );
acado_setBlockH11( 3, 12, &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 3, 12, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 3, 12, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 3, 12, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 3, 12, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 3, 12, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 3, 12, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 3, 12, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 3, 13 );
acado_setBlockH11( 3, 13, &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 3, 13, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 3, 13, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 3, 13, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 3, 13, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 3, 13, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 3, 13, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 3, 14 );
acado_setBlockH11( 3, 14, &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 3, 14, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 3, 14, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 3, 14, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 3, 14, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 3, 14, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 3, 15 );
acado_setBlockH11( 3, 15, &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 3, 15, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 3, 15, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 3, 15, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 3, 15, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 3, 16 );
acado_setBlockH11( 3, 16, &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 3, 16, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 3, 16, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 3, 16, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 3, 17 );
acado_setBlockH11( 3, 17, &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 3, 17, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 3, 17, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 3, 18 );
acado_setBlockH11( 3, 18, &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 3, 18, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 3, 19 );
acado_setBlockH11( 3, 19, &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 4, 4, &(acadoWorkspace.R1[ 4 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 28 ]), &(acadoWorkspace.QE[ 28 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 38 ]), &(acadoWorkspace.QE[ 38 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 50 ]), &(acadoWorkspace.QE[ 50 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 64 ]), &(acadoWorkspace.QE[ 64 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 80 ]), &(acadoWorkspace.QE[ 80 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 98 ]), &(acadoWorkspace.QE[ 98 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.QE[ 118 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.QE[ 140 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QE[ 164 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 190 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 218 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 248 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 280 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 314 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 350 ]) );
acado_setBlockH11( 4, 4, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 388 ]) );

acado_zeroBlockH11( 4, 5 );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 38 ]), &(acadoWorkspace.QE[ 40 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 50 ]), &(acadoWorkspace.QE[ 52 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 64 ]), &(acadoWorkspace.QE[ 66 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 80 ]), &(acadoWorkspace.QE[ 82 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 98 ]), &(acadoWorkspace.QE[ 100 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.QE[ 120 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.QE[ 142 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QE[ 166 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 192 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 220 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 250 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 282 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 316 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 352 ]) );
acado_setBlockH11( 4, 5, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 390 ]) );

acado_zeroBlockH11( 4, 6 );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 50 ]), &(acadoWorkspace.QE[ 54 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 64 ]), &(acadoWorkspace.QE[ 68 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 80 ]), &(acadoWorkspace.QE[ 84 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 98 ]), &(acadoWorkspace.QE[ 102 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.QE[ 122 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.QE[ 144 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QE[ 168 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 194 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 222 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 252 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 284 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 318 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 354 ]) );
acado_setBlockH11( 4, 6, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 392 ]) );

acado_zeroBlockH11( 4, 7 );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 64 ]), &(acadoWorkspace.QE[ 70 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 80 ]), &(acadoWorkspace.QE[ 86 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 98 ]), &(acadoWorkspace.QE[ 104 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.QE[ 124 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.QE[ 146 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QE[ 170 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 196 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 224 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 254 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 286 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 320 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 356 ]) );
acado_setBlockH11( 4, 7, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 394 ]) );

acado_zeroBlockH11( 4, 8 );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 80 ]), &(acadoWorkspace.QE[ 88 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 98 ]), &(acadoWorkspace.QE[ 106 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.QE[ 126 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.QE[ 148 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QE[ 172 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 198 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 226 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 256 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 288 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 322 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 358 ]) );
acado_setBlockH11( 4, 8, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 396 ]) );

acado_zeroBlockH11( 4, 9 );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 98 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_setBlockH11( 4, 9, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 398 ]) );

acado_zeroBlockH11( 4, 10 );
acado_setBlockH11( 4, 10, &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 4, 10, &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 4, 10, &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 4, 10, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 4, 10, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 4, 10, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 4, 10, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 4, 10, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 4, 10, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 4, 10, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 4, 11 );
acado_setBlockH11( 4, 11, &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 4, 11, &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 4, 11, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 4, 11, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 4, 11, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 4, 11, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 4, 11, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 4, 11, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 4, 11, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 4, 12 );
acado_setBlockH11( 4, 12, &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 4, 12, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 4, 12, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 4, 12, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 4, 12, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 4, 12, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 4, 12, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 4, 12, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 4, 13 );
acado_setBlockH11( 4, 13, &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 4, 13, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 4, 13, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 4, 13, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 4, 13, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 4, 13, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 4, 13, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 4, 14 );
acado_setBlockH11( 4, 14, &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 4, 14, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 4, 14, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 4, 14, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 4, 14, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 4, 14, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 4, 15 );
acado_setBlockH11( 4, 15, &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 4, 15, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 4, 15, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 4, 15, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 4, 15, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 4, 16 );
acado_setBlockH11( 4, 16, &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 4, 16, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 4, 16, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 4, 16, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 4, 17 );
acado_setBlockH11( 4, 17, &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 4, 17, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 4, 17, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 4, 18 );
acado_setBlockH11( 4, 18, &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 4, 18, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 4, 19 );
acado_setBlockH11( 4, 19, &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 5, 5, &(acadoWorkspace.R1[ 5 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 40 ]), &(acadoWorkspace.QE[ 40 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 52 ]), &(acadoWorkspace.QE[ 52 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 66 ]), &(acadoWorkspace.QE[ 66 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 82 ]), &(acadoWorkspace.QE[ 82 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 100 ]), &(acadoWorkspace.QE[ 100 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 120 ]), &(acadoWorkspace.QE[ 120 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.QE[ 142 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.QE[ 166 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QE[ 192 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 220 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 250 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 282 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 316 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 352 ]) );
acado_setBlockH11( 5, 5, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 390 ]) );

acado_zeroBlockH11( 5, 6 );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 52 ]), &(acadoWorkspace.QE[ 54 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 66 ]), &(acadoWorkspace.QE[ 68 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 82 ]), &(acadoWorkspace.QE[ 84 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 100 ]), &(acadoWorkspace.QE[ 102 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 120 ]), &(acadoWorkspace.QE[ 122 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.QE[ 144 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.QE[ 168 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QE[ 194 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 222 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 252 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 284 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 318 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 354 ]) );
acado_setBlockH11( 5, 6, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 392 ]) );

acado_zeroBlockH11( 5, 7 );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 66 ]), &(acadoWorkspace.QE[ 70 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 82 ]), &(acadoWorkspace.QE[ 86 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 100 ]), &(acadoWorkspace.QE[ 104 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 120 ]), &(acadoWorkspace.QE[ 124 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.QE[ 146 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.QE[ 170 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QE[ 196 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 224 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 254 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 286 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 320 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 356 ]) );
acado_setBlockH11( 5, 7, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 394 ]) );

acado_zeroBlockH11( 5, 8 );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 82 ]), &(acadoWorkspace.QE[ 88 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 100 ]), &(acadoWorkspace.QE[ 106 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 120 ]), &(acadoWorkspace.QE[ 126 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.QE[ 148 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.QE[ 172 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QE[ 198 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 226 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 256 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 288 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 322 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 358 ]) );
acado_setBlockH11( 5, 8, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 396 ]) );

acado_zeroBlockH11( 5, 9 );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 100 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 120 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_setBlockH11( 5, 9, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 398 ]) );

acado_zeroBlockH11( 5, 10 );
acado_setBlockH11( 5, 10, &(acadoWorkspace.E[ 120 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 5, 10, &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 5, 10, &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 5, 10, &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 5, 10, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 5, 10, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 5, 10, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 5, 10, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 5, 10, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 5, 10, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 5, 11 );
acado_setBlockH11( 5, 11, &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 5, 11, &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 5, 11, &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 5, 11, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 5, 11, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 5, 11, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 5, 11, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 5, 11, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 5, 11, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 5, 12 );
acado_setBlockH11( 5, 12, &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 5, 12, &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 5, 12, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 5, 12, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 5, 12, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 5, 12, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 5, 12, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 5, 12, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 5, 13 );
acado_setBlockH11( 5, 13, &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 5, 13, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 5, 13, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 5, 13, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 5, 13, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 5, 13, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 5, 13, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 5, 14 );
acado_setBlockH11( 5, 14, &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 5, 14, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 5, 14, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 5, 14, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 5, 14, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 5, 14, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 5, 15 );
acado_setBlockH11( 5, 15, &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 5, 15, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 5, 15, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 5, 15, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 5, 15, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 5, 16 );
acado_setBlockH11( 5, 16, &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 5, 16, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 5, 16, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 5, 16, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 5, 17 );
acado_setBlockH11( 5, 17, &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 5, 17, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 5, 17, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 5, 18 );
acado_setBlockH11( 5, 18, &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 5, 18, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 5, 19 );
acado_setBlockH11( 5, 19, &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 6, 6, &(acadoWorkspace.R1[ 6 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 54 ]), &(acadoWorkspace.QE[ 54 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 68 ]), &(acadoWorkspace.QE[ 68 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 84 ]), &(acadoWorkspace.QE[ 84 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 102 ]), &(acadoWorkspace.QE[ 102 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 122 ]), &(acadoWorkspace.QE[ 122 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 144 ]), &(acadoWorkspace.QE[ 144 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.QE[ 168 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.QE[ 194 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QE[ 222 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 252 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 284 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 318 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 354 ]) );
acado_setBlockH11( 6, 6, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 392 ]) );

acado_zeroBlockH11( 6, 7 );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 68 ]), &(acadoWorkspace.QE[ 70 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 84 ]), &(acadoWorkspace.QE[ 86 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 102 ]), &(acadoWorkspace.QE[ 104 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 122 ]), &(acadoWorkspace.QE[ 124 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 144 ]), &(acadoWorkspace.QE[ 146 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.QE[ 170 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.QE[ 196 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QE[ 224 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 254 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 286 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 320 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 356 ]) );
acado_setBlockH11( 6, 7, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 394 ]) );

acado_zeroBlockH11( 6, 8 );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 84 ]), &(acadoWorkspace.QE[ 88 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 102 ]), &(acadoWorkspace.QE[ 106 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 122 ]), &(acadoWorkspace.QE[ 126 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 144 ]), &(acadoWorkspace.QE[ 148 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.QE[ 172 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.QE[ 198 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QE[ 226 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 256 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 288 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 322 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 358 ]) );
acado_setBlockH11( 6, 8, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 396 ]) );

acado_zeroBlockH11( 6, 9 );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 102 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 122 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 144 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_setBlockH11( 6, 9, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 398 ]) );

acado_zeroBlockH11( 6, 10 );
acado_setBlockH11( 6, 10, &(acadoWorkspace.E[ 122 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 6, 10, &(acadoWorkspace.E[ 144 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 6, 10, &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 6, 10, &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 6, 10, &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 6, 10, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 6, 10, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 6, 10, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 6, 10, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 6, 10, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 6, 11 );
acado_setBlockH11( 6, 11, &(acadoWorkspace.E[ 144 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 6, 11, &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 6, 11, &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 6, 11, &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 6, 11, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 6, 11, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 6, 11, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 6, 11, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 6, 11, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 6, 12 );
acado_setBlockH11( 6, 12, &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 6, 12, &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 6, 12, &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 6, 12, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 6, 12, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 6, 12, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 6, 12, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 6, 12, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 6, 13 );
acado_setBlockH11( 6, 13, &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 6, 13, &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 6, 13, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 6, 13, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 6, 13, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 6, 13, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 6, 13, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 6, 14 );
acado_setBlockH11( 6, 14, &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 6, 14, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 6, 14, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 6, 14, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 6, 14, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 6, 14, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 6, 15 );
acado_setBlockH11( 6, 15, &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 6, 15, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 6, 15, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 6, 15, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 6, 15, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 6, 16 );
acado_setBlockH11( 6, 16, &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 6, 16, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 6, 16, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 6, 16, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 6, 17 );
acado_setBlockH11( 6, 17, &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 6, 17, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 6, 17, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 6, 18 );
acado_setBlockH11( 6, 18, &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 6, 18, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 6, 19 );
acado_setBlockH11( 6, 19, &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 7, 7, &(acadoWorkspace.R1[ 7 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 70 ]), &(acadoWorkspace.QE[ 70 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 86 ]), &(acadoWorkspace.QE[ 86 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 104 ]), &(acadoWorkspace.QE[ 104 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 124 ]), &(acadoWorkspace.QE[ 124 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 146 ]), &(acadoWorkspace.QE[ 146 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 170 ]), &(acadoWorkspace.QE[ 170 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.QE[ 196 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.QE[ 224 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QE[ 254 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 286 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 320 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 356 ]) );
acado_setBlockH11( 7, 7, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 394 ]) );

acado_zeroBlockH11( 7, 8 );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 86 ]), &(acadoWorkspace.QE[ 88 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 104 ]), &(acadoWorkspace.QE[ 106 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 124 ]), &(acadoWorkspace.QE[ 126 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 146 ]), &(acadoWorkspace.QE[ 148 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 170 ]), &(acadoWorkspace.QE[ 172 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.QE[ 198 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.QE[ 226 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QE[ 256 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 288 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 322 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 358 ]) );
acado_setBlockH11( 7, 8, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 396 ]) );

acado_zeroBlockH11( 7, 9 );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 104 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 124 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 146 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 170 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_setBlockH11( 7, 9, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 398 ]) );

acado_zeroBlockH11( 7, 10 );
acado_setBlockH11( 7, 10, &(acadoWorkspace.E[ 124 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 7, 10, &(acadoWorkspace.E[ 146 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 7, 10, &(acadoWorkspace.E[ 170 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 7, 10, &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 7, 10, &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 7, 10, &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 7, 10, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 7, 10, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 7, 10, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 7, 10, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 7, 11 );
acado_setBlockH11( 7, 11, &(acadoWorkspace.E[ 146 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 7, 11, &(acadoWorkspace.E[ 170 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 7, 11, &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 7, 11, &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 7, 11, &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 7, 11, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 7, 11, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 7, 11, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 7, 11, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 7, 12 );
acado_setBlockH11( 7, 12, &(acadoWorkspace.E[ 170 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 7, 12, &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 7, 12, &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 7, 12, &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 7, 12, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 7, 12, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 7, 12, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 7, 12, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 7, 13 );
acado_setBlockH11( 7, 13, &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 7, 13, &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 7, 13, &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 7, 13, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 7, 13, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 7, 13, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 7, 13, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 7, 14 );
acado_setBlockH11( 7, 14, &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 7, 14, &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 7, 14, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 7, 14, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 7, 14, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 7, 14, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 7, 15 );
acado_setBlockH11( 7, 15, &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 7, 15, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 7, 15, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 7, 15, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 7, 15, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 7, 16 );
acado_setBlockH11( 7, 16, &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 7, 16, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 7, 16, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 7, 16, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 7, 17 );
acado_setBlockH11( 7, 17, &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 7, 17, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 7, 17, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 7, 18 );
acado_setBlockH11( 7, 18, &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 7, 18, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 7, 19 );
acado_setBlockH11( 7, 19, &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 8, 8, &(acadoWorkspace.R1[ 8 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 88 ]), &(acadoWorkspace.QE[ 88 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 106 ]), &(acadoWorkspace.QE[ 106 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 126 ]), &(acadoWorkspace.QE[ 126 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 148 ]), &(acadoWorkspace.QE[ 148 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 172 ]), &(acadoWorkspace.QE[ 172 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 198 ]), &(acadoWorkspace.QE[ 198 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.QE[ 226 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.QE[ 256 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QE[ 288 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 322 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 358 ]) );
acado_setBlockH11( 8, 8, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 396 ]) );

acado_zeroBlockH11( 8, 9 );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 106 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 126 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 148 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 172 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 198 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_setBlockH11( 8, 9, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 398 ]) );

acado_zeroBlockH11( 8, 10 );
acado_setBlockH11( 8, 10, &(acadoWorkspace.E[ 126 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 8, 10, &(acadoWorkspace.E[ 148 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 8, 10, &(acadoWorkspace.E[ 172 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 8, 10, &(acadoWorkspace.E[ 198 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 8, 10, &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 8, 10, &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 8, 10, &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 8, 10, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 8, 10, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 8, 10, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 8, 11 );
acado_setBlockH11( 8, 11, &(acadoWorkspace.E[ 148 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 8, 11, &(acadoWorkspace.E[ 172 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 8, 11, &(acadoWorkspace.E[ 198 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 8, 11, &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 8, 11, &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 8, 11, &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 8, 11, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 8, 11, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 8, 11, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 8, 12 );
acado_setBlockH11( 8, 12, &(acadoWorkspace.E[ 172 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 8, 12, &(acadoWorkspace.E[ 198 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 8, 12, &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 8, 12, &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 8, 12, &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 8, 12, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 8, 12, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 8, 12, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 8, 13 );
acado_setBlockH11( 8, 13, &(acadoWorkspace.E[ 198 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 8, 13, &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 8, 13, &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 8, 13, &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 8, 13, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 8, 13, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 8, 13, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 8, 14 );
acado_setBlockH11( 8, 14, &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 8, 14, &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 8, 14, &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 8, 14, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 8, 14, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 8, 14, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 8, 15 );
acado_setBlockH11( 8, 15, &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 8, 15, &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 8, 15, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 8, 15, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 8, 15, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 8, 16 );
acado_setBlockH11( 8, 16, &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 8, 16, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 8, 16, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 8, 16, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 8, 17 );
acado_setBlockH11( 8, 17, &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 8, 17, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 8, 17, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 8, 18 );
acado_setBlockH11( 8, 18, &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 8, 18, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 8, 19 );
acado_setBlockH11( 8, 19, &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 9, 9, &(acadoWorkspace.R1[ 9 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 108 ]), &(acadoWorkspace.QE[ 108 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 128 ]), &(acadoWorkspace.QE[ 128 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 150 ]), &(acadoWorkspace.QE[ 150 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 174 ]), &(acadoWorkspace.QE[ 174 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 200 ]), &(acadoWorkspace.QE[ 200 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 228 ]), &(acadoWorkspace.QE[ 228 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.QE[ 258 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.QE[ 290 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QE[ 324 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 360 ]) );
acado_setBlockH11( 9, 9, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 398 ]) );

acado_zeroBlockH11( 9, 10 );
acado_setBlockH11( 9, 10, &(acadoWorkspace.E[ 128 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 9, 10, &(acadoWorkspace.E[ 150 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 9, 10, &(acadoWorkspace.E[ 174 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 9, 10, &(acadoWorkspace.E[ 200 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 9, 10, &(acadoWorkspace.E[ 228 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 9, 10, &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 9, 10, &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 9, 10, &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 9, 10, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 9, 10, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 9, 11 );
acado_setBlockH11( 9, 11, &(acadoWorkspace.E[ 150 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 9, 11, &(acadoWorkspace.E[ 174 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 9, 11, &(acadoWorkspace.E[ 200 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 9, 11, &(acadoWorkspace.E[ 228 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 9, 11, &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 9, 11, &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 9, 11, &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 9, 11, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 9, 11, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 9, 12 );
acado_setBlockH11( 9, 12, &(acadoWorkspace.E[ 174 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 9, 12, &(acadoWorkspace.E[ 200 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 9, 12, &(acadoWorkspace.E[ 228 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 9, 12, &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 9, 12, &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 9, 12, &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 9, 12, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 9, 12, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 9, 13 );
acado_setBlockH11( 9, 13, &(acadoWorkspace.E[ 200 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 9, 13, &(acadoWorkspace.E[ 228 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 9, 13, &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 9, 13, &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 9, 13, &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 9, 13, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 9, 13, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 9, 14 );
acado_setBlockH11( 9, 14, &(acadoWorkspace.E[ 228 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 9, 14, &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 9, 14, &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 9, 14, &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 9, 14, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 9, 14, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 9, 15 );
acado_setBlockH11( 9, 15, &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 9, 15, &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 9, 15, &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 9, 15, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 9, 15, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 9, 16 );
acado_setBlockH11( 9, 16, &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 9, 16, &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 9, 16, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 9, 16, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 9, 17 );
acado_setBlockH11( 9, 17, &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 9, 17, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 9, 17, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 9, 18 );
acado_setBlockH11( 9, 18, &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 9, 18, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 9, 19 );
acado_setBlockH11( 9, 19, &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 10, 10, &(acadoWorkspace.R1[ 10 ]) );
acado_setBlockH11( 10, 10, &(acadoWorkspace.E[ 130 ]), &(acadoWorkspace.QE[ 130 ]) );
acado_setBlockH11( 10, 10, &(acadoWorkspace.E[ 152 ]), &(acadoWorkspace.QE[ 152 ]) );
acado_setBlockH11( 10, 10, &(acadoWorkspace.E[ 176 ]), &(acadoWorkspace.QE[ 176 ]) );
acado_setBlockH11( 10, 10, &(acadoWorkspace.E[ 202 ]), &(acadoWorkspace.QE[ 202 ]) );
acado_setBlockH11( 10, 10, &(acadoWorkspace.E[ 230 ]), &(acadoWorkspace.QE[ 230 ]) );
acado_setBlockH11( 10, 10, &(acadoWorkspace.E[ 260 ]), &(acadoWorkspace.QE[ 260 ]) );
acado_setBlockH11( 10, 10, &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.QE[ 292 ]) );
acado_setBlockH11( 10, 10, &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.QE[ 326 ]) );
acado_setBlockH11( 10, 10, &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QE[ 362 ]) );
acado_setBlockH11( 10, 10, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 400 ]) );

acado_zeroBlockH11( 10, 11 );
acado_setBlockH11( 10, 11, &(acadoWorkspace.E[ 152 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 10, 11, &(acadoWorkspace.E[ 176 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 10, 11, &(acadoWorkspace.E[ 202 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 10, 11, &(acadoWorkspace.E[ 230 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 10, 11, &(acadoWorkspace.E[ 260 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 10, 11, &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 10, 11, &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 10, 11, &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 10, 11, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 10, 12 );
acado_setBlockH11( 10, 12, &(acadoWorkspace.E[ 176 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 10, 12, &(acadoWorkspace.E[ 202 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 10, 12, &(acadoWorkspace.E[ 230 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 10, 12, &(acadoWorkspace.E[ 260 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 10, 12, &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 10, 12, &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 10, 12, &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 10, 12, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 10, 13 );
acado_setBlockH11( 10, 13, &(acadoWorkspace.E[ 202 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 10, 13, &(acadoWorkspace.E[ 230 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 10, 13, &(acadoWorkspace.E[ 260 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 10, 13, &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 10, 13, &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 10, 13, &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 10, 13, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 10, 14 );
acado_setBlockH11( 10, 14, &(acadoWorkspace.E[ 230 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 10, 14, &(acadoWorkspace.E[ 260 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 10, 14, &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 10, 14, &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 10, 14, &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 10, 14, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 10, 15 );
acado_setBlockH11( 10, 15, &(acadoWorkspace.E[ 260 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 10, 15, &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 10, 15, &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 10, 15, &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 10, 15, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 10, 16 );
acado_setBlockH11( 10, 16, &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 10, 16, &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 10, 16, &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 10, 16, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 10, 17 );
acado_setBlockH11( 10, 17, &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 10, 17, &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 10, 17, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 10, 18 );
acado_setBlockH11( 10, 18, &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 10, 18, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 10, 19 );
acado_setBlockH11( 10, 19, &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 11, 11, &(acadoWorkspace.R1[ 11 ]) );
acado_setBlockH11( 11, 11, &(acadoWorkspace.E[ 154 ]), &(acadoWorkspace.QE[ 154 ]) );
acado_setBlockH11( 11, 11, &(acadoWorkspace.E[ 178 ]), &(acadoWorkspace.QE[ 178 ]) );
acado_setBlockH11( 11, 11, &(acadoWorkspace.E[ 204 ]), &(acadoWorkspace.QE[ 204 ]) );
acado_setBlockH11( 11, 11, &(acadoWorkspace.E[ 232 ]), &(acadoWorkspace.QE[ 232 ]) );
acado_setBlockH11( 11, 11, &(acadoWorkspace.E[ 262 ]), &(acadoWorkspace.QE[ 262 ]) );
acado_setBlockH11( 11, 11, &(acadoWorkspace.E[ 294 ]), &(acadoWorkspace.QE[ 294 ]) );
acado_setBlockH11( 11, 11, &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.QE[ 328 ]) );
acado_setBlockH11( 11, 11, &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.QE[ 364 ]) );
acado_setBlockH11( 11, 11, &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QE[ 402 ]) );

acado_zeroBlockH11( 11, 12 );
acado_setBlockH11( 11, 12, &(acadoWorkspace.E[ 178 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 11, 12, &(acadoWorkspace.E[ 204 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 11, 12, &(acadoWorkspace.E[ 232 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 11, 12, &(acadoWorkspace.E[ 262 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 11, 12, &(acadoWorkspace.E[ 294 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 11, 12, &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 11, 12, &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 11, 12, &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 11, 13 );
acado_setBlockH11( 11, 13, &(acadoWorkspace.E[ 204 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 11, 13, &(acadoWorkspace.E[ 232 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 11, 13, &(acadoWorkspace.E[ 262 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 11, 13, &(acadoWorkspace.E[ 294 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 11, 13, &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 11, 13, &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 11, 13, &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 11, 14 );
acado_setBlockH11( 11, 14, &(acadoWorkspace.E[ 232 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 11, 14, &(acadoWorkspace.E[ 262 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 11, 14, &(acadoWorkspace.E[ 294 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 11, 14, &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 11, 14, &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 11, 14, &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 11, 15 );
acado_setBlockH11( 11, 15, &(acadoWorkspace.E[ 262 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 11, 15, &(acadoWorkspace.E[ 294 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 11, 15, &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 11, 15, &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 11, 15, &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 11, 16 );
acado_setBlockH11( 11, 16, &(acadoWorkspace.E[ 294 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 11, 16, &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 11, 16, &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 11, 16, &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 11, 17 );
acado_setBlockH11( 11, 17, &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 11, 17, &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 11, 17, &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 11, 18 );
acado_setBlockH11( 11, 18, &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 11, 18, &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 11, 19 );
acado_setBlockH11( 11, 19, &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 12, 12, &(acadoWorkspace.R1[ 12 ]) );
acado_setBlockH11( 12, 12, &(acadoWorkspace.E[ 180 ]), &(acadoWorkspace.QE[ 180 ]) );
acado_setBlockH11( 12, 12, &(acadoWorkspace.E[ 206 ]), &(acadoWorkspace.QE[ 206 ]) );
acado_setBlockH11( 12, 12, &(acadoWorkspace.E[ 234 ]), &(acadoWorkspace.QE[ 234 ]) );
acado_setBlockH11( 12, 12, &(acadoWorkspace.E[ 264 ]), &(acadoWorkspace.QE[ 264 ]) );
acado_setBlockH11( 12, 12, &(acadoWorkspace.E[ 296 ]), &(acadoWorkspace.QE[ 296 ]) );
acado_setBlockH11( 12, 12, &(acadoWorkspace.E[ 330 ]), &(acadoWorkspace.QE[ 330 ]) );
acado_setBlockH11( 12, 12, &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.QE[ 366 ]) );
acado_setBlockH11( 12, 12, &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.QE[ 404 ]) );

acado_zeroBlockH11( 12, 13 );
acado_setBlockH11( 12, 13, &(acadoWorkspace.E[ 206 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 12, 13, &(acadoWorkspace.E[ 234 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 12, 13, &(acadoWorkspace.E[ 264 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 12, 13, &(acadoWorkspace.E[ 296 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 12, 13, &(acadoWorkspace.E[ 330 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 12, 13, &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 12, 13, &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 12, 14 );
acado_setBlockH11( 12, 14, &(acadoWorkspace.E[ 234 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 12, 14, &(acadoWorkspace.E[ 264 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 12, 14, &(acadoWorkspace.E[ 296 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 12, 14, &(acadoWorkspace.E[ 330 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 12, 14, &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 12, 14, &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 12, 15 );
acado_setBlockH11( 12, 15, &(acadoWorkspace.E[ 264 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 12, 15, &(acadoWorkspace.E[ 296 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 12, 15, &(acadoWorkspace.E[ 330 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 12, 15, &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 12, 15, &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 12, 16 );
acado_setBlockH11( 12, 16, &(acadoWorkspace.E[ 296 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 12, 16, &(acadoWorkspace.E[ 330 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 12, 16, &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 12, 16, &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 12, 17 );
acado_setBlockH11( 12, 17, &(acadoWorkspace.E[ 330 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 12, 17, &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 12, 17, &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 12, 18 );
acado_setBlockH11( 12, 18, &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 12, 18, &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 12, 19 );
acado_setBlockH11( 12, 19, &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 13, 13, &(acadoWorkspace.R1[ 13 ]) );
acado_setBlockH11( 13, 13, &(acadoWorkspace.E[ 208 ]), &(acadoWorkspace.QE[ 208 ]) );
acado_setBlockH11( 13, 13, &(acadoWorkspace.E[ 236 ]), &(acadoWorkspace.QE[ 236 ]) );
acado_setBlockH11( 13, 13, &(acadoWorkspace.E[ 266 ]), &(acadoWorkspace.QE[ 266 ]) );
acado_setBlockH11( 13, 13, &(acadoWorkspace.E[ 298 ]), &(acadoWorkspace.QE[ 298 ]) );
acado_setBlockH11( 13, 13, &(acadoWorkspace.E[ 332 ]), &(acadoWorkspace.QE[ 332 ]) );
acado_setBlockH11( 13, 13, &(acadoWorkspace.E[ 368 ]), &(acadoWorkspace.QE[ 368 ]) );
acado_setBlockH11( 13, 13, &(acadoWorkspace.E[ 406 ]), &(acadoWorkspace.QE[ 406 ]) );

acado_zeroBlockH11( 13, 14 );
acado_setBlockH11( 13, 14, &(acadoWorkspace.E[ 236 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 13, 14, &(acadoWorkspace.E[ 266 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 13, 14, &(acadoWorkspace.E[ 298 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 13, 14, &(acadoWorkspace.E[ 332 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 13, 14, &(acadoWorkspace.E[ 368 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 13, 14, &(acadoWorkspace.E[ 406 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 13, 15 );
acado_setBlockH11( 13, 15, &(acadoWorkspace.E[ 266 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 13, 15, &(acadoWorkspace.E[ 298 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 13, 15, &(acadoWorkspace.E[ 332 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 13, 15, &(acadoWorkspace.E[ 368 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 13, 15, &(acadoWorkspace.E[ 406 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 13, 16 );
acado_setBlockH11( 13, 16, &(acadoWorkspace.E[ 298 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 13, 16, &(acadoWorkspace.E[ 332 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 13, 16, &(acadoWorkspace.E[ 368 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 13, 16, &(acadoWorkspace.E[ 406 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 13, 17 );
acado_setBlockH11( 13, 17, &(acadoWorkspace.E[ 332 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 13, 17, &(acadoWorkspace.E[ 368 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 13, 17, &(acadoWorkspace.E[ 406 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 13, 18 );
acado_setBlockH11( 13, 18, &(acadoWorkspace.E[ 368 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 13, 18, &(acadoWorkspace.E[ 406 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 13, 19 );
acado_setBlockH11( 13, 19, &(acadoWorkspace.E[ 406 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 14, 14, &(acadoWorkspace.R1[ 14 ]) );
acado_setBlockH11( 14, 14, &(acadoWorkspace.E[ 238 ]), &(acadoWorkspace.QE[ 238 ]) );
acado_setBlockH11( 14, 14, &(acadoWorkspace.E[ 268 ]), &(acadoWorkspace.QE[ 268 ]) );
acado_setBlockH11( 14, 14, &(acadoWorkspace.E[ 300 ]), &(acadoWorkspace.QE[ 300 ]) );
acado_setBlockH11( 14, 14, &(acadoWorkspace.E[ 334 ]), &(acadoWorkspace.QE[ 334 ]) );
acado_setBlockH11( 14, 14, &(acadoWorkspace.E[ 370 ]), &(acadoWorkspace.QE[ 370 ]) );
acado_setBlockH11( 14, 14, &(acadoWorkspace.E[ 408 ]), &(acadoWorkspace.QE[ 408 ]) );

acado_zeroBlockH11( 14, 15 );
acado_setBlockH11( 14, 15, &(acadoWorkspace.E[ 268 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 14, 15, &(acadoWorkspace.E[ 300 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 14, 15, &(acadoWorkspace.E[ 334 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 14, 15, &(acadoWorkspace.E[ 370 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 14, 15, &(acadoWorkspace.E[ 408 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 14, 16 );
acado_setBlockH11( 14, 16, &(acadoWorkspace.E[ 300 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 14, 16, &(acadoWorkspace.E[ 334 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 14, 16, &(acadoWorkspace.E[ 370 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 14, 16, &(acadoWorkspace.E[ 408 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 14, 17 );
acado_setBlockH11( 14, 17, &(acadoWorkspace.E[ 334 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 14, 17, &(acadoWorkspace.E[ 370 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 14, 17, &(acadoWorkspace.E[ 408 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 14, 18 );
acado_setBlockH11( 14, 18, &(acadoWorkspace.E[ 370 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 14, 18, &(acadoWorkspace.E[ 408 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 14, 19 );
acado_setBlockH11( 14, 19, &(acadoWorkspace.E[ 408 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 15, 15, &(acadoWorkspace.R1[ 15 ]) );
acado_setBlockH11( 15, 15, &(acadoWorkspace.E[ 270 ]), &(acadoWorkspace.QE[ 270 ]) );
acado_setBlockH11( 15, 15, &(acadoWorkspace.E[ 302 ]), &(acadoWorkspace.QE[ 302 ]) );
acado_setBlockH11( 15, 15, &(acadoWorkspace.E[ 336 ]), &(acadoWorkspace.QE[ 336 ]) );
acado_setBlockH11( 15, 15, &(acadoWorkspace.E[ 372 ]), &(acadoWorkspace.QE[ 372 ]) );
acado_setBlockH11( 15, 15, &(acadoWorkspace.E[ 410 ]), &(acadoWorkspace.QE[ 410 ]) );

acado_zeroBlockH11( 15, 16 );
acado_setBlockH11( 15, 16, &(acadoWorkspace.E[ 302 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 15, 16, &(acadoWorkspace.E[ 336 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 15, 16, &(acadoWorkspace.E[ 372 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 15, 16, &(acadoWorkspace.E[ 410 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 15, 17 );
acado_setBlockH11( 15, 17, &(acadoWorkspace.E[ 336 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 15, 17, &(acadoWorkspace.E[ 372 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 15, 17, &(acadoWorkspace.E[ 410 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 15, 18 );
acado_setBlockH11( 15, 18, &(acadoWorkspace.E[ 372 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 15, 18, &(acadoWorkspace.E[ 410 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 15, 19 );
acado_setBlockH11( 15, 19, &(acadoWorkspace.E[ 410 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 16, 16, &(acadoWorkspace.R1[ 16 ]) );
acado_setBlockH11( 16, 16, &(acadoWorkspace.E[ 304 ]), &(acadoWorkspace.QE[ 304 ]) );
acado_setBlockH11( 16, 16, &(acadoWorkspace.E[ 338 ]), &(acadoWorkspace.QE[ 338 ]) );
acado_setBlockH11( 16, 16, &(acadoWorkspace.E[ 374 ]), &(acadoWorkspace.QE[ 374 ]) );
acado_setBlockH11( 16, 16, &(acadoWorkspace.E[ 412 ]), &(acadoWorkspace.QE[ 412 ]) );

acado_zeroBlockH11( 16, 17 );
acado_setBlockH11( 16, 17, &(acadoWorkspace.E[ 338 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 16, 17, &(acadoWorkspace.E[ 374 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 16, 17, &(acadoWorkspace.E[ 412 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 16, 18 );
acado_setBlockH11( 16, 18, &(acadoWorkspace.E[ 374 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 16, 18, &(acadoWorkspace.E[ 412 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 16, 19 );
acado_setBlockH11( 16, 19, &(acadoWorkspace.E[ 412 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 17, 17, &(acadoWorkspace.R1[ 17 ]) );
acado_setBlockH11( 17, 17, &(acadoWorkspace.E[ 340 ]), &(acadoWorkspace.QE[ 340 ]) );
acado_setBlockH11( 17, 17, &(acadoWorkspace.E[ 376 ]), &(acadoWorkspace.QE[ 376 ]) );
acado_setBlockH11( 17, 17, &(acadoWorkspace.E[ 414 ]), &(acadoWorkspace.QE[ 414 ]) );

acado_zeroBlockH11( 17, 18 );
acado_setBlockH11( 17, 18, &(acadoWorkspace.E[ 376 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 17, 18, &(acadoWorkspace.E[ 414 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 17, 19 );
acado_setBlockH11( 17, 19, &(acadoWorkspace.E[ 414 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 18, 18, &(acadoWorkspace.R1[ 18 ]) );
acado_setBlockH11( 18, 18, &(acadoWorkspace.E[ 378 ]), &(acadoWorkspace.QE[ 378 ]) );
acado_setBlockH11( 18, 18, &(acadoWorkspace.E[ 416 ]), &(acadoWorkspace.QE[ 416 ]) );

acado_zeroBlockH11( 18, 19 );
acado_setBlockH11( 18, 19, &(acadoWorkspace.E[ 416 ]), &(acadoWorkspace.QE[ 418 ]) );

acado_setBlockH11_R1( 19, 19, &(acadoWorkspace.R1[ 19 ]) );
acado_setBlockH11( 19, 19, &(acadoWorkspace.E[ 418 ]), &(acadoWorkspace.QE[ 418 ]) );


acado_copyHTH( 1, 0 );
acado_copyHTH( 2, 0 );
acado_copyHTH( 2, 1 );
acado_copyHTH( 3, 0 );
acado_copyHTH( 3, 1 );
acado_copyHTH( 3, 2 );
acado_copyHTH( 4, 0 );
acado_copyHTH( 4, 1 );
acado_copyHTH( 4, 2 );
acado_copyHTH( 4, 3 );
acado_copyHTH( 5, 0 );
acado_copyHTH( 5, 1 );
acado_copyHTH( 5, 2 );
acado_copyHTH( 5, 3 );
acado_copyHTH( 5, 4 );
acado_copyHTH( 6, 0 );
acado_copyHTH( 6, 1 );
acado_copyHTH( 6, 2 );
acado_copyHTH( 6, 3 );
acado_copyHTH( 6, 4 );
acado_copyHTH( 6, 5 );
acado_copyHTH( 7, 0 );
acado_copyHTH( 7, 1 );
acado_copyHTH( 7, 2 );
acado_copyHTH( 7, 3 );
acado_copyHTH( 7, 4 );
acado_copyHTH( 7, 5 );
acado_copyHTH( 7, 6 );
acado_copyHTH( 8, 0 );
acado_copyHTH( 8, 1 );
acado_copyHTH( 8, 2 );
acado_copyHTH( 8, 3 );
acado_copyHTH( 8, 4 );
acado_copyHTH( 8, 5 );
acado_copyHTH( 8, 6 );
acado_copyHTH( 8, 7 );
acado_copyHTH( 9, 0 );
acado_copyHTH( 9, 1 );
acado_copyHTH( 9, 2 );
acado_copyHTH( 9, 3 );
acado_copyHTH( 9, 4 );
acado_copyHTH( 9, 5 );
acado_copyHTH( 9, 6 );
acado_copyHTH( 9, 7 );
acado_copyHTH( 9, 8 );
acado_copyHTH( 10, 0 );
acado_copyHTH( 10, 1 );
acado_copyHTH( 10, 2 );
acado_copyHTH( 10, 3 );
acado_copyHTH( 10, 4 );
acado_copyHTH( 10, 5 );
acado_copyHTH( 10, 6 );
acado_copyHTH( 10, 7 );
acado_copyHTH( 10, 8 );
acado_copyHTH( 10, 9 );
acado_copyHTH( 11, 0 );
acado_copyHTH( 11, 1 );
acado_copyHTH( 11, 2 );
acado_copyHTH( 11, 3 );
acado_copyHTH( 11, 4 );
acado_copyHTH( 11, 5 );
acado_copyHTH( 11, 6 );
acado_copyHTH( 11, 7 );
acado_copyHTH( 11, 8 );
acado_copyHTH( 11, 9 );
acado_copyHTH( 11, 10 );
acado_copyHTH( 12, 0 );
acado_copyHTH( 12, 1 );
acado_copyHTH( 12, 2 );
acado_copyHTH( 12, 3 );
acado_copyHTH( 12, 4 );
acado_copyHTH( 12, 5 );
acado_copyHTH( 12, 6 );
acado_copyHTH( 12, 7 );
acado_copyHTH( 12, 8 );
acado_copyHTH( 12, 9 );
acado_copyHTH( 12, 10 );
acado_copyHTH( 12, 11 );
acado_copyHTH( 13, 0 );
acado_copyHTH( 13, 1 );
acado_copyHTH( 13, 2 );
acado_copyHTH( 13, 3 );
acado_copyHTH( 13, 4 );
acado_copyHTH( 13, 5 );
acado_copyHTH( 13, 6 );
acado_copyHTH( 13, 7 );
acado_copyHTH( 13, 8 );
acado_copyHTH( 13, 9 );
acado_copyHTH( 13, 10 );
acado_copyHTH( 13, 11 );
acado_copyHTH( 13, 12 );
acado_copyHTH( 14, 0 );
acado_copyHTH( 14, 1 );
acado_copyHTH( 14, 2 );
acado_copyHTH( 14, 3 );
acado_copyHTH( 14, 4 );
acado_copyHTH( 14, 5 );
acado_copyHTH( 14, 6 );
acado_copyHTH( 14, 7 );
acado_copyHTH( 14, 8 );
acado_copyHTH( 14, 9 );
acado_copyHTH( 14, 10 );
acado_copyHTH( 14, 11 );
acado_copyHTH( 14, 12 );
acado_copyHTH( 14, 13 );
acado_copyHTH( 15, 0 );
acado_copyHTH( 15, 1 );
acado_copyHTH( 15, 2 );
acado_copyHTH( 15, 3 );
acado_copyHTH( 15, 4 );
acado_copyHTH( 15, 5 );
acado_copyHTH( 15, 6 );
acado_copyHTH( 15, 7 );
acado_copyHTH( 15, 8 );
acado_copyHTH( 15, 9 );
acado_copyHTH( 15, 10 );
acado_copyHTH( 15, 11 );
acado_copyHTH( 15, 12 );
acado_copyHTH( 15, 13 );
acado_copyHTH( 15, 14 );
acado_copyHTH( 16, 0 );
acado_copyHTH( 16, 1 );
acado_copyHTH( 16, 2 );
acado_copyHTH( 16, 3 );
acado_copyHTH( 16, 4 );
acado_copyHTH( 16, 5 );
acado_copyHTH( 16, 6 );
acado_copyHTH( 16, 7 );
acado_copyHTH( 16, 8 );
acado_copyHTH( 16, 9 );
acado_copyHTH( 16, 10 );
acado_copyHTH( 16, 11 );
acado_copyHTH( 16, 12 );
acado_copyHTH( 16, 13 );
acado_copyHTH( 16, 14 );
acado_copyHTH( 16, 15 );
acado_copyHTH( 17, 0 );
acado_copyHTH( 17, 1 );
acado_copyHTH( 17, 2 );
acado_copyHTH( 17, 3 );
acado_copyHTH( 17, 4 );
acado_copyHTH( 17, 5 );
acado_copyHTH( 17, 6 );
acado_copyHTH( 17, 7 );
acado_copyHTH( 17, 8 );
acado_copyHTH( 17, 9 );
acado_copyHTH( 17, 10 );
acado_copyHTH( 17, 11 );
acado_copyHTH( 17, 12 );
acado_copyHTH( 17, 13 );
acado_copyHTH( 17, 14 );
acado_copyHTH( 17, 15 );
acado_copyHTH( 17, 16 );
acado_copyHTH( 18, 0 );
acado_copyHTH( 18, 1 );
acado_copyHTH( 18, 2 );
acado_copyHTH( 18, 3 );
acado_copyHTH( 18, 4 );
acado_copyHTH( 18, 5 );
acado_copyHTH( 18, 6 );
acado_copyHTH( 18, 7 );
acado_copyHTH( 18, 8 );
acado_copyHTH( 18, 9 );
acado_copyHTH( 18, 10 );
acado_copyHTH( 18, 11 );
acado_copyHTH( 18, 12 );
acado_copyHTH( 18, 13 );
acado_copyHTH( 18, 14 );
acado_copyHTH( 18, 15 );
acado_copyHTH( 18, 16 );
acado_copyHTH( 18, 17 );
acado_copyHTH( 19, 0 );
acado_copyHTH( 19, 1 );
acado_copyHTH( 19, 2 );
acado_copyHTH( 19, 3 );
acado_copyHTH( 19, 4 );
acado_copyHTH( 19, 5 );
acado_copyHTH( 19, 6 );
acado_copyHTH( 19, 7 );
acado_copyHTH( 19, 8 );
acado_copyHTH( 19, 9 );
acado_copyHTH( 19, 10 );
acado_copyHTH( 19, 11 );
acado_copyHTH( 19, 12 );
acado_copyHTH( 19, 13 );
acado_copyHTH( 19, 14 );
acado_copyHTH( 19, 15 );
acado_copyHTH( 19, 16 );
acado_copyHTH( 19, 17 );
acado_copyHTH( 19, 18 );

acado_multQ1d( &(acadoWorkspace.Q1[ 4 ]), acadoWorkspace.d, acadoWorkspace.Qd );
acado_multQ1d( &(acadoWorkspace.Q1[ 8 ]), &(acadoWorkspace.d[ 2 ]), &(acadoWorkspace.Qd[ 2 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 12 ]), &(acadoWorkspace.d[ 4 ]), &(acadoWorkspace.Qd[ 4 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 16 ]), &(acadoWorkspace.d[ 6 ]), &(acadoWorkspace.Qd[ 6 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 20 ]), &(acadoWorkspace.d[ 8 ]), &(acadoWorkspace.Qd[ 8 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 24 ]), &(acadoWorkspace.d[ 10 ]), &(acadoWorkspace.Qd[ 10 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 28 ]), &(acadoWorkspace.d[ 12 ]), &(acadoWorkspace.Qd[ 12 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 32 ]), &(acadoWorkspace.d[ 14 ]), &(acadoWorkspace.Qd[ 14 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 36 ]), &(acadoWorkspace.d[ 16 ]), &(acadoWorkspace.Qd[ 16 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 40 ]), &(acadoWorkspace.d[ 18 ]), &(acadoWorkspace.Qd[ 18 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 44 ]), &(acadoWorkspace.d[ 20 ]), &(acadoWorkspace.Qd[ 20 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 48 ]), &(acadoWorkspace.d[ 22 ]), &(acadoWorkspace.Qd[ 22 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 52 ]), &(acadoWorkspace.d[ 24 ]), &(acadoWorkspace.Qd[ 24 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 56 ]), &(acadoWorkspace.d[ 26 ]), &(acadoWorkspace.Qd[ 26 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 60 ]), &(acadoWorkspace.d[ 28 ]), &(acadoWorkspace.Qd[ 28 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 64 ]), &(acadoWorkspace.d[ 30 ]), &(acadoWorkspace.Qd[ 30 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 68 ]), &(acadoWorkspace.d[ 32 ]), &(acadoWorkspace.Qd[ 32 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 72 ]), &(acadoWorkspace.d[ 34 ]), &(acadoWorkspace.Qd[ 34 ]) );
acado_multQ1d( &(acadoWorkspace.Q1[ 76 ]), &(acadoWorkspace.d[ 36 ]), &(acadoWorkspace.Qd[ 36 ]) );
acado_multQN1d( acadoWorkspace.QN1, &(acadoWorkspace.d[ 38 ]), &(acadoWorkspace.Qd[ 38 ]) );

acado_macETSlu( acadoWorkspace.QE, acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 2 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 6 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 12 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 20 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 30 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 42 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 56 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 72 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 90 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 110 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 132 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 156 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 182 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 210 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 240 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 272 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 306 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 342 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 380 ]), acadoWorkspace.g );
acado_macETSlu( &(acadoWorkspace.QE[ 4 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 8 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 14 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 22 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 32 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 44 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 58 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 74 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 92 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 112 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 134 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 158 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 184 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 212 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 242 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 274 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 308 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 344 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 382 ]), &(acadoWorkspace.g[ 1 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 10 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 16 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 24 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 34 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 46 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 60 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 76 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 94 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 114 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 136 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 160 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 186 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 214 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 244 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 276 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 310 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 346 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 384 ]), &(acadoWorkspace.g[ 2 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 18 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 26 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 36 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 48 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 62 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 78 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 96 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 116 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 138 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 162 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 188 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 216 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 246 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 278 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 312 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 348 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 386 ]), &(acadoWorkspace.g[ 3 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 28 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 38 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 50 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 64 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 80 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 98 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 118 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 140 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 164 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 190 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 218 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 248 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 280 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 314 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 350 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 388 ]), &(acadoWorkspace.g[ 4 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 40 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 52 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 66 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 82 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 100 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 120 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 142 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 166 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 192 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 220 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 250 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 282 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 316 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 352 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 390 ]), &(acadoWorkspace.g[ 5 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 54 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 68 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 84 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 102 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 122 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 144 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 168 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 194 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 222 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 252 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 284 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 318 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 354 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 392 ]), &(acadoWorkspace.g[ 6 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 70 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 86 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 104 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 124 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 146 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 170 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 196 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 224 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 254 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 286 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 320 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 356 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 394 ]), &(acadoWorkspace.g[ 7 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 88 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 106 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 126 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 148 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 172 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 198 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 226 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 256 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 288 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 322 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 358 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 396 ]), &(acadoWorkspace.g[ 8 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 108 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 128 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 150 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 174 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 200 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 228 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 258 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 290 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 324 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 360 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 398 ]), &(acadoWorkspace.g[ 9 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 130 ]), &(acadoWorkspace.g[ 10 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 152 ]), &(acadoWorkspace.g[ 10 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 176 ]), &(acadoWorkspace.g[ 10 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 202 ]), &(acadoWorkspace.g[ 10 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 230 ]), &(acadoWorkspace.g[ 10 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 260 ]), &(acadoWorkspace.g[ 10 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 292 ]), &(acadoWorkspace.g[ 10 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 326 ]), &(acadoWorkspace.g[ 10 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 362 ]), &(acadoWorkspace.g[ 10 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 400 ]), &(acadoWorkspace.g[ 10 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 154 ]), &(acadoWorkspace.g[ 11 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 178 ]), &(acadoWorkspace.g[ 11 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 204 ]), &(acadoWorkspace.g[ 11 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 232 ]), &(acadoWorkspace.g[ 11 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 262 ]), &(acadoWorkspace.g[ 11 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 294 ]), &(acadoWorkspace.g[ 11 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 328 ]), &(acadoWorkspace.g[ 11 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 364 ]), &(acadoWorkspace.g[ 11 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 402 ]), &(acadoWorkspace.g[ 11 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 180 ]), &(acadoWorkspace.g[ 12 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 206 ]), &(acadoWorkspace.g[ 12 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 234 ]), &(acadoWorkspace.g[ 12 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 264 ]), &(acadoWorkspace.g[ 12 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 296 ]), &(acadoWorkspace.g[ 12 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 330 ]), &(acadoWorkspace.g[ 12 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 366 ]), &(acadoWorkspace.g[ 12 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 404 ]), &(acadoWorkspace.g[ 12 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 208 ]), &(acadoWorkspace.g[ 13 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 236 ]), &(acadoWorkspace.g[ 13 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 266 ]), &(acadoWorkspace.g[ 13 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 298 ]), &(acadoWorkspace.g[ 13 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 332 ]), &(acadoWorkspace.g[ 13 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 368 ]), &(acadoWorkspace.g[ 13 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 406 ]), &(acadoWorkspace.g[ 13 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 238 ]), &(acadoWorkspace.g[ 14 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 268 ]), &(acadoWorkspace.g[ 14 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 300 ]), &(acadoWorkspace.g[ 14 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 334 ]), &(acadoWorkspace.g[ 14 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 370 ]), &(acadoWorkspace.g[ 14 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 408 ]), &(acadoWorkspace.g[ 14 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 270 ]), &(acadoWorkspace.g[ 15 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 302 ]), &(acadoWorkspace.g[ 15 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 336 ]), &(acadoWorkspace.g[ 15 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 372 ]), &(acadoWorkspace.g[ 15 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 410 ]), &(acadoWorkspace.g[ 15 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 304 ]), &(acadoWorkspace.g[ 16 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 338 ]), &(acadoWorkspace.g[ 16 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 374 ]), &(acadoWorkspace.g[ 16 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 412 ]), &(acadoWorkspace.g[ 16 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 340 ]), &(acadoWorkspace.g[ 17 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 376 ]), &(acadoWorkspace.g[ 17 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 414 ]), &(acadoWorkspace.g[ 17 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 378 ]), &(acadoWorkspace.g[ 18 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 416 ]), &(acadoWorkspace.g[ 18 ]) );
acado_macETSlu( &(acadoWorkspace.QE[ 418 ]), &(acadoWorkspace.g[ 19 ]) );
for (lRun1 = 0; lRun1 < 40; ++lRun1)
{
lRun3 = xBoundIndices[ lRun1 ] - 2;
lRun4 = ((lRun3) / (2)) + (1);
for (lRun2 = 0; lRun2 < lRun4; ++lRun2)
{
lRun5 = (((((lRun4) * (lRun4-1)) / (2)) + (lRun2)) * (2)) + ((lRun3) % (2));
acadoWorkspace.A[(lRun1 * 20) + (lRun2)] = acadoWorkspace.E[lRun5];
}
}

}

void acado_condenseFdb(  )
{
real_t tmp;

acadoWorkspace.Dx0[0] = acadoVariables.x0[0] - acadoVariables.x[0];
acadoWorkspace.Dx0[1] = acadoVariables.x0[1] - acadoVariables.x[1];

acadoWorkspace.Dy[0] -= acadoVariables.y[0];
acadoWorkspace.Dy[1] -= acadoVariables.y[1];
acadoWorkspace.Dy[2] -= acadoVariables.y[2];
acadoWorkspace.Dy[3] -= acadoVariables.y[3];
acadoWorkspace.Dy[4] -= acadoVariables.y[4];
acadoWorkspace.Dy[5] -= acadoVariables.y[5];
acadoWorkspace.Dy[6] -= acadoVariables.y[6];
acadoWorkspace.Dy[7] -= acadoVariables.y[7];
acadoWorkspace.Dy[8] -= acadoVariables.y[8];
acadoWorkspace.Dy[9] -= acadoVariables.y[9];
acadoWorkspace.Dy[10] -= acadoVariables.y[10];
acadoWorkspace.Dy[11] -= acadoVariables.y[11];
acadoWorkspace.Dy[12] -= acadoVariables.y[12];
acadoWorkspace.Dy[13] -= acadoVariables.y[13];
acadoWorkspace.Dy[14] -= acadoVariables.y[14];
acadoWorkspace.Dy[15] -= acadoVariables.y[15];
acadoWorkspace.Dy[16] -= acadoVariables.y[16];
acadoWorkspace.Dy[17] -= acadoVariables.y[17];
acadoWorkspace.Dy[18] -= acadoVariables.y[18];
acadoWorkspace.Dy[19] -= acadoVariables.y[19];
acadoWorkspace.Dy[20] -= acadoVariables.y[20];
acadoWorkspace.Dy[21] -= acadoVariables.y[21];
acadoWorkspace.Dy[22] -= acadoVariables.y[22];
acadoWorkspace.Dy[23] -= acadoVariables.y[23];
acadoWorkspace.Dy[24] -= acadoVariables.y[24];
acadoWorkspace.Dy[25] -= acadoVariables.y[25];
acadoWorkspace.Dy[26] -= acadoVariables.y[26];
acadoWorkspace.Dy[27] -= acadoVariables.y[27];
acadoWorkspace.Dy[28] -= acadoVariables.y[28];
acadoWorkspace.Dy[29] -= acadoVariables.y[29];
acadoWorkspace.Dy[30] -= acadoVariables.y[30];
acadoWorkspace.Dy[31] -= acadoVariables.y[31];
acadoWorkspace.Dy[32] -= acadoVariables.y[32];
acadoWorkspace.Dy[33] -= acadoVariables.y[33];
acadoWorkspace.Dy[34] -= acadoVariables.y[34];
acadoWorkspace.Dy[35] -= acadoVariables.y[35];
acadoWorkspace.Dy[36] -= acadoVariables.y[36];
acadoWorkspace.Dy[37] -= acadoVariables.y[37];
acadoWorkspace.Dy[38] -= acadoVariables.y[38];
acadoWorkspace.Dy[39] -= acadoVariables.y[39];
acadoWorkspace.Dy[40] -= acadoVariables.y[40];
acadoWorkspace.Dy[41] -= acadoVariables.y[41];
acadoWorkspace.Dy[42] -= acadoVariables.y[42];
acadoWorkspace.Dy[43] -= acadoVariables.y[43];
acadoWorkspace.Dy[44] -= acadoVariables.y[44];
acadoWorkspace.Dy[45] -= acadoVariables.y[45];
acadoWorkspace.Dy[46] -= acadoVariables.y[46];
acadoWorkspace.Dy[47] -= acadoVariables.y[47];
acadoWorkspace.Dy[48] -= acadoVariables.y[48];
acadoWorkspace.Dy[49] -= acadoVariables.y[49];
acadoWorkspace.Dy[50] -= acadoVariables.y[50];
acadoWorkspace.Dy[51] -= acadoVariables.y[51];
acadoWorkspace.Dy[52] -= acadoVariables.y[52];
acadoWorkspace.Dy[53] -= acadoVariables.y[53];
acadoWorkspace.Dy[54] -= acadoVariables.y[54];
acadoWorkspace.Dy[55] -= acadoVariables.y[55];
acadoWorkspace.Dy[56] -= acadoVariables.y[56];
acadoWorkspace.Dy[57] -= acadoVariables.y[57];
acadoWorkspace.Dy[58] -= acadoVariables.y[58];
acadoWorkspace.Dy[59] -= acadoVariables.y[59];
acadoWorkspace.DyN[0] -= acadoVariables.yN[0];
acadoWorkspace.DyN[1] -= acadoVariables.yN[1];

acado_multRDy( acadoWorkspace.R2, acadoWorkspace.Dy, acadoWorkspace.g );
acado_multRDy( &(acadoWorkspace.R2[ 3 ]), &(acadoWorkspace.Dy[ 3 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 6 ]), &(acadoWorkspace.Dy[ 6 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 9 ]), &(acadoWorkspace.Dy[ 9 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 12 ]), &(acadoWorkspace.Dy[ 12 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 15 ]), &(acadoWorkspace.Dy[ 15 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 18 ]), &(acadoWorkspace.Dy[ 18 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 21 ]), &(acadoWorkspace.Dy[ 21 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 24 ]), &(acadoWorkspace.Dy[ 24 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 27 ]), &(acadoWorkspace.Dy[ 27 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 30 ]), &(acadoWorkspace.Dy[ 30 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 33 ]), &(acadoWorkspace.Dy[ 33 ]), &(acadoWorkspace.g[ 11 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 36 ]), &(acadoWorkspace.Dy[ 36 ]), &(acadoWorkspace.g[ 12 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 39 ]), &(acadoWorkspace.Dy[ 39 ]), &(acadoWorkspace.g[ 13 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 42 ]), &(acadoWorkspace.Dy[ 42 ]), &(acadoWorkspace.g[ 14 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 45 ]), &(acadoWorkspace.Dy[ 45 ]), &(acadoWorkspace.g[ 15 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 48 ]), &(acadoWorkspace.Dy[ 48 ]), &(acadoWorkspace.g[ 16 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 51 ]), &(acadoWorkspace.Dy[ 51 ]), &(acadoWorkspace.g[ 17 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 54 ]), &(acadoWorkspace.Dy[ 54 ]), &(acadoWorkspace.g[ 18 ]) );
acado_multRDy( &(acadoWorkspace.R2[ 57 ]), &(acadoWorkspace.Dy[ 57 ]), &(acadoWorkspace.g[ 19 ]) );

acado_multQDy( acadoWorkspace.Q2, acadoWorkspace.Dy, acadoWorkspace.QDy );
acado_multQDy( &(acadoWorkspace.Q2[ 6 ]), &(acadoWorkspace.Dy[ 3 ]), &(acadoWorkspace.QDy[ 2 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 12 ]), &(acadoWorkspace.Dy[ 6 ]), &(acadoWorkspace.QDy[ 4 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 18 ]), &(acadoWorkspace.Dy[ 9 ]), &(acadoWorkspace.QDy[ 6 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 24 ]), &(acadoWorkspace.Dy[ 12 ]), &(acadoWorkspace.QDy[ 8 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 30 ]), &(acadoWorkspace.Dy[ 15 ]), &(acadoWorkspace.QDy[ 10 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 36 ]), &(acadoWorkspace.Dy[ 18 ]), &(acadoWorkspace.QDy[ 12 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 42 ]), &(acadoWorkspace.Dy[ 21 ]), &(acadoWorkspace.QDy[ 14 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 48 ]), &(acadoWorkspace.Dy[ 24 ]), &(acadoWorkspace.QDy[ 16 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 54 ]), &(acadoWorkspace.Dy[ 27 ]), &(acadoWorkspace.QDy[ 18 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 60 ]), &(acadoWorkspace.Dy[ 30 ]), &(acadoWorkspace.QDy[ 20 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 66 ]), &(acadoWorkspace.Dy[ 33 ]), &(acadoWorkspace.QDy[ 22 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 72 ]), &(acadoWorkspace.Dy[ 36 ]), &(acadoWorkspace.QDy[ 24 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 78 ]), &(acadoWorkspace.Dy[ 39 ]), &(acadoWorkspace.QDy[ 26 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 84 ]), &(acadoWorkspace.Dy[ 42 ]), &(acadoWorkspace.QDy[ 28 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 90 ]), &(acadoWorkspace.Dy[ 45 ]), &(acadoWorkspace.QDy[ 30 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 96 ]), &(acadoWorkspace.Dy[ 48 ]), &(acadoWorkspace.QDy[ 32 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 102 ]), &(acadoWorkspace.Dy[ 51 ]), &(acadoWorkspace.QDy[ 34 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 108 ]), &(acadoWorkspace.Dy[ 54 ]), &(acadoWorkspace.QDy[ 36 ]) );
acado_multQDy( &(acadoWorkspace.Q2[ 114 ]), &(acadoWorkspace.Dy[ 57 ]), &(acadoWorkspace.QDy[ 38 ]) );

acadoWorkspace.QDy[40] = + acadoWorkspace.QN2[0]*acadoWorkspace.DyN[0] + acadoWorkspace.QN2[1]*acadoWorkspace.DyN[1];
acadoWorkspace.QDy[41] = + acadoWorkspace.QN2[2]*acadoWorkspace.DyN[0] + acadoWorkspace.QN2[3]*acadoWorkspace.DyN[1];

acadoWorkspace.QDy[2] += acadoWorkspace.Qd[0];
acadoWorkspace.QDy[3] += acadoWorkspace.Qd[1];
acadoWorkspace.QDy[4] += acadoWorkspace.Qd[2];
acadoWorkspace.QDy[5] += acadoWorkspace.Qd[3];
acadoWorkspace.QDy[6] += acadoWorkspace.Qd[4];
acadoWorkspace.QDy[7] += acadoWorkspace.Qd[5];
acadoWorkspace.QDy[8] += acadoWorkspace.Qd[6];
acadoWorkspace.QDy[9] += acadoWorkspace.Qd[7];
acadoWorkspace.QDy[10] += acadoWorkspace.Qd[8];
acadoWorkspace.QDy[11] += acadoWorkspace.Qd[9];
acadoWorkspace.QDy[12] += acadoWorkspace.Qd[10];
acadoWorkspace.QDy[13] += acadoWorkspace.Qd[11];
acadoWorkspace.QDy[14] += acadoWorkspace.Qd[12];
acadoWorkspace.QDy[15] += acadoWorkspace.Qd[13];
acadoWorkspace.QDy[16] += acadoWorkspace.Qd[14];
acadoWorkspace.QDy[17] += acadoWorkspace.Qd[15];
acadoWorkspace.QDy[18] += acadoWorkspace.Qd[16];
acadoWorkspace.QDy[19] += acadoWorkspace.Qd[17];
acadoWorkspace.QDy[20] += acadoWorkspace.Qd[18];
acadoWorkspace.QDy[21] += acadoWorkspace.Qd[19];
acadoWorkspace.QDy[22] += acadoWorkspace.Qd[20];
acadoWorkspace.QDy[23] += acadoWorkspace.Qd[21];
acadoWorkspace.QDy[24] += acadoWorkspace.Qd[22];
acadoWorkspace.QDy[25] += acadoWorkspace.Qd[23];
acadoWorkspace.QDy[26] += acadoWorkspace.Qd[24];
acadoWorkspace.QDy[27] += acadoWorkspace.Qd[25];
acadoWorkspace.QDy[28] += acadoWorkspace.Qd[26];
acadoWorkspace.QDy[29] += acadoWorkspace.Qd[27];
acadoWorkspace.QDy[30] += acadoWorkspace.Qd[28];
acadoWorkspace.QDy[31] += acadoWorkspace.Qd[29];
acadoWorkspace.QDy[32] += acadoWorkspace.Qd[30];
acadoWorkspace.QDy[33] += acadoWorkspace.Qd[31];
acadoWorkspace.QDy[34] += acadoWorkspace.Qd[32];
acadoWorkspace.QDy[35] += acadoWorkspace.Qd[33];
acadoWorkspace.QDy[36] += acadoWorkspace.Qd[34];
acadoWorkspace.QDy[37] += acadoWorkspace.Qd[35];
acadoWorkspace.QDy[38] += acadoWorkspace.Qd[36];
acadoWorkspace.QDy[39] += acadoWorkspace.Qd[37];
acadoWorkspace.QDy[40] += acadoWorkspace.Qd[38];
acadoWorkspace.QDy[41] += acadoWorkspace.Qd[39];

acado_multEQDy( acadoWorkspace.E, &(acadoWorkspace.QDy[ 2 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 2 ]), &(acadoWorkspace.QDy[ 4 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 6 ]), &(acadoWorkspace.QDy[ 6 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 12 ]), &(acadoWorkspace.QDy[ 8 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 20 ]), &(acadoWorkspace.QDy[ 10 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 30 ]), &(acadoWorkspace.QDy[ 12 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 42 ]), &(acadoWorkspace.QDy[ 14 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 56 ]), &(acadoWorkspace.QDy[ 16 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 72 ]), &(acadoWorkspace.QDy[ 18 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 90 ]), &(acadoWorkspace.QDy[ 20 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 110 ]), &(acadoWorkspace.QDy[ 22 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 132 ]), &(acadoWorkspace.QDy[ 24 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 156 ]), &(acadoWorkspace.QDy[ 26 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 182 ]), &(acadoWorkspace.QDy[ 28 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 210 ]), &(acadoWorkspace.QDy[ 30 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 240 ]), &(acadoWorkspace.QDy[ 32 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 272 ]), &(acadoWorkspace.QDy[ 34 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 306 ]), &(acadoWorkspace.QDy[ 36 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 342 ]), &(acadoWorkspace.QDy[ 38 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 380 ]), &(acadoWorkspace.QDy[ 40 ]), acadoWorkspace.g );
acado_multEQDy( &(acadoWorkspace.E[ 4 ]), &(acadoWorkspace.QDy[ 4 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 8 ]), &(acadoWorkspace.QDy[ 6 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 14 ]), &(acadoWorkspace.QDy[ 8 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 22 ]), &(acadoWorkspace.QDy[ 10 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 32 ]), &(acadoWorkspace.QDy[ 12 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 44 ]), &(acadoWorkspace.QDy[ 14 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.QDy[ 16 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.QDy[ 18 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.QDy[ 20 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.QDy[ 22 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 1 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 10 ]), &(acadoWorkspace.QDy[ 6 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 16 ]), &(acadoWorkspace.QDy[ 8 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 24 ]), &(acadoWorkspace.QDy[ 10 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 34 ]), &(acadoWorkspace.QDy[ 12 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 46 ]), &(acadoWorkspace.QDy[ 14 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 60 ]), &(acadoWorkspace.QDy[ 16 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.QDy[ 18 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.QDy[ 20 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.QDy[ 22 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 2 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 18 ]), &(acadoWorkspace.QDy[ 8 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 26 ]), &(acadoWorkspace.QDy[ 10 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 36 ]), &(acadoWorkspace.QDy[ 12 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 48 ]), &(acadoWorkspace.QDy[ 14 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 62 ]), &(acadoWorkspace.QDy[ 16 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 78 ]), &(acadoWorkspace.QDy[ 18 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.QDy[ 20 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.QDy[ 22 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 3 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 28 ]), &(acadoWorkspace.QDy[ 10 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 38 ]), &(acadoWorkspace.QDy[ 12 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 50 ]), &(acadoWorkspace.QDy[ 14 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 64 ]), &(acadoWorkspace.QDy[ 16 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 80 ]), &(acadoWorkspace.QDy[ 18 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 98 ]), &(acadoWorkspace.QDy[ 20 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.QDy[ 22 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 4 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 40 ]), &(acadoWorkspace.QDy[ 12 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 52 ]), &(acadoWorkspace.QDy[ 14 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 66 ]), &(acadoWorkspace.QDy[ 16 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 82 ]), &(acadoWorkspace.QDy[ 18 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 100 ]), &(acadoWorkspace.QDy[ 20 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 120 ]), &(acadoWorkspace.QDy[ 22 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 5 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 54 ]), &(acadoWorkspace.QDy[ 14 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 68 ]), &(acadoWorkspace.QDy[ 16 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 84 ]), &(acadoWorkspace.QDy[ 18 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 102 ]), &(acadoWorkspace.QDy[ 20 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 122 ]), &(acadoWorkspace.QDy[ 22 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 144 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 6 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 70 ]), &(acadoWorkspace.QDy[ 16 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 86 ]), &(acadoWorkspace.QDy[ 18 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 104 ]), &(acadoWorkspace.QDy[ 20 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 124 ]), &(acadoWorkspace.QDy[ 22 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 146 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 170 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 7 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 88 ]), &(acadoWorkspace.QDy[ 18 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 106 ]), &(acadoWorkspace.QDy[ 20 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 126 ]), &(acadoWorkspace.QDy[ 22 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 148 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 172 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 198 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 8 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 108 ]), &(acadoWorkspace.QDy[ 20 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 128 ]), &(acadoWorkspace.QDy[ 22 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 150 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 174 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 200 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 228 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 9 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 130 ]), &(acadoWorkspace.QDy[ 22 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 152 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 176 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 202 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 230 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 260 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 10 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 154 ]), &(acadoWorkspace.QDy[ 24 ]), &(acadoWorkspace.g[ 11 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 178 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 11 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 204 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 11 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 232 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 11 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 262 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 11 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 294 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 11 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 11 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 11 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 11 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 180 ]), &(acadoWorkspace.QDy[ 26 ]), &(acadoWorkspace.g[ 12 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 206 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 12 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 234 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 12 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 264 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 12 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 296 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 12 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 330 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 12 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 12 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 12 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 208 ]), &(acadoWorkspace.QDy[ 28 ]), &(acadoWorkspace.g[ 13 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 236 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 13 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 266 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 13 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 298 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 13 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 332 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 13 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 368 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 13 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 406 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 13 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 238 ]), &(acadoWorkspace.QDy[ 30 ]), &(acadoWorkspace.g[ 14 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 268 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 14 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 300 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 14 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 334 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 14 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 370 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 14 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 408 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 14 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 270 ]), &(acadoWorkspace.QDy[ 32 ]), &(acadoWorkspace.g[ 15 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 302 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 15 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 336 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 15 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 372 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 15 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 410 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 15 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 304 ]), &(acadoWorkspace.QDy[ 34 ]), &(acadoWorkspace.g[ 16 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 338 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 16 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 374 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 16 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 412 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 16 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 340 ]), &(acadoWorkspace.QDy[ 36 ]), &(acadoWorkspace.g[ 17 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 376 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 17 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 414 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 17 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 378 ]), &(acadoWorkspace.QDy[ 38 ]), &(acadoWorkspace.g[ 18 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 416 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 18 ]) );
acado_multEQDy( &(acadoWorkspace.E[ 418 ]), &(acadoWorkspace.QDy[ 40 ]), &(acadoWorkspace.g[ 19 ]) );

acadoWorkspace.g[0] += + acadoWorkspace.H10[0]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[1]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[1] += + acadoWorkspace.H10[2]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[3]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[2] += + acadoWorkspace.H10[4]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[5]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[3] += + acadoWorkspace.H10[6]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[7]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[4] += + acadoWorkspace.H10[8]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[9]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[5] += + acadoWorkspace.H10[10]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[11]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[6] += + acadoWorkspace.H10[12]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[13]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[7] += + acadoWorkspace.H10[14]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[15]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[8] += + acadoWorkspace.H10[16]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[17]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[9] += + acadoWorkspace.H10[18]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[19]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[10] += + acadoWorkspace.H10[20]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[21]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[11] += + acadoWorkspace.H10[22]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[23]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[12] += + acadoWorkspace.H10[24]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[25]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[13] += + acadoWorkspace.H10[26]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[27]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[14] += + acadoWorkspace.H10[28]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[29]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[15] += + acadoWorkspace.H10[30]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[31]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[16] += + acadoWorkspace.H10[32]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[33]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[17] += + acadoWorkspace.H10[34]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[35]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[18] += + acadoWorkspace.H10[36]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[37]*acadoWorkspace.Dx0[1];
acadoWorkspace.g[19] += + acadoWorkspace.H10[38]*acadoWorkspace.Dx0[0] + acadoWorkspace.H10[39]*acadoWorkspace.Dx0[1];

acadoWorkspace.lb[0] = acadoVariables.lbValues[0] - acadoVariables.u[0];
acadoWorkspace.lb[1] = acadoVariables.lbValues[1] - acadoVariables.u[1];
acadoWorkspace.lb[2] = acadoVariables.lbValues[2] - acadoVariables.u[2];
acadoWorkspace.lb[3] = acadoVariables.lbValues[3] - acadoVariables.u[3];
acadoWorkspace.lb[4] = acadoVariables.lbValues[4] - acadoVariables.u[4];
acadoWorkspace.lb[5] = acadoVariables.lbValues[5] - acadoVariables.u[5];
acadoWorkspace.lb[6] = acadoVariables.lbValues[6] - acadoVariables.u[6];
acadoWorkspace.lb[7] = acadoVariables.lbValues[7] - acadoVariables.u[7];
acadoWorkspace.lb[8] = acadoVariables.lbValues[8] - acadoVariables.u[8];
acadoWorkspace.lb[9] = acadoVariables.lbValues[9] - acadoVariables.u[9];
acadoWorkspace.lb[10] = acadoVariables.lbValues[10] - acadoVariables.u[10];
acadoWorkspace.lb[11] = acadoVariables.lbValues[11] - acadoVariables.u[11];
acadoWorkspace.lb[12] = acadoVariables.lbValues[12] - acadoVariables.u[12];
acadoWorkspace.lb[13] = acadoVariables.lbValues[13] - acadoVariables.u[13];
acadoWorkspace.lb[14] = acadoVariables.lbValues[14] - acadoVariables.u[14];
acadoWorkspace.lb[15] = acadoVariables.lbValues[15] - acadoVariables.u[15];
acadoWorkspace.lb[16] = acadoVariables.lbValues[16] - acadoVariables.u[16];
acadoWorkspace.lb[17] = acadoVariables.lbValues[17] - acadoVariables.u[17];
acadoWorkspace.lb[18] = acadoVariables.lbValues[18] - acadoVariables.u[18];
acadoWorkspace.lb[19] = acadoVariables.lbValues[19] - acadoVariables.u[19];
acadoWorkspace.ub[0] = acadoVariables.ubValues[0] - acadoVariables.u[0];
acadoWorkspace.ub[1] = acadoVariables.ubValues[1] - acadoVariables.u[1];
acadoWorkspace.ub[2] = acadoVariables.ubValues[2] - acadoVariables.u[2];
acadoWorkspace.ub[3] = acadoVariables.ubValues[3] - acadoVariables.u[3];
acadoWorkspace.ub[4] = acadoVariables.ubValues[4] - acadoVariables.u[4];
acadoWorkspace.ub[5] = acadoVariables.ubValues[5] - acadoVariables.u[5];
acadoWorkspace.ub[6] = acadoVariables.ubValues[6] - acadoVariables.u[6];
acadoWorkspace.ub[7] = acadoVariables.ubValues[7] - acadoVariables.u[7];
acadoWorkspace.ub[8] = acadoVariables.ubValues[8] - acadoVariables.u[8];
acadoWorkspace.ub[9] = acadoVariables.ubValues[9] - acadoVariables.u[9];
acadoWorkspace.ub[10] = acadoVariables.ubValues[10] - acadoVariables.u[10];
acadoWorkspace.ub[11] = acadoVariables.ubValues[11] - acadoVariables.u[11];
acadoWorkspace.ub[12] = acadoVariables.ubValues[12] - acadoVariables.u[12];
acadoWorkspace.ub[13] = acadoVariables.ubValues[13] - acadoVariables.u[13];
acadoWorkspace.ub[14] = acadoVariables.ubValues[14] - acadoVariables.u[14];
acadoWorkspace.ub[15] = acadoVariables.ubValues[15] - acadoVariables.u[15];
acadoWorkspace.ub[16] = acadoVariables.ubValues[16] - acadoVariables.u[16];
acadoWorkspace.ub[17] = acadoVariables.ubValues[17] - acadoVariables.u[17];
acadoWorkspace.ub[18] = acadoVariables.ubValues[18] - acadoVariables.u[18];
acadoWorkspace.ub[19] = acadoVariables.ubValues[19] - acadoVariables.u[19];

tmp = + acadoWorkspace.evGx[0]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[1]*acadoWorkspace.Dx0[1] + acadoVariables.x[2];
tmp += acadoWorkspace.d[0];
acadoWorkspace.lbA[0] = acadoVariables.lbAValues[0] - tmp;
acadoWorkspace.ubA[0] = acadoVariables.ubAValues[0] - tmp;
tmp = + acadoWorkspace.evGx[2]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[3]*acadoWorkspace.Dx0[1] + acadoVariables.x[3];
tmp += acadoWorkspace.d[1];
acadoWorkspace.lbA[1] = acadoVariables.lbAValues[1] - tmp;
acadoWorkspace.ubA[1] = acadoVariables.ubAValues[1] - tmp;
tmp = + acadoWorkspace.evGx[4]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[5]*acadoWorkspace.Dx0[1] + acadoVariables.x[4];
tmp += acadoWorkspace.d[2];
acadoWorkspace.lbA[2] = acadoVariables.lbAValues[2] - tmp;
acadoWorkspace.ubA[2] = acadoVariables.ubAValues[2] - tmp;
tmp = + acadoWorkspace.evGx[6]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[7]*acadoWorkspace.Dx0[1] + acadoVariables.x[5];
tmp += acadoWorkspace.d[3];
acadoWorkspace.lbA[3] = acadoVariables.lbAValues[3] - tmp;
acadoWorkspace.ubA[3] = acadoVariables.ubAValues[3] - tmp;
tmp = + acadoWorkspace.evGx[8]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[9]*acadoWorkspace.Dx0[1] + acadoVariables.x[6];
tmp += acadoWorkspace.d[4];
acadoWorkspace.lbA[4] = acadoVariables.lbAValues[4] - tmp;
acadoWorkspace.ubA[4] = acadoVariables.ubAValues[4] - tmp;
tmp = + acadoWorkspace.evGx[10]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[11]*acadoWorkspace.Dx0[1] + acadoVariables.x[7];
tmp += acadoWorkspace.d[5];
acadoWorkspace.lbA[5] = acadoVariables.lbAValues[5] - tmp;
acadoWorkspace.ubA[5] = acadoVariables.ubAValues[5] - tmp;
tmp = + acadoWorkspace.evGx[12]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[13]*acadoWorkspace.Dx0[1] + acadoVariables.x[8];
tmp += acadoWorkspace.d[6];
acadoWorkspace.lbA[6] = acadoVariables.lbAValues[6] - tmp;
acadoWorkspace.ubA[6] = acadoVariables.ubAValues[6] - tmp;
tmp = + acadoWorkspace.evGx[14]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[15]*acadoWorkspace.Dx0[1] + acadoVariables.x[9];
tmp += acadoWorkspace.d[7];
acadoWorkspace.lbA[7] = acadoVariables.lbAValues[7] - tmp;
acadoWorkspace.ubA[7] = acadoVariables.ubAValues[7] - tmp;
tmp = + acadoWorkspace.evGx[16]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[17]*acadoWorkspace.Dx0[1] + acadoVariables.x[10];
tmp += acadoWorkspace.d[8];
acadoWorkspace.lbA[8] = acadoVariables.lbAValues[8] - tmp;
acadoWorkspace.ubA[8] = acadoVariables.ubAValues[8] - tmp;
tmp = + acadoWorkspace.evGx[18]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[19]*acadoWorkspace.Dx0[1] + acadoVariables.x[11];
tmp += acadoWorkspace.d[9];
acadoWorkspace.lbA[9] = acadoVariables.lbAValues[9] - tmp;
acadoWorkspace.ubA[9] = acadoVariables.ubAValues[9] - tmp;
tmp = + acadoWorkspace.evGx[20]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[21]*acadoWorkspace.Dx0[1] + acadoVariables.x[12];
tmp += acadoWorkspace.d[10];
acadoWorkspace.lbA[10] = acadoVariables.lbAValues[10] - tmp;
acadoWorkspace.ubA[10] = acadoVariables.ubAValues[10] - tmp;
tmp = + acadoWorkspace.evGx[22]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[23]*acadoWorkspace.Dx0[1] + acadoVariables.x[13];
tmp += acadoWorkspace.d[11];
acadoWorkspace.lbA[11] = acadoVariables.lbAValues[11] - tmp;
acadoWorkspace.ubA[11] = acadoVariables.ubAValues[11] - tmp;
tmp = + acadoWorkspace.evGx[24]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[25]*acadoWorkspace.Dx0[1] + acadoVariables.x[14];
tmp += acadoWorkspace.d[12];
acadoWorkspace.lbA[12] = acadoVariables.lbAValues[12] - tmp;
acadoWorkspace.ubA[12] = acadoVariables.ubAValues[12] - tmp;
tmp = + acadoWorkspace.evGx[26]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[27]*acadoWorkspace.Dx0[1] + acadoVariables.x[15];
tmp += acadoWorkspace.d[13];
acadoWorkspace.lbA[13] = acadoVariables.lbAValues[13] - tmp;
acadoWorkspace.ubA[13] = acadoVariables.ubAValues[13] - tmp;
tmp = + acadoWorkspace.evGx[28]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[29]*acadoWorkspace.Dx0[1] + acadoVariables.x[16];
tmp += acadoWorkspace.d[14];
acadoWorkspace.lbA[14] = acadoVariables.lbAValues[14] - tmp;
acadoWorkspace.ubA[14] = acadoVariables.ubAValues[14] - tmp;
tmp = + acadoWorkspace.evGx[30]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[31]*acadoWorkspace.Dx0[1] + acadoVariables.x[17];
tmp += acadoWorkspace.d[15];
acadoWorkspace.lbA[15] = acadoVariables.lbAValues[15] - tmp;
acadoWorkspace.ubA[15] = acadoVariables.ubAValues[15] - tmp;
tmp = + acadoWorkspace.evGx[32]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[33]*acadoWorkspace.Dx0[1] + acadoVariables.x[18];
tmp += acadoWorkspace.d[16];
acadoWorkspace.lbA[16] = acadoVariables.lbAValues[16] - tmp;
acadoWorkspace.ubA[16] = acadoVariables.ubAValues[16] - tmp;
tmp = + acadoWorkspace.evGx[34]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[35]*acadoWorkspace.Dx0[1] + acadoVariables.x[19];
tmp += acadoWorkspace.d[17];
acadoWorkspace.lbA[17] = acadoVariables.lbAValues[17] - tmp;
acadoWorkspace.ubA[17] = acadoVariables.ubAValues[17] - tmp;
tmp = + acadoWorkspace.evGx[36]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[37]*acadoWorkspace.Dx0[1] + acadoVariables.x[20];
tmp += acadoWorkspace.d[18];
acadoWorkspace.lbA[18] = acadoVariables.lbAValues[18] - tmp;
acadoWorkspace.ubA[18] = acadoVariables.ubAValues[18] - tmp;
tmp = + acadoWorkspace.evGx[38]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[39]*acadoWorkspace.Dx0[1] + acadoVariables.x[21];
tmp += acadoWorkspace.d[19];
acadoWorkspace.lbA[19] = acadoVariables.lbAValues[19] - tmp;
acadoWorkspace.ubA[19] = acadoVariables.ubAValues[19] - tmp;
tmp = + acadoWorkspace.evGx[40]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[41]*acadoWorkspace.Dx0[1] + acadoVariables.x[22];
tmp += acadoWorkspace.d[20];
acadoWorkspace.lbA[20] = acadoVariables.lbAValues[20] - tmp;
acadoWorkspace.ubA[20] = acadoVariables.ubAValues[20] - tmp;
tmp = + acadoWorkspace.evGx[42]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[43]*acadoWorkspace.Dx0[1] + acadoVariables.x[23];
tmp += acadoWorkspace.d[21];
acadoWorkspace.lbA[21] = acadoVariables.lbAValues[21] - tmp;
acadoWorkspace.ubA[21] = acadoVariables.ubAValues[21] - tmp;
tmp = + acadoWorkspace.evGx[44]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[45]*acadoWorkspace.Dx0[1] + acadoVariables.x[24];
tmp += acadoWorkspace.d[22];
acadoWorkspace.lbA[22] = acadoVariables.lbAValues[22] - tmp;
acadoWorkspace.ubA[22] = acadoVariables.ubAValues[22] - tmp;
tmp = + acadoWorkspace.evGx[46]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[47]*acadoWorkspace.Dx0[1] + acadoVariables.x[25];
tmp += acadoWorkspace.d[23];
acadoWorkspace.lbA[23] = acadoVariables.lbAValues[23] - tmp;
acadoWorkspace.ubA[23] = acadoVariables.ubAValues[23] - tmp;
tmp = + acadoWorkspace.evGx[48]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[49]*acadoWorkspace.Dx0[1] + acadoVariables.x[26];
tmp += acadoWorkspace.d[24];
acadoWorkspace.lbA[24] = acadoVariables.lbAValues[24] - tmp;
acadoWorkspace.ubA[24] = acadoVariables.ubAValues[24] - tmp;
tmp = + acadoWorkspace.evGx[50]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[51]*acadoWorkspace.Dx0[1] + acadoVariables.x[27];
tmp += acadoWorkspace.d[25];
acadoWorkspace.lbA[25] = acadoVariables.lbAValues[25] - tmp;
acadoWorkspace.ubA[25] = acadoVariables.ubAValues[25] - tmp;
tmp = + acadoWorkspace.evGx[52]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[53]*acadoWorkspace.Dx0[1] + acadoVariables.x[28];
tmp += acadoWorkspace.d[26];
acadoWorkspace.lbA[26] = acadoVariables.lbAValues[26] - tmp;
acadoWorkspace.ubA[26] = acadoVariables.ubAValues[26] - tmp;
tmp = + acadoWorkspace.evGx[54]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[55]*acadoWorkspace.Dx0[1] + acadoVariables.x[29];
tmp += acadoWorkspace.d[27];
acadoWorkspace.lbA[27] = acadoVariables.lbAValues[27] - tmp;
acadoWorkspace.ubA[27] = acadoVariables.ubAValues[27] - tmp;
tmp = + acadoWorkspace.evGx[56]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[57]*acadoWorkspace.Dx0[1] + acadoVariables.x[30];
tmp += acadoWorkspace.d[28];
acadoWorkspace.lbA[28] = acadoVariables.lbAValues[28] - tmp;
acadoWorkspace.ubA[28] = acadoVariables.ubAValues[28] - tmp;
tmp = + acadoWorkspace.evGx[58]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[59]*acadoWorkspace.Dx0[1] + acadoVariables.x[31];
tmp += acadoWorkspace.d[29];
acadoWorkspace.lbA[29] = acadoVariables.lbAValues[29] - tmp;
acadoWorkspace.ubA[29] = acadoVariables.ubAValues[29] - tmp;
tmp = + acadoWorkspace.evGx[60]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[61]*acadoWorkspace.Dx0[1] + acadoVariables.x[32];
tmp += acadoWorkspace.d[30];
acadoWorkspace.lbA[30] = acadoVariables.lbAValues[30] - tmp;
acadoWorkspace.ubA[30] = acadoVariables.ubAValues[30] - tmp;
tmp = + acadoWorkspace.evGx[62]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[63]*acadoWorkspace.Dx0[1] + acadoVariables.x[33];
tmp += acadoWorkspace.d[31];
acadoWorkspace.lbA[31] = acadoVariables.lbAValues[31] - tmp;
acadoWorkspace.ubA[31] = acadoVariables.ubAValues[31] - tmp;
tmp = + acadoWorkspace.evGx[64]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[65]*acadoWorkspace.Dx0[1] + acadoVariables.x[34];
tmp += acadoWorkspace.d[32];
acadoWorkspace.lbA[32] = acadoVariables.lbAValues[32] - tmp;
acadoWorkspace.ubA[32] = acadoVariables.ubAValues[32] - tmp;
tmp = + acadoWorkspace.evGx[66]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[67]*acadoWorkspace.Dx0[1] + acadoVariables.x[35];
tmp += acadoWorkspace.d[33];
acadoWorkspace.lbA[33] = acadoVariables.lbAValues[33] - tmp;
acadoWorkspace.ubA[33] = acadoVariables.ubAValues[33] - tmp;
tmp = + acadoWorkspace.evGx[68]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[69]*acadoWorkspace.Dx0[1] + acadoVariables.x[36];
tmp += acadoWorkspace.d[34];
acadoWorkspace.lbA[34] = acadoVariables.lbAValues[34] - tmp;
acadoWorkspace.ubA[34] = acadoVariables.ubAValues[34] - tmp;
tmp = + acadoWorkspace.evGx[70]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[71]*acadoWorkspace.Dx0[1] + acadoVariables.x[37];
tmp += acadoWorkspace.d[35];
acadoWorkspace.lbA[35] = acadoVariables.lbAValues[35] - tmp;
acadoWorkspace.ubA[35] = acadoVariables.ubAValues[35] - tmp;
tmp = + acadoWorkspace.evGx[72]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[73]*acadoWorkspace.Dx0[1] + acadoVariables.x[38];
tmp += acadoWorkspace.d[36];
acadoWorkspace.lbA[36] = acadoVariables.lbAValues[36] - tmp;
acadoWorkspace.ubA[36] = acadoVariables.ubAValues[36] - tmp;
tmp = + acadoWorkspace.evGx[74]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[75]*acadoWorkspace.Dx0[1] + acadoVariables.x[39];
tmp += acadoWorkspace.d[37];
acadoWorkspace.lbA[37] = acadoVariables.lbAValues[37] - tmp;
acadoWorkspace.ubA[37] = acadoVariables.ubAValues[37] - tmp;
tmp = + acadoWorkspace.evGx[76]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[77]*acadoWorkspace.Dx0[1] + acadoVariables.x[40];
tmp += acadoWorkspace.d[38];
acadoWorkspace.lbA[38] = acadoVariables.lbAValues[38] - tmp;
acadoWorkspace.ubA[38] = acadoVariables.ubAValues[38] - tmp;
tmp = + acadoWorkspace.evGx[78]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[79]*acadoWorkspace.Dx0[1] + acadoVariables.x[41];
tmp += acadoWorkspace.d[39];
acadoWorkspace.lbA[39] = acadoVariables.lbAValues[39] - tmp;
acadoWorkspace.ubA[39] = acadoVariables.ubAValues[39] - tmp;

}

void acado_expand(  )
{
acadoVariables.u[0] += acadoWorkspace.x[0];
acadoVariables.u[1] += acadoWorkspace.x[1];
acadoVariables.u[2] += acadoWorkspace.x[2];
acadoVariables.u[3] += acadoWorkspace.x[3];
acadoVariables.u[4] += acadoWorkspace.x[4];
acadoVariables.u[5] += acadoWorkspace.x[5];
acadoVariables.u[6] += acadoWorkspace.x[6];
acadoVariables.u[7] += acadoWorkspace.x[7];
acadoVariables.u[8] += acadoWorkspace.x[8];
acadoVariables.u[9] += acadoWorkspace.x[9];
acadoVariables.u[10] += acadoWorkspace.x[10];
acadoVariables.u[11] += acadoWorkspace.x[11];
acadoVariables.u[12] += acadoWorkspace.x[12];
acadoVariables.u[13] += acadoWorkspace.x[13];
acadoVariables.u[14] += acadoWorkspace.x[14];
acadoVariables.u[15] += acadoWorkspace.x[15];
acadoVariables.u[16] += acadoWorkspace.x[16];
acadoVariables.u[17] += acadoWorkspace.x[17];
acadoVariables.u[18] += acadoWorkspace.x[18];
acadoVariables.u[19] += acadoWorkspace.x[19];

acadoVariables.x[0] += acadoWorkspace.Dx0[0];
acadoVariables.x[1] += acadoWorkspace.Dx0[1];

acadoVariables.x[2] += + acadoWorkspace.evGx[0]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[1]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[0];
acadoVariables.x[3] += + acadoWorkspace.evGx[2]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[3]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[1];
acadoVariables.x[4] += + acadoWorkspace.evGx[4]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[5]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[2];
acadoVariables.x[5] += + acadoWorkspace.evGx[6]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[7]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[3];
acadoVariables.x[6] += + acadoWorkspace.evGx[8]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[9]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[4];
acadoVariables.x[7] += + acadoWorkspace.evGx[10]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[11]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[5];
acadoVariables.x[8] += + acadoWorkspace.evGx[12]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[13]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[6];
acadoVariables.x[9] += + acadoWorkspace.evGx[14]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[15]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[7];
acadoVariables.x[10] += + acadoWorkspace.evGx[16]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[17]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[8];
acadoVariables.x[11] += + acadoWorkspace.evGx[18]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[19]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[9];
acadoVariables.x[12] += + acadoWorkspace.evGx[20]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[21]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[10];
acadoVariables.x[13] += + acadoWorkspace.evGx[22]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[23]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[11];
acadoVariables.x[14] += + acadoWorkspace.evGx[24]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[25]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[12];
acadoVariables.x[15] += + acadoWorkspace.evGx[26]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[27]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[13];
acadoVariables.x[16] += + acadoWorkspace.evGx[28]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[29]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[14];
acadoVariables.x[17] += + acadoWorkspace.evGx[30]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[31]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[15];
acadoVariables.x[18] += + acadoWorkspace.evGx[32]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[33]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[16];
acadoVariables.x[19] += + acadoWorkspace.evGx[34]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[35]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[17];
acadoVariables.x[20] += + acadoWorkspace.evGx[36]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[37]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[18];
acadoVariables.x[21] += + acadoWorkspace.evGx[38]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[39]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[19];
acadoVariables.x[22] += + acadoWorkspace.evGx[40]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[41]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[20];
acadoVariables.x[23] += + acadoWorkspace.evGx[42]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[43]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[21];
acadoVariables.x[24] += + acadoWorkspace.evGx[44]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[45]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[22];
acadoVariables.x[25] += + acadoWorkspace.evGx[46]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[47]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[23];
acadoVariables.x[26] += + acadoWorkspace.evGx[48]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[49]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[24];
acadoVariables.x[27] += + acadoWorkspace.evGx[50]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[51]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[25];
acadoVariables.x[28] += + acadoWorkspace.evGx[52]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[53]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[26];
acadoVariables.x[29] += + acadoWorkspace.evGx[54]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[55]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[27];
acadoVariables.x[30] += + acadoWorkspace.evGx[56]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[57]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[28];
acadoVariables.x[31] += + acadoWorkspace.evGx[58]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[59]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[29];
acadoVariables.x[32] += + acadoWorkspace.evGx[60]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[61]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[30];
acadoVariables.x[33] += + acadoWorkspace.evGx[62]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[63]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[31];
acadoVariables.x[34] += + acadoWorkspace.evGx[64]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[65]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[32];
acadoVariables.x[35] += + acadoWorkspace.evGx[66]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[67]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[33];
acadoVariables.x[36] += + acadoWorkspace.evGx[68]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[69]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[34];
acadoVariables.x[37] += + acadoWorkspace.evGx[70]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[71]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[35];
acadoVariables.x[38] += + acadoWorkspace.evGx[72]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[73]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[36];
acadoVariables.x[39] += + acadoWorkspace.evGx[74]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[75]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[37];
acadoVariables.x[40] += + acadoWorkspace.evGx[76]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[77]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[38];
acadoVariables.x[41] += + acadoWorkspace.evGx[78]*acadoWorkspace.Dx0[0] + acadoWorkspace.evGx[79]*acadoWorkspace.Dx0[1] + acadoWorkspace.d[39];

acado_multEDu( acadoWorkspace.E, acadoWorkspace.x, &(acadoVariables.x[ 2 ]) );
acado_multEDu( &(acadoWorkspace.E[ 2 ]), acadoWorkspace.x, &(acadoVariables.x[ 4 ]) );
acado_multEDu( &(acadoWorkspace.E[ 4 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 4 ]) );
acado_multEDu( &(acadoWorkspace.E[ 6 ]), acadoWorkspace.x, &(acadoVariables.x[ 6 ]) );
acado_multEDu( &(acadoWorkspace.E[ 8 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 6 ]) );
acado_multEDu( &(acadoWorkspace.E[ 10 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 6 ]) );
acado_multEDu( &(acadoWorkspace.E[ 12 ]), acadoWorkspace.x, &(acadoVariables.x[ 8 ]) );
acado_multEDu( &(acadoWorkspace.E[ 14 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 8 ]) );
acado_multEDu( &(acadoWorkspace.E[ 16 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 8 ]) );
acado_multEDu( &(acadoWorkspace.E[ 18 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 8 ]) );
acado_multEDu( &(acadoWorkspace.E[ 20 ]), acadoWorkspace.x, &(acadoVariables.x[ 10 ]) );
acado_multEDu( &(acadoWorkspace.E[ 22 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 10 ]) );
acado_multEDu( &(acadoWorkspace.E[ 24 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 10 ]) );
acado_multEDu( &(acadoWorkspace.E[ 26 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 10 ]) );
acado_multEDu( &(acadoWorkspace.E[ 28 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 10 ]) );
acado_multEDu( &(acadoWorkspace.E[ 30 ]), acadoWorkspace.x, &(acadoVariables.x[ 12 ]) );
acado_multEDu( &(acadoWorkspace.E[ 32 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 12 ]) );
acado_multEDu( &(acadoWorkspace.E[ 34 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 12 ]) );
acado_multEDu( &(acadoWorkspace.E[ 36 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 12 ]) );
acado_multEDu( &(acadoWorkspace.E[ 38 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 12 ]) );
acado_multEDu( &(acadoWorkspace.E[ 40 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 12 ]) );
acado_multEDu( &(acadoWorkspace.E[ 42 ]), acadoWorkspace.x, &(acadoVariables.x[ 14 ]) );
acado_multEDu( &(acadoWorkspace.E[ 44 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 14 ]) );
acado_multEDu( &(acadoWorkspace.E[ 46 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 14 ]) );
acado_multEDu( &(acadoWorkspace.E[ 48 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 14 ]) );
acado_multEDu( &(acadoWorkspace.E[ 50 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 14 ]) );
acado_multEDu( &(acadoWorkspace.E[ 52 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 14 ]) );
acado_multEDu( &(acadoWorkspace.E[ 54 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 14 ]) );
acado_multEDu( &(acadoWorkspace.E[ 56 ]), acadoWorkspace.x, &(acadoVariables.x[ 16 ]) );
acado_multEDu( &(acadoWorkspace.E[ 58 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 16 ]) );
acado_multEDu( &(acadoWorkspace.E[ 60 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 16 ]) );
acado_multEDu( &(acadoWorkspace.E[ 62 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 16 ]) );
acado_multEDu( &(acadoWorkspace.E[ 64 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 16 ]) );
acado_multEDu( &(acadoWorkspace.E[ 66 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 16 ]) );
acado_multEDu( &(acadoWorkspace.E[ 68 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 16 ]) );
acado_multEDu( &(acadoWorkspace.E[ 70 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 16 ]) );
acado_multEDu( &(acadoWorkspace.E[ 72 ]), acadoWorkspace.x, &(acadoVariables.x[ 18 ]) );
acado_multEDu( &(acadoWorkspace.E[ 74 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 18 ]) );
acado_multEDu( &(acadoWorkspace.E[ 76 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 18 ]) );
acado_multEDu( &(acadoWorkspace.E[ 78 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 18 ]) );
acado_multEDu( &(acadoWorkspace.E[ 80 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 18 ]) );
acado_multEDu( &(acadoWorkspace.E[ 82 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 18 ]) );
acado_multEDu( &(acadoWorkspace.E[ 84 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 18 ]) );
acado_multEDu( &(acadoWorkspace.E[ 86 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 18 ]) );
acado_multEDu( &(acadoWorkspace.E[ 88 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 18 ]) );
acado_multEDu( &(acadoWorkspace.E[ 90 ]), acadoWorkspace.x, &(acadoVariables.x[ 20 ]) );
acado_multEDu( &(acadoWorkspace.E[ 92 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 20 ]) );
acado_multEDu( &(acadoWorkspace.E[ 94 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 20 ]) );
acado_multEDu( &(acadoWorkspace.E[ 96 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 20 ]) );
acado_multEDu( &(acadoWorkspace.E[ 98 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 20 ]) );
acado_multEDu( &(acadoWorkspace.E[ 100 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 20 ]) );
acado_multEDu( &(acadoWorkspace.E[ 102 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 20 ]) );
acado_multEDu( &(acadoWorkspace.E[ 104 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 20 ]) );
acado_multEDu( &(acadoWorkspace.E[ 106 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 20 ]) );
acado_multEDu( &(acadoWorkspace.E[ 108 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 20 ]) );
acado_multEDu( &(acadoWorkspace.E[ 110 ]), acadoWorkspace.x, &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 112 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 114 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 116 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 118 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 120 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 122 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 124 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 126 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 128 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 130 ]), &(acadoWorkspace.x[ 10 ]), &(acadoVariables.x[ 22 ]) );
acado_multEDu( &(acadoWorkspace.E[ 132 ]), acadoWorkspace.x, &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 134 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 136 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 138 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 140 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 142 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 144 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 146 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 148 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 150 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 152 ]), &(acadoWorkspace.x[ 10 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 154 ]), &(acadoWorkspace.x[ 11 ]), &(acadoVariables.x[ 24 ]) );
acado_multEDu( &(acadoWorkspace.E[ 156 ]), acadoWorkspace.x, &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 158 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 160 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 162 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 164 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 166 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 168 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 170 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 172 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 174 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 176 ]), &(acadoWorkspace.x[ 10 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 178 ]), &(acadoWorkspace.x[ 11 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 180 ]), &(acadoWorkspace.x[ 12 ]), &(acadoVariables.x[ 26 ]) );
acado_multEDu( &(acadoWorkspace.E[ 182 ]), acadoWorkspace.x, &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 184 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 186 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 188 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 190 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 192 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 194 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 196 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 198 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 200 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 202 ]), &(acadoWorkspace.x[ 10 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 204 ]), &(acadoWorkspace.x[ 11 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 206 ]), &(acadoWorkspace.x[ 12 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 208 ]), &(acadoWorkspace.x[ 13 ]), &(acadoVariables.x[ 28 ]) );
acado_multEDu( &(acadoWorkspace.E[ 210 ]), acadoWorkspace.x, &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 212 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 214 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 216 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 218 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 220 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 222 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 224 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 226 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 228 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 230 ]), &(acadoWorkspace.x[ 10 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 232 ]), &(acadoWorkspace.x[ 11 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 234 ]), &(acadoWorkspace.x[ 12 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 236 ]), &(acadoWorkspace.x[ 13 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 238 ]), &(acadoWorkspace.x[ 14 ]), &(acadoVariables.x[ 30 ]) );
acado_multEDu( &(acadoWorkspace.E[ 240 ]), acadoWorkspace.x, &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 242 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 244 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 246 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 248 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 250 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 252 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 254 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 256 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 258 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 260 ]), &(acadoWorkspace.x[ 10 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 262 ]), &(acadoWorkspace.x[ 11 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 264 ]), &(acadoWorkspace.x[ 12 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 266 ]), &(acadoWorkspace.x[ 13 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 268 ]), &(acadoWorkspace.x[ 14 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 270 ]), &(acadoWorkspace.x[ 15 ]), &(acadoVariables.x[ 32 ]) );
acado_multEDu( &(acadoWorkspace.E[ 272 ]), acadoWorkspace.x, &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 274 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 276 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 278 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 280 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 282 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 284 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 286 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 288 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 290 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 292 ]), &(acadoWorkspace.x[ 10 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 294 ]), &(acadoWorkspace.x[ 11 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 296 ]), &(acadoWorkspace.x[ 12 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 298 ]), &(acadoWorkspace.x[ 13 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 300 ]), &(acadoWorkspace.x[ 14 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 302 ]), &(acadoWorkspace.x[ 15 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 304 ]), &(acadoWorkspace.x[ 16 ]), &(acadoVariables.x[ 34 ]) );
acado_multEDu( &(acadoWorkspace.E[ 306 ]), acadoWorkspace.x, &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 308 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 310 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 312 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 314 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 316 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 318 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 320 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 322 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 324 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 326 ]), &(acadoWorkspace.x[ 10 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 328 ]), &(acadoWorkspace.x[ 11 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 330 ]), &(acadoWorkspace.x[ 12 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 332 ]), &(acadoWorkspace.x[ 13 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 334 ]), &(acadoWorkspace.x[ 14 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 336 ]), &(acadoWorkspace.x[ 15 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 338 ]), &(acadoWorkspace.x[ 16 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 340 ]), &(acadoWorkspace.x[ 17 ]), &(acadoVariables.x[ 36 ]) );
acado_multEDu( &(acadoWorkspace.E[ 342 ]), acadoWorkspace.x, &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 344 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 346 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 348 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 350 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 352 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 354 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 356 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 358 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 360 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 362 ]), &(acadoWorkspace.x[ 10 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 364 ]), &(acadoWorkspace.x[ 11 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 366 ]), &(acadoWorkspace.x[ 12 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 368 ]), &(acadoWorkspace.x[ 13 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 370 ]), &(acadoWorkspace.x[ 14 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 372 ]), &(acadoWorkspace.x[ 15 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 374 ]), &(acadoWorkspace.x[ 16 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 376 ]), &(acadoWorkspace.x[ 17 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 378 ]), &(acadoWorkspace.x[ 18 ]), &(acadoVariables.x[ 38 ]) );
acado_multEDu( &(acadoWorkspace.E[ 380 ]), acadoWorkspace.x, &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 382 ]), &(acadoWorkspace.x[ 1 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 384 ]), &(acadoWorkspace.x[ 2 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 386 ]), &(acadoWorkspace.x[ 3 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 388 ]), &(acadoWorkspace.x[ 4 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 390 ]), &(acadoWorkspace.x[ 5 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 392 ]), &(acadoWorkspace.x[ 6 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 394 ]), &(acadoWorkspace.x[ 7 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 396 ]), &(acadoWorkspace.x[ 8 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 398 ]), &(acadoWorkspace.x[ 9 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 400 ]), &(acadoWorkspace.x[ 10 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 402 ]), &(acadoWorkspace.x[ 11 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 404 ]), &(acadoWorkspace.x[ 12 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 406 ]), &(acadoWorkspace.x[ 13 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 408 ]), &(acadoWorkspace.x[ 14 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 410 ]), &(acadoWorkspace.x[ 15 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 412 ]), &(acadoWorkspace.x[ 16 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 414 ]), &(acadoWorkspace.x[ 17 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 416 ]), &(acadoWorkspace.x[ 18 ]), &(acadoVariables.x[ 40 ]) );
acado_multEDu( &(acadoWorkspace.E[ 418 ]), &(acadoWorkspace.x[ 19 ]), &(acadoVariables.x[ 40 ]) );
}

int acado_preparationStep(  )
{
int ret;

ret = acado_modelSimulation();
acado_evaluateObjective(  );
acado_condensePrep(  );
return ret;
}

int acado_feedbackStep(  )
{
int tmp;

acado_condenseFdb(  );

tmp = acado_solve( );

acado_expand(  );
return tmp;
}

int acado_initializeSolver(  )
{
int ret;

/* This is a function which must be called once before any other function call! */


ret = 0;

memset(&acadoWorkspace, 0, sizeof( acadoWorkspace ));
acadoVariables.lbValues[0] = -2.0000000000000000e+01;
acadoVariables.lbValues[1] = -2.0000000000000000e+01;
acadoVariables.lbValues[2] = -2.0000000000000000e+01;
acadoVariables.lbValues[3] = -2.0000000000000000e+01;
acadoVariables.lbValues[4] = -2.0000000000000000e+01;
acadoVariables.lbValues[5] = -2.0000000000000000e+01;
acadoVariables.lbValues[6] = -2.0000000000000000e+01;
acadoVariables.lbValues[7] = -2.0000000000000000e+01;
acadoVariables.lbValues[8] = -2.0000000000000000e+01;
acadoVariables.lbValues[9] = -2.0000000000000000e+01;
acadoVariables.lbValues[10] = -2.0000000000000000e+01;
acadoVariables.lbValues[11] = -2.0000000000000000e+01;
acadoVariables.lbValues[12] = -2.0000000000000000e+01;
acadoVariables.lbValues[13] = -2.0000000000000000e+01;
acadoVariables.lbValues[14] = -2.0000000000000000e+01;
acadoVariables.lbValues[15] = -2.0000000000000000e+01;
acadoVariables.lbValues[16] = -2.0000000000000000e+01;
acadoVariables.lbValues[17] = -2.0000000000000000e+01;
acadoVariables.lbValues[18] = -2.0000000000000000e+01;
acadoVariables.lbValues[19] = -2.0000000000000000e+01;
acadoVariables.ubValues[0] = 2.0000000000000000e+01;
acadoVariables.ubValues[1] = 2.0000000000000000e+01;
acadoVariables.ubValues[2] = 2.0000000000000000e+01;
acadoVariables.ubValues[3] = 2.0000000000000000e+01;
acadoVariables.ubValues[4] = 2.0000000000000000e+01;
acadoVariables.ubValues[5] = 2.0000000000000000e+01;
acadoVariables.ubValues[6] = 2.0000000000000000e+01;
acadoVariables.ubValues[7] = 2.0000000000000000e+01;
acadoVariables.ubValues[8] = 2.0000000000000000e+01;
acadoVariables.ubValues[9] = 2.0000000000000000e+01;
acadoVariables.ubValues[10] = 2.0000000000000000e+01;
acadoVariables.ubValues[11] = 2.0000000000000000e+01;
acadoVariables.ubValues[12] = 2.0000000000000000e+01;
acadoVariables.ubValues[13] = 2.0000000000000000e+01;
acadoVariables.ubValues[14] = 2.0000000000000000e+01;
acadoVariables.ubValues[15] = 2.0000000000000000e+01;
acadoVariables.ubValues[16] = 2.0000000000000000e+01;
acadoVariables.ubValues[17] = 2.0000000000000000e+01;
acadoVariables.ubValues[18] = 2.0000000000000000e+01;
acadoVariables.ubValues[19] = 2.0000000000000000e+01;
acadoVariables.lbAValues[0] = -5.3630000000000000e-01;
acadoVariables.lbAValues[1] = -1.0000000000000000e+01;
acadoVariables.lbAValues[2] = -5.3630000000000000e-01;
acadoVariables.lbAValues[3] = -1.0000000000000000e+01;
acadoVariables.lbAValues[4] = -5.3630000000000000e-01;
acadoVariables.lbAValues[5] = -1.0000000000000000e+01;
acadoVariables.lbAValues[6] = -5.3630000000000000e-01;
acadoVariables.lbAValues[7] = -1.0000000000000000e+01;
acadoVariables.lbAValues[8] = -5.3630000000000000e-01;
acadoVariables.lbAValues[9] = -1.0000000000000000e+01;
acadoVariables.lbAValues[10] = -5.3630000000000000e-01;
acadoVariables.lbAValues[11] = -1.0000000000000000e+01;
acadoVariables.lbAValues[12] = -5.3630000000000000e-01;
acadoVariables.lbAValues[13] = -1.0000000000000000e+01;
acadoVariables.lbAValues[14] = -5.3630000000000000e-01;
acadoVariables.lbAValues[15] = -1.0000000000000000e+01;
acadoVariables.lbAValues[16] = -5.3630000000000000e-01;
acadoVariables.lbAValues[17] = -1.0000000000000000e+01;
acadoVariables.lbAValues[18] = -5.3630000000000000e-01;
acadoVariables.lbAValues[19] = -1.0000000000000000e+01;
acadoVariables.lbAValues[20] = -5.3630000000000000e-01;
acadoVariables.lbAValues[21] = -1.0000000000000000e+01;
acadoVariables.lbAValues[22] = -5.3630000000000000e-01;
acadoVariables.lbAValues[23] = -1.0000000000000000e+01;
acadoVariables.lbAValues[24] = -5.3630000000000000e-01;
acadoVariables.lbAValues[25] = -1.0000000000000000e+01;
acadoVariables.lbAValues[26] = -5.3630000000000000e-01;
acadoVariables.lbAValues[27] = -1.0000000000000000e+01;
acadoVariables.lbAValues[28] = -5.3630000000000000e-01;
acadoVariables.lbAValues[29] = -1.0000000000000000e+01;
acadoVariables.lbAValues[30] = -5.3630000000000000e-01;
acadoVariables.lbAValues[31] = -1.0000000000000000e+01;
acadoVariables.lbAValues[32] = -5.3630000000000000e-01;
acadoVariables.lbAValues[33] = -1.0000000000000000e+01;
acadoVariables.lbAValues[34] = -5.3630000000000000e-01;
acadoVariables.lbAValues[35] = -1.0000000000000000e+01;
acadoVariables.lbAValues[36] = -5.3630000000000000e-01;
acadoVariables.lbAValues[37] = -1.0000000000000000e+01;
acadoVariables.lbAValues[38] = -5.3630000000000000e-01;
acadoVariables.lbAValues[39] = -1.0000000000000000e+01;
acadoVariables.ubAValues[0] = 5.3630000000000000e-01;
acadoVariables.ubAValues[1] = 1.0000000000000000e+01;
acadoVariables.ubAValues[2] = 5.3630000000000000e-01;
acadoVariables.ubAValues[3] = 1.0000000000000000e+01;
acadoVariables.ubAValues[4] = 5.3630000000000000e-01;
acadoVariables.ubAValues[5] = 1.0000000000000000e+01;
acadoVariables.ubAValues[6] = 5.3630000000000000e-01;
acadoVariables.ubAValues[7] = 1.0000000000000000e+01;
acadoVariables.ubAValues[8] = 5.3630000000000000e-01;
acadoVariables.ubAValues[9] = 1.0000000000000000e+01;
acadoVariables.ubAValues[10] = 5.3630000000000000e-01;
acadoVariables.ubAValues[11] = 1.0000000000000000e+01;
acadoVariables.ubAValues[12] = 5.3630000000000000e-01;
acadoVariables.ubAValues[13] = 1.0000000000000000e+01;
acadoVariables.ubAValues[14] = 5.3630000000000000e-01;
acadoVariables.ubAValues[15] = 1.0000000000000000e+01;
acadoVariables.ubAValues[16] = 5.3630000000000000e-01;
acadoVariables.ubAValues[17] = 1.0000000000000000e+01;
acadoVariables.ubAValues[18] = 5.3630000000000000e-01;
acadoVariables.ubAValues[19] = 1.0000000000000000e+01;
acadoVariables.ubAValues[20] = 5.3630000000000000e-01;
acadoVariables.ubAValues[21] = 1.0000000000000000e+01;
acadoVariables.ubAValues[22] = 5.3630000000000000e-01;
acadoVariables.ubAValues[23] = 1.0000000000000000e+01;
acadoVariables.ubAValues[24] = 5.3630000000000000e-01;
acadoVariables.ubAValues[25] = 1.0000000000000000e+01;
acadoVariables.ubAValues[26] = 5.3630000000000000e-01;
acadoVariables.ubAValues[27] = 1.0000000000000000e+01;
acadoVariables.ubAValues[28] = 5.3630000000000000e-01;
acadoVariables.ubAValues[29] = 1.0000000000000000e+01;
acadoVariables.ubAValues[30] = 5.3630000000000000e-01;
acadoVariables.ubAValues[31] = 1.0000000000000000e+01;
acadoVariables.ubAValues[32] = 5.3630000000000000e-01;
acadoVariables.ubAValues[33] = 1.0000000000000000e+01;
acadoVariables.ubAValues[34] = 5.3630000000000000e-01;
acadoVariables.ubAValues[35] = 1.0000000000000000e+01;
acadoVariables.ubAValues[36] = 5.3630000000000000e-01;
acadoVariables.ubAValues[37] = 1.0000000000000000e+01;
acadoVariables.ubAValues[38] = 5.3630000000000000e-01;
acadoVariables.ubAValues[39] = 1.0000000000000000e+01;
return ret;
}

void acado_initializeNodesByForwardSimulation(  )
{
int index;
for (index = 0; index < 20; ++index)
{
acadoWorkspace.state[0] = acadoVariables.x[index * 2];
acadoWorkspace.state[1] = acadoVariables.x[index * 2 + 1];
acadoWorkspace.state[8] = acadoVariables.u[index];

acado_integrate(acadoWorkspace.state, index == 0);

acadoVariables.x[index * 2 + 2] = acadoWorkspace.state[0];
acadoVariables.x[index * 2 + 3] = acadoWorkspace.state[1];
}
}

void acado_shiftStates( int strategy, real_t* const xEnd, real_t* const uEnd )
{
int index;
for (index = 0; index < 20; ++index)
{
acadoVariables.x[index * 2] = acadoVariables.x[index * 2 + 2];
acadoVariables.x[index * 2 + 1] = acadoVariables.x[index * 2 + 3];
}

if (strategy == 1 && xEnd != 0)
{
acadoVariables.x[40] = xEnd[0];
acadoVariables.x[41] = xEnd[1];
}
else if (strategy == 2) 
{
acadoWorkspace.state[0] = acadoVariables.x[40];
acadoWorkspace.state[1] = acadoVariables.x[41];
if (uEnd != 0)
{
acadoWorkspace.state[8] = uEnd[0];
}
else
{
acadoWorkspace.state[8] = acadoVariables.u[19];
}

acado_integrate(acadoWorkspace.state, 1);

acadoVariables.x[40] = acadoWorkspace.state[0];
acadoVariables.x[41] = acadoWorkspace.state[1];
}
}

void acado_shiftControls( real_t* const uEnd )
{
int index;
for (index = 0; index < 19; ++index)
{
acadoVariables.u[index] = acadoVariables.u[index + 1];
}

if (uEnd != 0)
{
acadoVariables.u[19] = uEnd[0];
}
}

real_t acado_getKKT(  )
{
real_t kkt;

int index;
real_t prd;

kkt = + acadoWorkspace.g[0]*acadoWorkspace.x[0] + acadoWorkspace.g[1]*acadoWorkspace.x[1] + acadoWorkspace.g[2]*acadoWorkspace.x[2] + acadoWorkspace.g[3]*acadoWorkspace.x[3] + acadoWorkspace.g[4]*acadoWorkspace.x[4] + acadoWorkspace.g[5]*acadoWorkspace.x[5] + acadoWorkspace.g[6]*acadoWorkspace.x[6] + acadoWorkspace.g[7]*acadoWorkspace.x[7] + acadoWorkspace.g[8]*acadoWorkspace.x[8] + acadoWorkspace.g[9]*acadoWorkspace.x[9] + acadoWorkspace.g[10]*acadoWorkspace.x[10] + acadoWorkspace.g[11]*acadoWorkspace.x[11] + acadoWorkspace.g[12]*acadoWorkspace.x[12] + acadoWorkspace.g[13]*acadoWorkspace.x[13] + acadoWorkspace.g[14]*acadoWorkspace.x[14] + acadoWorkspace.g[15]*acadoWorkspace.x[15] + acadoWorkspace.g[16]*acadoWorkspace.x[16] + acadoWorkspace.g[17]*acadoWorkspace.x[17] + acadoWorkspace.g[18]*acadoWorkspace.x[18] + acadoWorkspace.g[19]*acadoWorkspace.x[19];
kkt = fabs( kkt );
for (index = 0; index < 20; ++index)
{
prd = acadoWorkspace.y[index];
if (prd > 1e-12)
kkt += fabs(acadoWorkspace.lb[index] * prd);
else if (prd < -1e-12)
kkt += fabs(acadoWorkspace.ub[index] * prd);
}
for (index = 0; index < 40; ++index)
{
prd = acadoWorkspace.y[index + 20];
if (prd > 1e-12)
kkt += fabs(acadoWorkspace.lbA[index] * prd);
else if (prd < -1e-12)
kkt += fabs(acadoWorkspace.ubA[index] * prd);
}
return kkt;
}

real_t acado_getObjective(  )
{
real_t objVal;

int lRun1;
/** Row vector of size: 3 */
real_t tmpDy[ 3 ];

/** Row vector of size: 2 */
real_t tmpDyN[ 2 ];

for (lRun1 = 0; lRun1 < 20; ++lRun1)
{
acadoWorkspace.objValueIn[0] = acadoVariables.x[lRun1 * 2];
acadoWorkspace.objValueIn[1] = acadoVariables.x[lRun1 * 2 + 1];
acadoWorkspace.objValueIn[2] = acadoVariables.u[lRun1];

acado_evaluateLSQ( acadoWorkspace.objValueIn, acadoWorkspace.objValueOut );
acadoWorkspace.Dy[lRun1 * 3] = acadoWorkspace.objValueOut[0] - acadoVariables.y[lRun1 * 3];
acadoWorkspace.Dy[lRun1 * 3 + 1] = acadoWorkspace.objValueOut[1] - acadoVariables.y[lRun1 * 3 + 1];
acadoWorkspace.Dy[lRun1 * 3 + 2] = acadoWorkspace.objValueOut[2] - acadoVariables.y[lRun1 * 3 + 2];
}
acadoWorkspace.objValueIn[0] = acadoVariables.x[40];
acadoWorkspace.objValueIn[1] = acadoVariables.x[41];
acado_evaluateLSQEndTerm( acadoWorkspace.objValueIn, acadoWorkspace.objValueOut );
acadoWorkspace.DyN[0] = acadoWorkspace.objValueOut[0] - acadoVariables.yN[0];
acadoWorkspace.DyN[1] = acadoWorkspace.objValueOut[1] - acadoVariables.yN[1];
objVal = 0.0000000000000000e+00;
for (lRun1 = 0; lRun1 < 20; ++lRun1)
{
tmpDy[0] = + acadoWorkspace.Dy[lRun1 * 3]*acadoVariables.W[0];
tmpDy[1] = + acadoWorkspace.Dy[lRun1 * 3 + 1]*acadoVariables.W[4];
tmpDy[2] = + acadoWorkspace.Dy[lRun1 * 3 + 2]*acadoVariables.W[8];
objVal += + acadoWorkspace.Dy[lRun1 * 3]*tmpDy[0] + acadoWorkspace.Dy[lRun1 * 3 + 1]*tmpDy[1] + acadoWorkspace.Dy[lRun1 * 3 + 2]*tmpDy[2];
}

tmpDyN[0] = + acadoWorkspace.DyN[0]*acadoVariables.WN[0];
tmpDyN[1] = + acadoWorkspace.DyN[1]*acadoVariables.WN[3];
objVal += + acadoWorkspace.DyN[0]*tmpDyN[0] + acadoWorkspace.DyN[1]*tmpDyN[1];

objVal *= 0.5;
return objVal;
}

