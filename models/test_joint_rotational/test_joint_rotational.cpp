#include <acado_toolkit.hpp>
#include <acado_gnuplot.hpp>
#include <iostream>
#include <cmath>

#define _USE_MATH_DEFINES

USING_NAMESPACE_ACADO

void simulateModel(DifferentialEquation f, OCP ocp, double Ts, double t_end, double t_sim) {
    // SETTING UP THE (SIMULATED) PROCESS:
    // -----------------------------------
    OutputFcn identity;
    DynamicSystem dynamicSystem(f, identity);

    Process process(dynamicSystem, INT_RK45);


    // SETTING UP THE MPC CONTROLLER:
    // ------------------------------
    RealTimeAlgorithm alg(ocp, Ts);
    alg.set(INTEGRATOR_TYPE, INT_RK78);

    StaticReferenceTrajectory zeroReference;

    Controller controller(alg, zeroReference);


    // SETTING UP THE SIMULATION ENVIRONMENT,  RUN THE EXAMPLE...
    // ----------------------------------------------------------
    SimulationEnvironment sim(0.0,t_sim,process,controller);

    DVector x0(2);
    x0.setAll(0.0);

    if (sim.init(x0) != SUCCESSFUL_RETURN) {
        exit(EXIT_FAILURE);
    }
    if (sim.run() != SUCCESSFUL_RETURN) {
        exit(EXIT_FAILURE);
    }


    // PLOT THE RESULTS
    // ----------------
    VariablesGrid sampledProcessOutput;
    sim.getSampledProcessOutput(sampledProcessOutput);

    VariablesGrid feedbackControl;
    sim.getFeedbackControl(feedbackControl);

    VariablesGrid intermediateStates;
    sim.getProcessIntermediateStates(intermediateStates);

    GnuplotWindow window;

    // State theta, Angle
    window.addSubplot(sampledProcessOutput(0), "state, Angle");
    window.setLabelX(0,"time [s]");
    window.setLabelY(0,"rad");

    // State dtheta, Angular Velocity
    window.addSubplot(sampledProcessOutput(1), "state, Angular Velocity");
    window.setLabelX(1, "time [s]");
    window.setLabelY(1, "rad/s");

    // Control T, torque
    window.addSubplot(feedbackControl(0), "input, Torque");
    window.setLabelX(2, "time [s]");
    window.setLabelY(2, "N/m");

    // ddtheta, Angular Acceleration
    window.addSubplot(intermediateStates(0), "Angular Acceleration");
    window.setLabelX(3, "time [s]");
    window.setLabelY(3, "rad/s^2");

    window.plot();

    exit(EXIT_SUCCESS);
}

void generateMPCModel(OCP ocp, int N, int Ni) {

    // DEFINE AN MPC EXPORT MODULE AND GENERATE THE CODE:
    // --------------------------------------------------

    OCPexport mpc(ocp);

    mpc.set( HESSIAN_APPROXIMATION,        GAUSS_NEWTON  		 );
    mpc.set( DISCRETIZATION_TYPE,          MULTIPLE_SHOOTING );
    mpc.set( INTEGRATOR_TYPE,              INT_RK4   			   );
    mpc.set( NUM_INTEGRATOR_STEPS,         N*Ni            	 );
    mpc.set( QP_SOLVER,                    QP_QPOASES    		 );
    mpc.set( HOTSTART_QP,                  NO             	 );
    mpc.set(CG_HARDCODE_CONSTRAINT_VALUES, NO                );
    mpc.set( GENERATE_TEST_FILE,           NO            		 );
    mpc.set( GENERATE_MAKE_FILE,           NO            		 );

    if (mpc.exportCode( "../model_export" ) != SUCCESSFUL_RETURN) {
        exit(EXIT_FAILURE);
    }

    mpc.printDimensionsQP( );
}

bool generateModelCheck(int argc, char** argv) {
    /*
     * Checks if the mpc model should be generated or simulated based on the given executable arguments.
     * On default this functions returns true but when the executable arguments -s or --simulate are used
     * the functions returns false.
     * There is also a help argument that will exit the program after it has displayed the help page.
     */

    // Check given arguments
    for (int i = 0; i < argc; i++)
    {
        // strcmp() returns 0 when the two strings are equal.
        // The NOT operator "!" is used to flip the output of strcmp() for correct usage in the if statement.
        if (!strcmp(argv[i], "-h") || !strcmp(argv[i],"--help")) {
            std::cout
              << "Usage: ./mpc_model [OPTION]...\n"
              << "Generates the mpc model files using the ACADO Toolkit code generation when no options are given\n\n"
              << "Options:\n"
              << "-s, --simulate" << "\t\t" << "perform a closed-loop mpc simulation\n"
              << "-h, --help" << "\t\t" << "display this help and exit\n"
              << std::endl;
            exit(EXIT_SUCCESS);
        }
        else if (!strcmp(argv[i], "-s") || !strcmp(argv[i],"--simulate")) {
            return false;
        }
    }

    return true;
}

/**
 * The executable build with this script supports command-line arguments
 * These are supplied to the main function by the variables argc and argv
 * @param argc - number of command-line arguments
 * @param argv - list of command-line arguments
 */

int main(int argc, char** argv)
{

    // Switching variable for generating or simulating the mpc
    bool generate_model = generateModelCheck(argc, argv);


    // Define MPC  :
    // ============================

    // DIFFERENTIAL STATES :
    // ----------------------------
    DifferentialState    theta;         // the angle of the joint from the horizontal
    // ----------------------------     // -------------------------------------------
    DifferentialState   dtheta;         // first derivative of theta with respect to time


    // CONTROL :
    // ----------------------------
    Control                  T;     // joint torque


    // PARAMETERS :
    // =========================

    // SYSTEM PARAMETERS :
    // -------------------------
    double g =         9.81;                  // gravitational constant       // [m/s^2]
    double m =        1.080;                  // bar mass                     // [kg]
    double L =         0.39;                  // length of the arm            // [m]

    // OCP PARAMETERS :
    // -------------------------
    const int    N       =   20;            // horizon length               // [ ]
    const int    Ni      =    4;            // integrator amount            // [ ]
    const double Ts      = 0.02;            // sampling time                // [s]
    const double t_end   = N*Ts;            // ocp end time                 // [s]
    const double t_sim   =  2.0;            // simulation time              // [s]

    // CONSTRAINT PARAMETERS :
    // ----------------------------
    double theta_max    = 0.5363;   // Maximum joint angle         // [rad]
    double dtheta_max   =   10.0;   // Maximum joint speed         // [rad/s]
    double T_max        =   20.0;   // Maximum torque              // [Nm]


    // TERMS ON RIGHT-HAND-SIDE
    // OF THE DIFFERENTIAL
    // EQUATIONS :
    // ----------------------------
    IntermediateState  ddtheta;         // second derivative of theta with respect to time


    // MODEL EQUATIONS :
    // ========================================

    // THE EQUATIONS OF MOTION :
    // ----------------------------------------
    ddtheta = (3*T)/(m*L*L) - (3*g*sin(theta))/(2*L);


    // THE "RIGHT-HAND-SIDE" OF THE ODE :
    // ----------------------------------------
    DifferentialEquation f;

    f << dot(theta)  == dtheta;
    f << dot(dtheta) == ddtheta;

    //  DEFINE THE WEIGHTING MATRICES :
    // ----------------------------------------
    Expression states;
    states << theta;
    states << dtheta;

    Expression control;
    control << T;

    // Running least square function
    Function h;
    h << states;
    h << control;

    // End least square function
    Function hN;
    hN << states;

    // Running cost matrix
    DMatrix Q(h.getDim(),h.getDim());
    Q.setAll(0.0);
    Q(0,0) = 100.0; // theta
    Q(1,1) = 1.0;   // dtheta
    Q(2,2) = 0.5;   // T

    // End cost matrix
    DMatrix QN(hN.getDim(),hN.getDim());
    QN.setAll(0.0);
    QN(0,0) = Q(0,0); // theta
    QN(1,1) = Q(1,1); // dtheta

    // Running reference
    DVector r(h.getDim());
    r.setAll(0.0);
    r(0) = 0.0;

    // End reference
    DVector rN(h.getDim());
    rN.setAll(0.0);
    rN(0) = r(0);


    // SET UP THE MPC - OPTIMAL CONTROL PROBLEM :
    // ------------------------------------------

    OCP ocp(0.0, t_end, N);

    // System dynamics constraint
    ocp.subjectTo(f);

    // Set the LSQ objective for either simulation or code generation purposes
    if (!generate_model) {
        // The simulation environment requires a reference vector in the LSQ objective
        ocp.minimizeLSQ(Q, h, r);
        ocp.minimizeLSQEndTerm(QN, hN, rN);
    } else {

        // Create post model generation modifiable weighting matrices
        // Keep in mind that the amount of values in Q and QN
        // increase quadratically with the length of h and hN respectively.
        BMatrix Q  = eye<bool>(h.getDim());
        BMatrix QN = eye<bool>(hN.getDim());

        // Code generation supports only the standard LSQ objective
        ocp.minimizeLSQ(Q, h);
        ocp.minimizeLSQEndTerm(QN, hN);
    }

    // state and input constraints
    ocp.subjectTo(-theta_max  <= theta  <= theta_max);
    ocp.subjectTo(-dtheta_max <= dtheta <= dtheta_max);
    ocp.subjectTo(-T_max      <= T      <= T_max);


    // SIMULATE OR GENERATED THE MODEL :
    // ---------------------------------
    if (!generate_model) {
        simulateModel(f, ocp, Ts, t_end, t_sim);
    } else {
        generateMPCModel(ocp, N, Ni);
    }

    return EXIT_SUCCESS;

}


